;; -*- coding: utf-8 -*-

;; *Exercise 2.73:* Section *Note 2-3-2:: described a program that
;; performs symbolic differentiation:

;;      (define (deriv exp var)
;;        (cond ((number? exp) 0)
;;              ((variable? exp) (if (same-variable? exp var) 1 0))
;;              ((sum? exp)
;;               (make-sum (deriv (addend exp) var)
;;                         (deriv (augend exp) var)))
;;              ((product? exp)
;;               (make-sum
;;                 (make-product (multiplier exp)
;;                               (deriv (multiplicand exp) var))
;;                 (make-product (deriv (multiplier exp) var)
;;                               (multiplicand exp))))
;;              <MORE RULES CAN BE ADDED HERE>
;;              (else (error "unknown expression type -- DERIV" exp))))

;; We can regard this program as performing a dispatch on the type of
;; the expression to be differentiated.  In this situation the "type
;; tag" of the datum is the algebraic operator symbol (such as `+')
;; and the operation being performed is `deriv'.  We can transform
;; this program into data-directed style by rewriting the basic
;; derivative procedure as

;;      (define (deriv exp var)
;;         (cond ((number? exp) 0)
;;               ((variable? exp) (if (same-variable? exp var) 1 0))
;;               (else ((get 'deriv (operator exp)) (operands exp)
;;                                                  var))))

;;      (define (operator exp) (car exp))

;;      (define (operands exp) (cdr exp))

;;   a. Explain what was done above.  Why can't we assimilate the
;;      predicates `number?' and `same-variable?' into the
;;      data-directed dispatch?

;;   b. Write the procedures for derivatives of sums and products,
;;      and the auxiliary code required to install them in the table
;;      used by the program above.

;;   c. Choose any additional differentiation rule that you like,
;;      such as the one for exponents (*Note Exercise 2-56::), and
;;      install it in this data-directed system.

;;   d. In this simple algebraic manipulator the type of an
;;      expression is the algebraic operator that binds it together.
;;      Suppose, however, we indexed the procedures in the opposite
;;      way, so that the dispatch line in `deriv' looked like

;;           ((get (operator exp) 'deriv) (operands exp) var)

;;      What corresponding changes to the derivative system are
;;      required?


;; a.
;; primitive なデータを引数に取る場合は deriv 手続き内で場合分けして処理され、
;; それ以外はテーブルを元に dispatch される
;; number? みたいのを使うのは primitive なデータにタグを付与できないから。
;; （あるいは primitive なデータ用のコンストラクタを用意すればよいが、煩雑）

;; b.
(define *table* (make-hash-table 'equal?))
(define (get op type)
  (hash-table-get *table* (list op type)))
(define (put op type item)
  (hash-table-put! *table* (list op type) item))

(define (deriv exp var)
  (cond ((number? exp) 0)
        ((variable? exp) (if (same-variable? exp var) 1 0))
        (else ((get 'deriv (operator exp)) (operands exp)
               var))))
(define (operator exp) (car exp))
(define (operands exp) (cdr exp))

(define (variable? e) (symbol? e))
(define (same-variable? x y) (eq? x y))

(define (make-sum x y) (list '+ x y))
(define (addend args) (car args))
(define (augend args) (cadr args))

(define (make-product x y) (list '* x y))
(define (multiplier args) (car args))
(define (multiplicand args) (cadr args))

(define (install-sum)
  (put 'deriv '+ (lambda (exp var)
                   (make-sum (deriv (addend exp) var)
                             (deriv (augend exp) var))))
  'done)
(define (install-product)
  (put 'deriv '* (lambda (exp var)
                   (make-sum (make-product (deriv (multiplier exp) var)
                                           (multiplicand exp))
                             (make-product (multiplier exp)
                                           (deriv (multiplicand exp) var)))))
  'done)

(install-sum)
(install-product)

;; c
(define (make-exponential b n)
  (cond [(= n 0) 1]
        [(= n 1) b]
        [else (list '** b n)]))
(define (base args) (car args))
(define (exponent args) (cadr args))
(define (install-exponential)
  (put 'deriv '** (lambda (exp var)
                    (let ((b (base exp))
                          (n (exponent exp)))
                      (make-product (make-product n
                                                  (make-exponential b (- n 1)))
                                    (deriv b var)))))
  'done)
(install-exponential)

;; d.
;; get の引数を逆にする
;; put も
