;; -*- coding: utf-8 -*-

;; *Exercise 2.40:* Define a procedure `unique-pairs' that, given an
;; integer n, generates the sequence of pairs (i,j) with 1 <= j< i <=
;; n.  Use `unique-pairs' to simplify the definition of
;; `prime-sum-pairs' given above.

(use srfi-1)

(define (enumerate-interval low high)
  (define (iter n result)
    (if (< n low)
        result
        (iter (- n 1) (cons n result))))
  (iter high '()))

(define (unique-pairs n)
  (fold-right append '()
              (map (lambda (i)
                     (map (lambda (j) (list j i))
                          (enumerate-interval 1 (- i 1))))
                   (enumerate-interval 1 n))))

(define (prime? n)
  (define (divides? a b) (= (remainder a b) 0))
  (define (check i result)
    (cond [(> i (sqrt n)) result]
          [(not result) #f]
          [else (check (+ i 1) (and result (not (divides? n i))))]))
  (check 2 #t))

(define (make-pair-sum pair)
  (list (car pair) (cadr pair) (+ (car pair) (cadr pair))))


(define (prime-sum-pairs n)
  (map make-pair-sum
       (filter (lambda (pair) (prime? (+ (car pair) (cadr pair))))
               (unique-pairs n))))

(use gauche.test)
(test-start "exercise 2.40")
(test* "unique-pairs" '((1 2) (1 3) (2 3)) (unique-pairs 3))
(test* "prime-sum-pairs"
       '((1 2 3) (2 3 5) (1 4 5) (3 4 7) (2 5 7) (1 6 7) (5 6 11))
       (prime-sum-pairs 6))
(test-end)
