;; -*- coding: utf-8 -*-

;; *Exercise 2.48:* A directed line segment in the plane can be
;; represented as a pair of vectors--the vector running from the
;; origin to the start-point of the segment, and the vector running
;; from the origin to the end-point of the segment.  Use your vector
;; representation from *Note Exercise 2-46:: to define a
;; representation for segments with a constructor `make-segment' and
;; selectors `start-segment' and `end-segment'.

(define (make-vect x y) (list x y))
(define (xcor-vect v) (car v))
(define (ycor-vect v) (cadr v))
(define (equal-vect? v1 v2)
  (and (= (xcor-vect v1) (xcor-vect v2))
       (= (ycor-vect v1) (ycor-vect v2))))

(define (make-segment start-point end-point)
  (list start-point end-point))
(define (start-segment segment)
  (car segment))
(define (end-segment segment)
  (cadr segment))

(use gauche.test)
(test-start "exercise 2.48")
(let* ((start-point (make-vect 1 1))
       (end-point (make-vect 2 2))
       (segment (make-segment start-point end-point)))
  (test* "start-segment" start-point (start-segment segment) equal-vect?)
  (test* "end-segment" end-point (end-segment segment) equal-vect?))
(test-end)
