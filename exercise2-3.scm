;; -*- coding: utf-8 -*-

;; *Exercise 2.3:* Implement a representation for rectangles in a
;; plane.  (Hint: You may want to make use of *Note Exercise 2-2::.)
;; In terms of your constructors and selectors, create procedures
;; that compute the perimeter and the area of a given rectangle.  Now
;; implement a different representation for rectangles.  Can you
;; design your system with suitable abstraction barriers, so that the
;; same perimeter and area procedures will work using either
;; representation?

;; point
(define (make-point x y)
  (cons x y))

(define (x-point p)
  (car p))

(define (y-point p)
  (cdr p))

;; line segment
(define (make-segment starting-point ending-point)
  (cons starting-point ending-point))

(define (start-segment segment)
  (car segment))
(define (end-segment segment)
  (cdr segment))

;; answer from here
(define (make-rectangle top-left bottom-right)
  (let ([p1 top-left]
        [p2 (make-point (x-point bottom-right) (y-point top-left))]
        [p3 bottom-right]
        [p4 (make-point (x-point top-left) (y-point bottom-right))])
    (cons (cons p1 p2) (cons p3 p4))))

(define (top-left-rectangle rect)
  (car (car rect)))
(define (top-right-rectangle rect)
  (cdr (car rect)))
(define (bottom-right-rectangle rect)
  (car (cdr rect)))
(define (bottom-left-rectangle rect)
  (cdr (cdr rect)))

(define (width-rectangle rect)
  (let ([x1 (x-point (top-left-rectangle rect))]
        [x2 (x-point (top-right-rectangle rect))])
    (- x2 x1)))
(define (height-rectangle rect)
  (let ([y1 (y-point (top-left-rectangle rect))]
        [y2 (y-point (bottom-left-rectangle rect))])
    (- y1 y2)))

(define (perimeter-rectangle rect)
  (* 2 (+ (width-rectangle rect)
          (height-rectangle rect))))
(define (area-rectangle rect)
  (* (width-rectangle rect) (height-rectangle rect)))

;; test
(use gauche.test)
(test-start "exercise 2.3")
(let* ([top-left (make-point 0 2)]
       [bottom-right (make-point 3 0)]
       [rect (make-rectangle top-left bottom-right)])
  (test* "perimeter is 10" 10 (perimeter-rectangle rect))
  (test* "area is 6" 6 (area-rectangle rect)))
(test-end)


;; other rectangle implementation
(define (make-rectangle2 top-left bottom-right)
  (let ([width (- (x-point bottom-right) (x-point top-left))]
        [height (- (y-point top-left) (y-point bottom-right))])
    (cons top-left (cons width height))))

(define (top-left-rectangle rect)
  (car rect))
(define (top-right-rectangle rect)
  (let ([top-left (top-left-rectangle rect)]
        [width (car (cdr rect))])
    (make-point (+ width (x-point top-left))
                (y-point top-left))))
(define (bottom-right-rectangle rect)
  (let ([top-left (top-left-rectangle rect)]
        [width (car (cdr rect))]
        [height (cdr (cdr rect))])
    (make-point (+ width (x-point top-left))
                (- height (y-point top-left)))))
(define (bottom-left-rectangle rect)
  (let ([top-left (top-left-rectangle rect)]
        [height (cdr (cdr rect))])
    (make-point (x-point top-left)
                (- height (y-point top-left)))))

;; test
(use gauche.test)
(test-start "exercise 2.3 (other implementation)")
(let* ([top-left (make-point 0 2)]
       [bottom-right (make-point 3 0)]
       [rect (make-rectangle2 top-left bottom-right)])
  (test* "perimeter is 10" 10 (perimeter-rectangle rect))
  (test* "area is 6" 6 (area-rectangle rect)))
(test-end)
