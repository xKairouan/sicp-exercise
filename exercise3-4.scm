;; -*- coding: utf-8 -*-

;; *Exercise 3.4:* Modify the `make-account' procedure of *Note
;; Exercise 3-3:: by adding another local state variable so that, if
;; an account is accessed more than seven consecutive times with an
;; incorrect password, it invokes the procedure `call-the-cops'.

(define (make-account balance password)
  (let ((incorrect-count 0))
    (define (withdraw amount)
      (if (>= balance amount)
          (begin (set! balance (- balance amount))
                 balance)
          "Insufficient funds"))
    (define (deposit amount)
      (set! balance (+ balance amount))
      balance)
    (define (dispatch pw m)
      (cond [(not (eq? pw password))
             (set! incorrect-count (+ incorrect-count 1))
             (if (> incorrect-count 7)
                 call-the-cops
                 (lambda (x) "Incorrect password"))]
            [(eq? m 'withdraw) withdraw]
            [(eq? m 'deposit) deposit]
            [else (error "Unknown request -- MAKE-ACCOUNT"
                         m)]))
    dispatch))

(define (call-the-cops x)
  "We have called the cops for you!")


;; test
(use gauche.test)
(test-start "exercise 3.3")
(let ((acc (make-account 100 'secret-password)))
  (test* "withdraw (succeed)" 60 ((acc 'secret-password 'withdraw) 40))
  (test* "deposit (fail)" "Incorrect password" ((acc 'some-other-password 'deposit) 50))
  ((acc 'some-other-password 'withdraw) 50)
  ((acc 'some-other-password 'withdraw) 50)
  ((acc 'some-other-password 'withdraw) 50)
  ((acc 'some-other-password 'withdraw) 50)
  ((acc 'some-other-password 'withdraw) 50)
  ((acc 'some-other-password 'withdraw) 50)
  (test* "deposit (call-the-cops)" "We have called the cops for you!" ((acc 'some-other-password 'withdraw) 50)))
(test-end)
