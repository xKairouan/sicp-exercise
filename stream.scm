;; -*- coding: utf-8 -*-

;; (define-module stream
;;   (export cons-stream
;;           stream-car
;;           stream-cdr
;;           the-empty-stream
;;           stream-null?
;;           stream-ref
;;           stream-map
;;           stream-filter
;;           stream-for-each
;;           stream-take-while
;;           stream-take
;;           stream->list
;;           display-stream
;;           stream-enumerate-interval
;;           delay
;;           force))
;; (select-module stream)

;; delay/force
(define-syntax delay
  (syntax-rules ()
    [(delay exp) (memo-proc (lambda () exp))]))
(define (force delayed-object) (delayed-object))
(define (memo-proc proc)
  (let ((already-run? #f) (result #f))
    (lambda ()
      (if (not already-run?)
          (begin (set! result (proc))
                 (set! already-run? #t)
                 result)
          result))))

;; data constructor and selector
(define-syntax cons-stream
  (syntax-rules ()
    [(cons-stream exp1 exp2)
     (cons exp1 (delay exp2))]))
(define (stream-car stream) (car stream))
(define (stream-cdr stream) (force (cdr stream)))

;; empty
(define the-empty-stream '())
(define stream-null? null?)

(define (stream-ref s n)
  (if (= n 0)
      (stream-car s)
      (stream-ref (stream-cdr s) (- n 1))))

(define (stream-map proc . argstreams)
  (if (fold (lambda (x y) (or x y)) #f (map stream-null? argstreams))
      the-empty-stream
      (cons-stream (apply proc (map stream-car argstreams))
                   (apply stream-map
                          (cons proc (map stream-cdr argstreams))))))

(define (stream-for-each proc s)
  (if (stream-null? s)
      'done
      (begin (proc (stream-car s))
             (stream-for-each proc (stream-cdr s)))))

(define (stream-filter pred stream)
  (cond ((stream-null? stream) the-empty-stream)
        ((pred (stream-car stream))
         (cons-stream (stream-car stream)
                      (stream-filter pred
                                     (stream-cdr stream))))
        (else (stream-filter pred (stream-cdr stream)))))

(define (stream-enumerate-interval low high)
  (if (> low high)
      the-empty-stream
      (cons-stream
       low
       (stream-enumerate-interval (+ low 1) high))))

(define (stream-take-while pred stream)
  (cond [(stream-null? stream) the-empty-stream]
        [(pred (stream-car stream))
         (cons-stream (stream-car stream)
                      (stream-take-while pred (stream-cdr stream)))]
        [else the-empty-stream]))

(define (stream-take stream count)
  (cond [(stream-null? stream) the-empty-stream]
        [(= count 0) the-empty-stream]
        [else (cons-stream (stream-car stream)
                           (stream-take (stream-cdr stream) (- count 1)))]))

(define (stream->list stream)
  (if (stream-null? stream)
      '()
      (cons (stream-car stream) (stream->list (stream-cdr stream)))))


;; display
(define (display-stream s)
  (stream-for-each display-line s))

(define (display-line x)
  (newline)
  (display x))

;; (provide "stream")
