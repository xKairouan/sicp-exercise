;; -*- coding: utf-8 -*-

;; *Exercise 3.47:* A semaphore (of size n) is a generalization of a
;; mutex.  Like a mutex, a semaphore supports acquire and release
;; operations, but it is more general in that up to n processes can
;; acquire it concurrently.  Additional processes that attempt to
;; acquire the semaphore must wait for release operations.  Give
;; implementations of semaphores

;;   a. in terms of mutexes

;;   b. in terms of atomic `test-and-set!' operations.


;; a.
(define (make-semaphore n)
  (let ((mutex (make-mutex))
        (count 0))
    (define (acquire)
      (mutex 'acquire)
      (if (< count n)
          (set! count (+ count 1))
          (begin (mutex 'release)
                 (acquire)))
      (mutex 'release))
    (define (release)
      (mutex 'acquire)
      (set! count (+ count 1))
      (mutex 'release))
    (define (the-semaphore m)
      (cond [(eq? m 'acquire) (acquire)]
            [(eq? m 'release) (release)]))
    the-semaphore))