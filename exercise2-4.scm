;; -*- coding: utf-8 -*-

;; *Exercise 2.4:* Here is an alternative procedural representation
;; of pairs.  For this representation, verify that `(car (cons x y))'
;; yields `x' for any objects `x' and `y'.

;;      (define (cons x y)
;;        (lambda (m) (m x y)))

;;      (define (car z)
;;        (z (lambda (p q) p)))

;; What is the corresponding definition of `cdr'? (Hint: To verify
;; that this works, make use of the substitution model of section
;; *Note 1-1-5::.)

(define (mycons x y)
  (lambda (m) (m x y)))

(define (mycar z)
  (z (lambda (p q) p)))

(define (mycdr z)
  (z (lambda (p q) q)))

;; (mycdr (mycons 1 2))
;; => (mycdr (lambda (m) (m 1 2)))
;; => ((lambda (m) (m 1 2)) (lambda (p q) q))
;; => ((lambda (p q) q) 1 2)
;; => 2


(use gauche.test)
(test-start "exercise2-4")
(test* "(mycdr (mycons 1 2)) => 2" 2 (mycdr (mycons 1 2)))
(test-end)
