;; -*- coding: utf-8 -*-

;; *Exercise 3.62:* Use the results of *Note Exercise 3-60:: and
;; *Note Exercise 3-61:: to define a procedure `div-series' that
;; divides two power series.  `Div-series' should work for any two
;; series, provided that the denominator series begins with a nonzero
;; constant term.  (If the denominator has a zero constant term, then
;; `div-series' should signal an error.)  Show how to use
;; `div-series' together with the result of *Note Exercise 3-59:: to
;; generate the power series for tangent.

(load "./stream.scm")

(define (add-streams s1 s2)
  (stream-map + s1 s2))

(define (scale-stream stream factor)
  (stream-map (lambda (x) (* x factor)) stream))

(define (mul-series s1 s2)
  (cons-stream (* (stream-car s1)
                  (stream-car s2))
               (add-streams (scale-stream (stream-cdr s2) (stream-car s1))
                            (mul-series (stream-cdr s1) s2))))

(define (invert-unit-series s)
  (if (not (= (stream-car s) 1))
      (error "not a power series with constant 1 -- INVERT-UNIT-SERIES")
      (cons-stream 1 (stream-map - (mul-series (stream-cdr s)
                                               (invert-unit-series s))))))


;; answer
(define (div-series num-series den-series)
  (if (zero? (stream-car den-series))
      (error "denominator has zero constant -- DIV-SERIES")
      (let ((c (/ 1(stream-car den-series))))
        (mul-series (scale-stream num-series c)
                    (invert-unit-series (scale-stream den-series c))))))

(define ones (cons-stream 1 ones))
(define integers  (cons-stream 1 (stream-map + ones integers)))
(define (integrate-series a-stream)
  (stream-map * (stream-map (lambda (x) (/ 1 x)) integers)
                a-stream))

(define cosine-series
  (cons-stream 1 (stream-map - (integrate-series sine-series))))
(define sine-series
  (cons-stream 0 (integrate-series cosine-series)))

(define tan-series (div-series sine-series cosine-series))
(print (stream->list (stream-take tan-series 10)))