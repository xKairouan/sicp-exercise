;; -*- coding: utf-8 -*-

;; *Exercise 1.18:* Using the results of *Note Exercise 1-16:: and
;; *Note Exercise 1-17::, devise a procedure that generates an
;; iterative process for multiplying two integers in terms of adding,
;; doubling, and halving and uses a logarithmic number of steps.(4)

(define (double x) (* x 2))
(define (halve x) (/ x 2))

(define (mul-iter a b)
  (define (iter a b result)
    (cond [(= b 0) result]
          [(even? b) (iter (double a) (/ b 2) result)]
          [else (iter a (- b 1) (+ result a))]))
  (iter a b 0))

(use gauche.test)
(test-start "exercise 1.18")
(test* "2 * 0 = 0" (* 1 0) (mul-iter 1 0))
(test* "0 * 2 = 0" (* 0 2) (mul-iter 0 2))
(test* "3 * 3 = 9" (* 3 3) (mul-iter 3 3))
(test* "3 * 4 = 12" (* 3 4) (mul-iter 3 4))
(test* "3 * 9 = 27" (* 3 9) (mul-iter 3 9))
(test* "3 * 10 = 30" (* 3 10) (mul-iter 3 10))
(test-end)
