;; -*- coding: utf-8 -*-

;; *Exercise 3.7:* Consider the bank account objects created by
;; `make-account', with the password modification described in *Note
;; Exercise 3-3::.  Suppose that our banking system requires the
;; ability to make joint accounts.  Define a procedure `make-joint'
;; that accomplishes this.  `Make-joint' should take three arguments.
;; The first is a password-protected account.  The second argument
;; must match the password with which the account was defined in
;; order for the `make-joint' operation to proceed.  The third
;; argument is a new password.  `Make-joint' is to create an
;; additional access to the original account using the new password.
;; For example, if `peter-acc' is a bank account with password
;; `open-sesame', then

;;      (define paul-acc
;;        (make-joint peter-acc 'open-sesame 'rosebud))

;; will allow one to make transactions on `peter-acc' using the name
;; `paul-acc' and the password `rosebud'.  You may wish to modify your
;; solution to *Note Exercise 3-3:: to accommodate this new feature


(define (make-account balance password)
  (let ((joint-passwords '()))
    (define (withdraw amount)
      (if (>= balance amount)
          (begin (set! balance (- balance amount))
                 balance)
          "Insufficient funds"))
    (define (deposit amount)
      (set! balance (+ balance amount))
      balance)
    (define (joint jpass)
      (set! joint-passwords (cons jpass joint-passwords))
      (print joint-passwords))
    (define (dispatch pw m)
      (cond [(and (not (eq? pw password))
                  (not (memq pw joint-passwords)))
             (lambda (x) "Incorrect password")]
            [(eq? m 'withdraw) withdraw]
            [(eq? m 'deposit) deposit]
            [(eq? m 'joint) joint]
            [else (error "Unknown request -- MAKE-ACCOUNT"
                         m)]))
    dispatch))


(define (make-joint account password new-password)
  ((account password 'joint) new-password)
  account)

(use gauche.test)
(test-start "exercise 3.7")
(define peter-acc (make-account 100 'open-sesame))
(define paul-acc (make-joint peter-acc 'open-sesame 'rosebud))
(test* "deposit (peter)" 150 ((peter-acc 'open-sesame 'deposit) 50))
(test* "deposit (paul)" 200 ((paul-acc 'rosebud 'deposit) 50))
(test* "withdraw (paul)" 140 ((paul-acc 'rosebud 'withdraw) 60))
(test* "withdraw (peter)" 90 ((peter-acc 'open-sesame 'withdraw) 50))
(test-end)
