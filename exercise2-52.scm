;; -*- coding: utf-8 -*-

;; *Exercise 2.52:* Make changes to the square limit of `wave' shown
;; in *Note Figure 2-9:: by working at each of the levels described
;; above.  In particular:

;;   a. Add some segments to the primitive `wave' painter of *Note
;;      Exercise 2-49:: (to add a smile, for example).

;;   b. Change the pattern constructed by `corner-split' (for
;;      example, by using only one copy of the `up-split' and
;;      `right-split' images instead of two).

;;   c. Modify the version of `square-limit' that uses
;;      `square-of-four' so as to assemble the corners in a different
;;      pattern.  (For example, you might make the big Mr. Rogers
;;      look outward from each corner of the square.)

(use gl)
(use gl.glut)

(add-load-path ".")

(use painter)

;; a.
(define (wave/smile frame)
  (let ((p0 (make-vect 0.45 0.90))
        (p1 (make-vect 0.55 0.90))
        (p2 (make-vect 0.45 0.80))
        (p3 (make-vect 0.55 0.80))
        (p4 (make-vect 0.45 0.75))
        (p5 (make-vect 0.55 0.75))
        (p6 (make-vect 0.45 0.65))
        (p7 (make-vect 0.55 0.65)))
    (let ((seg0 (make-segment p0 p2))
          (seg1 (make-segment p1 p3))
          (seg2 (make-segment p4 p6))
          (seg3 (make-segment p6 p7))
          (seg4 (make-segment p7 p5)))
      (wave frame)
      ((segments->painter (list seg0 seg1 seg2 seg3 seg4)) frame))))

;; b.
;; (set! corner-split
;;       (lambda (painter n)
;;         (if (= n 0)
;;             painter
;;             (let ((up (up-split painter (- n 1)))
;;                   (right (right-split painter (- n 1))))
;;               (beside (below painter up)
;;                       (below right (corner-split painter (- n 1))))))))

;; c
(define (square-limit2 painter n)
  (let ((combine4 (square-of-four flip-vert rotate180 identity flip-horiz)))
    (combine4 (corner-split painter n))))

;; drawing
(define (init)
  (gl-clear-color 1.0 1.0 1.0 1.0))

(define (draw)
  (gl-clear GL_COLOR_BUFFER_BIT)
  (gl-color 0.0 0.0 0.0 0.0)
  ;; !!!write painter here!!!
  ;((square-limit wave/smile 4) frame)
  ((square-limit2 wave 4) frame)
  (gl-flush))

(define (main args)
  (glut-init args)
  (glut-create-window "Painter")
  (glut-display-func draw)
  (init)
  (glut-main-loop))

