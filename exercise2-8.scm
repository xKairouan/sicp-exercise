;; -*- coding: utf-8 -*-

;; *Exercise 2.8:* Using reasoning analogous to Alyssa's, describe
;; how the difference of two intervals may be computed.  Define a
;; corresponding subtraction procedure, called `sub-interval'.

(define (make-interval a b) (cons a b))

(define (upper-bound interval) (cdr interval))
(define (lower-bound interval) (car interval))

;; answer from here
(define (sub-interval x y)
  (make-interval (- (lower-bound x) (upper-bound y))
                 (- (upper-bound x) (lower-bound y))))

(use gauche.test)
(test-start "exercise 2.8")
(let ([result (sub-interval (make-interval 8 10)
                            (make-interval 2 5))])
  (test* "sub-interval - both plus" 3 (lower-bound result))
  (test* "sub-interval - both plus" 8 (upper-bound result))
  (test* "sub-interval - both plus (construction)"
         #t
         (> (upper-bound result) (lower-bound result))))
(let ([result (sub-interval (make-interval -3 -1)
                            (make-interval -10 -5))])
  (test* "sub-interval - both minus" 2 (lower-bound result))
  (test* "sub-interval - both minus" 9 (upper-bound result))
  (test* "sub-interval - both minus (construction)"
         #t
         (> (upper-bound result) (lower-bound result))))
(let ([result (sub-interval (make-interval -3 -1)
                            (make-interval 8 9))])
  (test* "sub-interval - x signed" -12 (lower-bound result))
  (test* "sub-interval - x signed" -9 (upper-bound result))
  (test* "sub-interval - x signed (construction)"
         #t
         (> (upper-bound result) (lower-bound result))))
(let ([result (sub-interval (make-interval 8 9)
                            (make-interval -3 -1))])
  (test* "sub-interval - y signed" 9 (lower-bound result))
  (test* "sub-interval - y signed" 12 (upper-bound result))
  (test* "sub-interval - y signed (construction)"
         #t
         (> (upper-bound result) (lower-bound result))))
(test-end)
