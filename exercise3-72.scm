;; -*- coding: utf-8 -*-

;; *Exercise 3.72:* In a similar way to *Note Exercise 3-71::
;; generate a stream of all numbers that can be written as the sum of
;; two squares in three different ways (showing how they can be so
;; written).

(load "./stream.scm")

(define (merge-weighted s1 s2 weight)
  (cond ((stream-null? s1) s2)
        ((stream-null? s2) s1)
        (else
         (let ((s1car (stream-car s1))
               (s2car (stream-car s2)))
           (let ((w1 (apply weight s1car))
                 (w2 (apply weight s2car)))
             (if (< w1 w2)
                 (cons-stream s1car
                              (merge-weighted (stream-cdr s1) s2 weight))
                 (cons-stream s2car
                              (merge-weighted s1 (stream-cdr s2) weight))))))))

(define (weighted-pairs s t weight)
  (cons-stream
   (list (stream-car s) (stream-car t))
   (merge-weighted (stream-map (lambda (x) (list (stream-car s) x))
                               (stream-cdr t))
                   (weighted-pairs (stream-cdr s) (stream-cdr t) weight)
                   weight)))

(define ones (cons-stream 1 ones))
(define integers (cons-stream 1
                              (stream-map + ones integers)))


;; answer
(define (generate-sum-of-square-stream s)
  (define (square x) (* x x))
  (define (sum-of-square x y) (+ (square x) (square y)))
  (let ((s0 (stream-ref s 0))
        (s1 (stream-ref s 1))
        (s2 (stream-ref s 2)))
    (let ((sc0 (sum-of-square (car s0) (cadr s0)))
          (sc1 (sum-of-square (car s1) (cadr s1)))
          (sc2 (sum-of-square (car s2) (cadr s2))))
      (if (= sc0 sc1 sc2)
          (cons-stream sc0
                       (generate-sum-of-square-stream
                        (stream-filter (lambda (pair)
                                         (not (= sc0
                                                 (sum-of-square (car pair)
                                                                (cadr pair)))))
                                       s)))
          (generate-sum-of-square-stream (stream-cdr s))))))

;; sum of two square!!!
(define sum-of-square-stream
  (generate-sum-of-square-stream (weighted-pairs integers
                                                 integers
                                                 (lambda (i j) (+ (* i i)
                                                                  (* j j))))))
;; => (325 425 650 725 845 850 925 1025 1105 1250)
(print (stream->list (stream-take sum-of-square-stream 10)))