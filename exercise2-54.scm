;; -*- coding: utf-8 -*-

;; *Exercise 2.54:* Two lists are said to be `equal?' if they contain
;; equal elements arranged in the same order.  For example,

;;      (equal? '(this is a list) '(this is a list))

;; is true, but

;;      (equal? '(this is a list) '(this (is a) list))

;; is false.  To be more precise, we can define `equal?'  recursively
;; in terms of the basic `eq?' equality of symbols by saying that `a'
;; and `b' are `equal?' if they are both symbols and the symbols are
;; `eq?', or if they are both lists such that `(car a)' is `equal?'
;; to `(car b)' and `(cdr a)' is `equal?' to `(cdr b)'.  Using this
;; idea, implement `equal?' as a procedure.(5)

(define (my-equal? list1 list2)
  (cond [(null? list1) (null? list2)]
        [(list? (car list1))
         (and (list? (car list2))
              (my-equal? (car list1) (car list2))
              (my-equal? (cdr list1) (cdr list2)))]
        [else (and (eq? (car list1) (car list2))
                   (my-equal? (cdr list1) (cdr list2)))]))

;; test
(use gauche.test)
(test-start "exercise 2.54")
(test* "my-equal?" #t (my-equal? '(a b c) '(a b c)))
(test* "my-equal?" #f (my-equal? '(a b c) '(a b c d)))
(test* "my-equal? (list contained)" #t (my-equal? '(a (b c) d) '(a (b c) d)))
(test* "my-equal? (list contained)" #f (my-equal? '(a (b c) d) '(a (b c) e)))
(test* "my-equal? (list contained)" #f (my-equal? '(a (b c) d) '(a (b d) d)))
(test* "my-equal? (list contained)" #f (my-equal? '(a (b c) d) '(a b c d)))
(test-end)