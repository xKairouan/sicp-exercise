;; -*- coding: utf-8 -*-

;; *Exercise 2.70:* The following eight-symbol alphabet with
;; associated relative frequencies was designed to efficiently encode
;; the lyrics of 1950s rock songs.  (Note that the "symbols" of an
;; "alphabet" need not be individual letters.)

;;      A     2 NA   16
;;      BOOM  1 SHA  3
;;      GET   2 YIP  9
;;      JOB   2 WAH  1

;; Use `generate-huffman-tree' (*Note Exercise 2-69::) to generate a
;; corresponding Huffman tree, and use `encode' (*Note Exercise
;; 2-68::) to encode the following message:

;;      Get a job

;;      Sha na na na na na na na na

;;      Get a job

;;      Sha na na na na na na na na

;;      Wah yip yip yip yip yip yip yip yip yip

;;      Sha boom

;; How many bits are required for the encoding?  What is the smallest
;; number of bits that would be needed to encode this song if we used
;; a fixed-length code for the eight-symbol alphabet?

(define (make-leaf symbol weight)
  (list 'leaf symbol weight))
(define (leaf? object)
  (eq? (car object) 'leaf))
(define (symbol-leaf x) (cadr x))
(define (weight-leaf x) (caddr x))

;; tree
(define (make-code-tree left right)
  (list left
        right
        (append (symbols left) (symbols right))
        (+ (weight left) (weight right))))
(define (left-branch tree) (car tree))
(define (right-branch tree) (cadr tree))
(define (symbols tree)
  (if (leaf? tree)
      (list (symbol-leaf tree))
      (caddr tree)))
(define (weight tree)
  (if (leaf? tree)
      (weight-leaf tree)
      (cadddr tree)))

;; encode
(define (encode message tree)
  (if (null? message)
      '()
      (append (encode-symbol (car message) tree)
              (encode (cdr message) tree))))

(define (encode-symbol sym tree)
  (define (choose-branch sym tree)
    (cond [(memq sym (symbols (left-branch tree)))
           (cons 0 (left-branch tree))]
          [(memq sym (symbols (right-branch tree)))
           (cons 1 (right-branch tree))]
          [else (error "symbol not in code tree -- CHOOSE-BRANCH" sym)]))
  (define (encode-symbol-iter tree result-code)
    (let ((result (choose-branch sym tree)))
      (let ((code (car result))
            (next-branch (cdr result)))
        (if (leaf? next-branch)
            (append result-code (list code))
            (encode-symbol-iter next-branch (append result-code (list code)))))))
  (encode-symbol-iter tree '()))

;; decode
(define (decode bits tree)
  (define (choose-branch bit branch)
    (cond ((= bit 0) (left-branch branch))
          ((= bit 1) (right-branch branch))
          (else (error "bad bit -- CHOOSE-BRANCH" bit))))
  (define (decode-1 bits current-branch)
    (if (null? bits)
        '()
        (let ((next-branch
               (choose-branch (car bits) current-branch)))
          (if (leaf? next-branch)
              (cons (symbol-leaf next-branch)
                    (decode-1 (cdr bits) tree))
              (decode-1 (cdr bits) next-branch)))))
  (decode-1 bits tree))

;; huffman code generator
(define (generate-huffman-tree pairs)
  (successive-merge (make-leaf-set pairs)))

(define (adjoin-set x set)
  (cond ((null? set) (list x))
        ((< (weight x) (weight (car set))) (cons x set))
        (else (cons (car set)
                    (adjoin-set x (cdr set))))))

(define (make-leaf-set pairs)
  (if (null? pairs)
      '()
      (let ((pair (car pairs)))
        (adjoin-set (make-leaf (car pair)    ; symbol
                               (cadr pair))  ; frequency
                    (make-leaf-set (cdr pairs))))))

(define (successive-merge leaf-set)
  (if (= (length leaf-set) 1)
      (car leaf-set)
      (let ((first (car leaf-set))
            (second (cadr leaf-set)))
        (successive-merge (adjoin-set (make-code-tree first second)
                                      (cddr leaf-set))))))

;; answer from here
(define lyrics-tree
  (generate-huffman-tree '((A 2) (BOOM 1) (GET 2) (JOB 2)
                           (NA 16) (SHA 3) (YIP 9) (WAH 1))))

;; result:
;; (1 1 1 1 1 1 1 0 0 1 1 1 1 0 1 1 1 0 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 0 0 1 1 1 1 0 1 1 1 0 0 0 0 0 0 0 0 0 1 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 1 1 0 1 1 0 1 1)
;; 84bit
;; fixed なら 3 * 36 = 108 bit 必要
(decode (encode '(GET A JOB
          SHA NA NA NA NA NA NA NA NA
          GET A JOB
          SHA NA NA NA NA NA NA NA NA
          WAH YIP YIP YIP YIP YIP YIP YIP YIP YIP
          SHA BOOM)
                lyrics-tree) lyrics-tree)