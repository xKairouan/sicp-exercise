;; -*- coding: utf-8 -*-

;; *Exercise 2.62:* Give a [theta](n) implementation of `union-set'
;; for sets represented as ordered lists.

(define (union-set set1 set2)
  (define (union-set-iter set1 set2 result)
    (cond [(null? set1) (append result set2)]
          [(null? set2) (append result set1)]
          [(< (car set1) (car set2))
           (union-set-iter (cdr set1) set2 (append result (list (car set1))))]
          [(= (car set1) (car set2))
           (union-set-iter (cdr set1) (cdr set2) (append result (list (car set1))))]
          [(> (car set1) (car set2))
           (union-set-iter set1 (cdr set2) (append result (list (car set2))))]))
  (union-set-iter set1 set2 '()))


(use gauche.test)
(test-start "exercise 2.62")
(test* "union-set, () () => ()" '() (union-set '() '()))
(test* "union-set, (1 2) (3 4) => (1 2 3 4)" '(1 2 3 4) (union-set '(1 2) '(3 4)))
(test* "union-set, (1 3) (2 4) => (1 2 3 4)" '(1 2 3 4) (union-set '(1 3) '(2 4)))
(test* "union-set, (1 3) (2) => (1 2 3)" '(1 2 3) (union-set '(1 3) '(2)))
(test* "union-set, (100 101) (3 4) => (3 4 100 101)" '(3 4 100 101) (union-set '(100 101) '(3 4)))
(test-end)