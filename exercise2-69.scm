;; -*- coding: utf-8 -*-

;; *Exercise 2.69:* The following procedure takes as its argument a
;; list of symbol-frequency pairs (where no symbol appears in more
;; than one pair) and generates a Huffman encoding tree according to
;; the Huffman algorithm.

;;      (define (generate-huffman-tree pairs)
;;        (successive-merge (make-leaf-set pairs)))

;; `Make-leaf-set' is the procedure given above that transforms the
;; list of pairs into an ordered set of leaves.  `Successive-merge'
;; is the procedure you must write, using `make-code-tree' to
;; successively merge the smallest-weight elements of the set until
;; there is only one element left, which is the desired Huffman tree.
;; (This procedure is slightly tricky, but not really complicated.
;; If you find yourself designing a complex procedure, then you are
;; almost certainly doing something wrong.  You can take significant
;; advantage of the fact that we are using an ordered set
;; representation.)

;; leaf
(define (make-leaf symbol weight)
  (list 'leaf symbol weight))
(define (leaf? object)
  (eq? (car object) 'leaf))
(define (symbol-leaf x) (cadr x))
(define (weight-leaf x) (caddr x))

;; tree
(define (make-code-tree left right)
  (list left
        right
        (append (symbols left) (symbols right))
        (+ (weight left) (weight right))))
(define (left-branch tree) (car tree))
(define (right-branch tree) (cadr tree))
(define (symbols tree)
  (if (leaf? tree)
      (list (symbol-leaf tree))
      (caddr tree)))
(define (weight tree)
  (if (leaf? tree)
      (weight-leaf tree)
      (cadddr tree)))

;; encode
(define (encode message tree)
  (if (null? message)
      '()
      (append (encode-symbol (car message) tree)
              (encode (cdr message) tree))))

(define (encode-symbol sym tree)
  (define (choose-branch sym tree)
    (cond [(memq sym (symbols (left-branch tree)))
           (cons 0 (left-branch tree))]
          [(memq sym (symbols (right-branch tree)))
           (cons 1 (right-branch tree))]
          [else (error "symbol not in code tree -- CHOOSE-BRANCH" sym)]))
  (define (encode-symbol-iter tree result-code)
    (let ((result (choose-branch sym tree)))
      (let ((code (car result))
            (next-branch (cdr result)))
        (if (leaf? next-branch)
            (append result-code (list code))
            (encode-symbol-iter next-branch (append result-code (list code)))))))
  (encode-symbol-iter tree '()))

;; decode
(define (decode bits tree)
  (define (choose-branch bit branch)
    (cond ((= bit 0) (left-branch branch))
          ((= bit 1) (right-branch branch))
          (else (error "bad bit -- CHOOSE-BRANCH" bit))))
  (define (decode-1 bits current-branch)
    (if (null? bits)
        '()
        (let ((next-branch
               (choose-branch (car bits) current-branch)))
          (if (leaf? next-branch)
              (cons (symbol-leaf next-branch)
                    (decode-1 (cdr bits) tree))
              (decode-1 (cdr bits) next-branch)))))
  (decode-1 bits tree))

;; answer from here
(define (generate-huffman-tree pairs)
  (successive-merge (make-leaf-set pairs)))

(define (adjoin-set x set)
  (cond ((null? set) (list x))
        ((< (weight x) (weight (car set))) (cons x set))
        (else (cons (car set)
                    (adjoin-set x (cdr set))))))

(define (make-leaf-set pairs)
  (if (null? pairs)
      '()
      (let ((pair (car pairs)))
        (adjoin-set (make-leaf (car pair)    ; symbol
                               (cadr pair))  ; frequency
                    (make-leaf-set (cdr pairs))))))

(define (successive-merge leaf-set)
  (if (= (length leaf-set) 1)
      (car leaf-set)
      (let ((first (car leaf-set))
            (second (cadr leaf-set)))
        (successive-merge (adjoin-set (make-code-tree first second)
                                      (cddr leaf-set))))))

;; test
(use gauche.test)
(test-start "exercise 2.69")
(let ((tree (generate-huffman-tree '((A 4) (B 2) (C 1) (D 1))))
      (message '(A D A B B C A)))
  (test* "generate-huffman-tree"
         message
         (decode (encode message tree) tree)))
(test-end)