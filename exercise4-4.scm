;; -*- coding: utf-8 -*-

;; *Exercise 4.4:* Recall the definitions of the special forms `and'
;; and `or' from *Note Chapter 1:::

;;    * `and': The expressions are evaluated from left to right.  If
;;      any expression evaluates to false, false is returned; any
;;      remaining expressions are not evaluated.  If all the
;;      expressions evaluate to true values, the value of the last
;;      expression is returned.  If there are no expressions then
;;      true is returned.

;;    * `or': The expressions are evaluated from left to right.  If
;;      any expression evaluates to a true value, that value is
;;      returned; any remaining expressions are not evaluated.  If
;;      all expressions evaluate to false, or if there are no
;;      expressions, then false is returned.


;; Install `and' and `or' as new special forms for the evaluator by
;; defining appropriate syntax procedures and evaluation procedures
;; `eval-and' and `eval-or'.  Alternatively, show how to implement
;; `and' and `or' as derived expressions.

(load "./metacircular.scm")

(define (and? exp) (tagged-list? exp 'and))
(define (and-args exp) (cdr exp))
(define (and-first-arg args) (car args))
(define (and-rest-args args) (cdr args))
(define (eval-and exp env)
  (define (iter args result)
    (if (null? args)
        result
        (let ((value (eval (and-first-arg args) env))
              (rest (and-rest-args args)))
          (if (true? value)
              (iter rest value)
              'false))))
  (iter (and-args exp) 'true))

(define (or? exp) (tagged-list? exp 'or))
(define (or-args exp) (cdr exp))
(define (or-first-arg args) (car args))
(define (or-rest-args args) (cdr args))
(define (eval-or exp env)
  (define (iter args resulf)
    (if (null? args)
        result
        (let ((value (eval (or-first-arg args) env))
              (rest (or-rest-args args)))
          (if (true? value)
              value
              (iter rest value)))))
  (iter (or-args exp) 'false))


;; derived
(define (eval-and2 exp env)
  (eval (and->if exp) env))
(define (and->if exp)
  (define (expand-and-clauses args result)
    (if (null? args)
        result
        (let ((first (and-first-arg args))
              (rest (and-rest-args args)))
          (make-if first
                   (expand-and-clauses rest first)
                   'false))))
  (expand-and-clauses (and-args exp) 'true))

(define (eval-or2 exp env)
  (eval (or->if exp) env))
(define (or->if exp)
  (define (expand-or-clauses args result)
    (if (null? args)
        result
        (let ((first (or-first-arg args))
              (rest (or-rest-args args)))
          (make-if first
                   first
                   (expand-or-clauses rest first))))))

(define (eval exp env)
  (cond [(self-evaluating? exp) exp]
        [(variable? exp) (lookup-variable-value exp env)]
        [(quoted? exp) (text-of-quotation exp)]
        [(assignment? exp) (eval-assignment exp env)]
        [(definition? exp) (eval-definition exp env)]
        [(if? exp) (eval-if exp env)]
        [(lambda? exp)
         (make-procedure (lambda-parameters exp)
                         (lambda-body exp)
                         env)]
        [(begin? exp)
         (eval-sequence (begin-actions exp) env)]
        [(cond? exp) (eval (cond->if exp) env)]
        ;; [(and? exp) (eval-and exp env)]
        ;; [(or? exp) (eval-or exp env)]
        [(and? exp) (eval-and2 exp env)]
        [(or? exp) (eval-or2 exp env)]
        [(application? exp)
         (apply (eval (operator exp) env)
                (list-of-values (operands exp) env))]
        [else
         (error "Unknown expression type -- EVAL" exp)]))


(driver-loop)
