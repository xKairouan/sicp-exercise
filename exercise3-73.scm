;; -*- coding: utf-8 -*-

;; *Exercise 3.73:* We can model electrical circuits using streams to
;; represent the values of currents or voltages at a sequence of
;; times.  For instance, suppose we have an "RC circuit" consisting
;; of a resistor of resistance R and a capacitor of capacitance C in
;; series.  The voltage response v of the circuit to an injected
;; current i is determined by the formula in *Note Figure 3-33::,
;; whose structure is shown by the accompanying signal-flow diagram.

;; Write a procedure `RC' that models this circuit.  `RC' should take
;; as inputs the values of R, C, and dt and should return a procedure
;; that takes as inputs a stream representing the current i and an
;; initial value for the capacitor voltage v_0 and produces as output
;; the stream of voltages v.  For example, you should be able to use
;; `RC' to model an RC circuit with R = 5 ohms, C = 1 farad, and a
;; 0.5-second time step by evaluating `(define RC1 (RC 5 1 0.5))'.
;; This defines `RC1' as a procedure that takes a stream representing
;; the time sequence of currents and an initial capacitor voltage and
;; produces the output stream of voltages.

;; *Figure 3.33:* An RC circuit and the associated signal-flow
;; diagram.

;;        +                 -
;;       ->----'\/\/\,---| |---
;;        i                 C

;;                    / t
;;                    |  i
;;       v  =  v   +  |      dt + R i
;;              0     |
;;                    / 0

;;               +--------------+
;;           +-->|   scale: R   |---------------------+   |\_
;;           |   +--------------+                     |   |  \_
;;           |                                        +-->|    \   v
;;        i  |   +--------------+     +------------+      | add >--->
;;       ----+-->|  scale: 1/C  |---->|  integral  |----->|   _/
;;               +--------------+     +------------+      | _/
;;                                                        |/

(load "./stream.scm")

(define (add-streams s1 s2)
  (stream-map + s1 s2))
(define (scale-stream s factor)
  (stream-map (lambda (x) (* x factor)) s))

(define (integral integrand initial-value dt)
  (define int
    (cons-stream initial-value
                 (add-streams (scale-stream integrand dt)
                              int)))
  int)

(define (RC resistance capacity dt)
  (lambda (i-stream v0)
    (add-streams (scale-stream i-stream resistance)
                 (integral (scale-stream i-stream (/ 1 capacity)) v0 dt))))

(use math.const)
(define istream (cons-stream
                 0
                 (cons-stream
                  (sin (/ pi 4))
                  (cons-stream
                   1
                   (cons-stream
                    (sin (/ pi 4))
                    (stream-map - istream))))))
(print (stream->list (stream-take
                      ((RC 5 1 0.5) istream 1.0)
                      100)))
