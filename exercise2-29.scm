;; -*- coding: utf-8 -*-

;; *Exercise 2.29:* A binary mobile consists of two branches, a left
;; branch and a right branch.  Each branch is a rod of a certain
;; length, from which hangs either a weight or another binary mobile.
;; We can represent a binary mobile using compound data by
;; constructing it from two branches (for example, using `list'):

;;      (define (make-mobile left right)
;;        (list left right))

;; A branch is constructed from a `length' (which must be a number)
;; together with a `structure', which may be either a number
;; (representing a simple weight) or another mobile:

;;      (define (make-branch length structure)
;;        (list length structure))

;;   a. Write the corresponding selectors `left-branch' and
;;      `right-branch', which return the branches of a mobile, and
;;      `branch-length' and `branch-structure', which return the
;;      components of a branch.

;;   b. Using your selectors, define a procedure `total-weight' that
;;      returns the total weight of a mobile.

;;   c. A mobile is said to be "balanced" if the torque applied by
;;      its top-left branch is equal to that applied by its top-right
;;      branch (that is, if the length of the left rod multiplied by
;;      the weight hanging from that rod is equal to the
;;      corresponding product for the right side) and if each of the
;;      submobiles hanging off its branches is balanced. Design a
;;      predicate that tests whether a binary mobile is balanced.

;;   d. Suppose we change the representation of mobiles so that the
;;      constructors are

;;           (define (make-mobile left right)
;;             (cons left right))

;;           (define (make-branch length structure)
;;             (cons length structure))

;;      How much do you need to change your programs to convert to
;;      the new representation?

(define (make-mobile left right)
  (list left right))

(define (make-branch length structure)
  (list length structure))

;; a.
(define (left-branch mobile)
  (car mobile))
(define (right-branch mobile)
  (car (cdr mobile)))

(define (branch-length branch)
  (car branch))
(define (branch-structure branch)
  (car (cdr branch)))

(use gauche.test)
(test-start "exercise 2.29 a")
(let* ((branch-a (make-branch 1 2))
       (branch-b (make-branch 2 1))
       (mobile (make-mobile branch-a branch-b)))
  (test* "left-branch" branch-a (left-branch mobile))
  (test* "right-branch" branch-b (right-branch mobile))
  (test* "branch-length" 1 (branch-length branch-a))
  (test* "branch-structure" 2 (branch-structure branch-a)))
(test-end)

;; b.
(define (mobile? x)
  (pair? x))
(define (branch-weight branch)
  (let ((structure (branch-structure branch)))
    (if (mobile? structure)
        (+ (branch-weight (left-branch structure))
           (branch-weight (right-branch structure)))
        structure)))

(define (total-weight mobile)
  (+ (branch-weight (left-branch mobile))
     (branch-weight (right-branch mobile))))

(test-start "exercise 2.29 b")
(let* ((branch-a (make-branch 1 2))
       (branch-b (make-branch 2 1))
       (mobile (make-mobile branch-a branch-b)))
  (test* "total-weight" 3 (total-weight mobile))
  (test* "total-weight 2"
         6
         (total-weight (make-mobile (make-branch 1 (make-mobile branch-a branch-b))
                                    (make-branch 2 (make-mobile branch-b branch-a))))))
(test-end)

;; c.
(define (balanced-branch? branch)
  (let ((structure (branch-structure branch)))
    (if (mobile? structure)
        (balanced? structure)
        #t)))

(define (balanced? mobile)
  (let ((left (left-branch mobile))
        (right (right-branch mobile)))
    (and (balanced-branch? left)
         (balanced-branch? right)
         (= (* (branch-length left) (branch-weight left))
            (* (branch-length right) (branch-weight right))))))

(test-start "exercise 2.29 c")
(let* ((branch-a (make-branch 1 2))
       (branch-b (make-branch 2 1))
       (mobile (make-mobile branch-a branch-b))
       (mobile2 (make-mobile (make-branch 1 2)
                             (make-branch 3 1)))
       (mobile3 (make-mobile (make-branch 4 mobile)
                             (make-branch 2 (make-mobile (make-branch 1 mobile)
                                                         (make-branch 1 mobile))))))
  (test* "balanced?" #t (balanced? mobile))
  (test* "balanced? 2" #f (balanced? mobile2))
  (test* "balanced? 3" #t (balanced? mobile3)))
(test-end)

;; d.
(define (make-mobile left right)
  (cons left right))
(define (make-branch length structure)
  (cons length structure))

(define (left-branch mobile)
  (car mobile))
(define (right-branch mobile)
  (cdr mobile))
(define (branch-length branch)
  (car branch))
(define (branch-structure branch)
  (cdr branch))

(test-start "exercise 2.29 d")
(let* ((branch-a (make-branch 1 2))
       (branch-b (make-branch 2 1))
       (mobile (make-mobile branch-a branch-b)))
  (test* "left-branch" branch-a (left-branch mobile))
  (test* "right-branch" branch-b (right-branch mobile))
  (test* "branch-length" 1 (branch-length branch-a))
  (test* "branch-structure" 2 (branch-structure branch-a)))
(let* ((branch-a (make-branch 1 2))
       (branch-b (make-branch 2 1))
       (mobile (make-mobile branch-a branch-b)))
  (test* "total-weight" 3 (total-weight mobile))
  (test* "total-weight 2"
         6
         (total-weight (make-mobile (make-branch 1 (make-mobile branch-a branch-b))
                                    (make-branch 2 (make-mobile branch-b branch-a))))))
(let* ((branch-a (make-branch 1 2))
       (branch-b (make-branch 2 1))
       (mobile (make-mobile branch-a branch-b))
       (mobile2 (make-mobile (make-branch 1 2)
                             (make-branch 3 1)))
       (mobile3 (make-mobile (make-branch 4 mobile)
                             (make-branch 2 (make-mobile (make-branch 1 mobile)
                                                         (make-branch 1 mobile))))))
  (test* "balanced?" #t (balanced? mobile))
  (test* "balanced? 2" #f (balanced? mobile2))
  (test* "balanced? 3" #t (balanced? mobile3)))
(test-end)
