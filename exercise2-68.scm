;; -*- coding: utf-8 -*-

;; *Exercise 2.68:* The `encode' procedure takes as arguments a
;; message and a tree and produces the list of bits that gives the
;; encoded message.

;;      (define (encode message tree)
;;        (if (null? message)
;;            '()
;;            (append (encode-symbol (car message) tree)
;;                    (encode (cdr message) tree))))

;; `Encode-symbol' is a procedure, which you must write, that returns
;; the list of bits that encodes a given symbol according to a given
;; tree.  You should design `encode-symbol' so that it signals an
;; error if the symbol is not in the tree at all.  Test your
;; procedure by encoding the result you obtained in *Note Exercise
;; 2-67:: with the sample tree and seeing whether it is the same as
;; the original sample message.

;; leaf
(define (make-leaf symbol weight)
  (list 'leaf symbol weight))
(define (leaf? object)
  (eq? (car object) 'leaf))
(define (symbol-leaf x) (cadr x))
(define (weight-leaf x) (caddr x))

;; tree
(define (make-code-tree left right)
  (list left
        right
        (append (symbols left) (symbols right))
        (+ (weight left) (weight right))))
(define (left-branch tree) (car tree))
(define (right-branch tree) (cadr tree))
(define (symbols tree)
  (if (leaf? tree)
      (list (symbol-leaf tree))
      (caddr tree)))
(define (weight tree)
  (if (leaf? tree)
      (weight-leaf tree)
      (cadddr tree)))

;; answer from here.
(define (encode message tree)
  (if (null? message)
      '()
      (append (encode-symbol (car message) tree)
              (encode (cdr message) tree))))

(define (encode-symbol sym tree)
  (define (choose-branch sym tree)
    (cond [(memq sym (symbols (left-branch tree)))
           (cons 0 (left-branch tree))]
          [(memq sym (symbols (right-branch tree)))
           (cons 1 (right-branch tree))]
          [else (error "symbol not in code tree -- CHOOSE-BRANCH" sym)]))
  (define (encode-symbol-iter tree result-code)
    (let ((result (choose-branch sym tree)))
      (let ((code (car result))
            (next-branch (cdr result)))
        (if (leaf? next-branch)
            (append result-code (list code))
            (encode-symbol-iter next-branch (append result-code (list code)))))))
  (encode-symbol-iter tree '()))

;; data
(define sample-tree
  (make-code-tree (make-leaf 'A 4)
                  (make-code-tree
                   (make-leaf 'B 2)
                   (make-code-tree (make-leaf 'D 1)
                                   (make-leaf 'C 1)))))

(define sample-message '(0 1 1 0 0 1 0 1 0 1 1 1 0))

(use gauche.test)
(test-start "exercise 2.68")
(test* "encode"
       '(0 1 1 0 0 1 0 1 0 1 1 1 0)
       (encode '(A D A B B C A) sample-tree))
(test-end)