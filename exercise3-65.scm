;; -*- coding: utf-8 -*-

;; *Exercise 3.65:* Use the series

;;                  1     1     1
;;      ln 2 = 1 - --- + --- - --- + ...
;;                  2     3     4

;; to compute three sequences of approximations to the natural
;; logarithm of 2, in the same way we did above for [pi].  How
;; rapidly do these sequences converge?

(load "./stream.scm")

;; normal
;; => (1.0 0.5 0.8333333333333333 0.5833333333333333 0.7833333333333332 0.6166666666666666 0.7595238095238095 0.6345238095238095 0.7456349206349207 0.6456349206349207)
(define (partial-sums stream)
  (cons-stream (stream-car stream)
               (stream-map + (stream-cdr stream) (partial-sums stream))))

(define (ln2-summands n)
  (cons-stream (/ 1.0 n)
               (stream-map - (ln2-summands (+ n 1)))))
(define ln2-stream
  (partial-sums (ln2-summands 1)))
(print (stream->list (stream-take ln2-stream 10)))


;; fast (use euler accelerate)
;; => (0.7 0.6904761904761905 0.6944444444444444 0.6924242424242424 0.6935897435897436 0.6928571428571428 0.6933473389355742 0.6930033416875522 0.6932539682539683 0.6930657506744464)
(define (euler-transform s)
  (define (square x) (* x x))
  (let ((s0 (stream-ref s 0))           ; S_(n-1)
        (s1 (stream-ref s 1))           ; S_n
        (s2 (stream-ref s 2)))          ; S_(n+1)
    (cons-stream (- s2 (/ (square (- s2 s1))
                          (+ s0 (* -2 s1) s2)))
                 (euler-transform (stream-cdr s)))))
(print (stream->list (stream-take (euler-transform ln2-stream) 10)))

;; more fast (euler transform recursively)
;; => (1.0 0.7 0.6932773109243697 0.6931488693329254 0.6931471960735491 0.6931471806635636 0.6931471805604039 0.6931471805599445 0.6931471805599427 0.6931471805599454)
(define (make-tableau transform s)
  (cons-stream s
               (make-tableau transform
                             (transform s))))
(define (accelerated-sequence transform s)
  (stream-map stream-car
              (make-tableau transform s)))
(print (stream->list (stream-take (accelerated-sequence euler-transform ln2-stream) 10)))
