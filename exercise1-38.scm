;; -*- coding: utf-8 -*-

;; *Exercise 1.38:* In 1737, the Swiss mathematician Leonhard Euler
;; published a memoir `De Fractionibus Continuis', which included a
;; continued fraction expansion for e - 2, where e is the base of the
;; natural logarithms.  In this fraction, the n_i are all 1, and the
;; D_i are successively 1, 2, 1, 1, 4, 1, 1, 6, 1, 1, 8, ....  Write
;; a program that uses your `cont-frac' procedure from *Note Exercise
;; 1-37:: to approximate e, based on Euler's expansion.

(define (cont-frac n d k)
  (define (cont-frac-iterative i result)
    (if (= i 0)
        result
        (cont-frac-iterative (- i 1)
                             (/. (n i) (+ (d i) result)))))
  (cont-frac-iterative (- k 1) (/. (n k) (d k))))

(define e
  (cont-frac (lambda (i) 1)
             (lambda (i)
               (cond [(= (remainder i 3) 2) (* 2 (+ 1 (quotient i 3)))]
                     [else 1]))
             100))
