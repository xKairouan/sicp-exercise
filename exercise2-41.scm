;; -*- coding: utf-8 -*-

;; *Exercise 2.41:* Write a procedure to find all ordered triples of
;; distinct positive integers i, j, and k less than or equal to a
;; given integer n that sum to a given integer s.

(define (flatmap proc seq)
  (fold-right append '() (map proc seq)))

(define (enumerate-interval low high)
  (define (iter n result)
    (if (< n low)
        result
        (iter (- n 1) (cons n result))))
  (iter high '()))

(define (unique-triple n)
  (flatmap (lambda (i)
             (flatmap (lambda (j)
                        (map (lambda (k)
                               (list i j k))
                             (enumerate-interval 1 (- j 1))))
                      (enumerate-interval 1 (- i 1))))
           (enumerate-interval 1 n)))

(define (find-triple-sum n s)
  (filter (lambda (triple)
            (= (+ (car triple) (cadr triple) (caddr triple))
               s))
          (unique-triple n)))

;; test
(use gauche.test)
(test-start "exercise 2-41")
(test* "find-triple-sum" '((5 3 2) (5 4 1) (6 3 1)) (find-triple-sum 6 10))
(test-end)
