;; -*- coding: utf-8 -*-

;; *Exercise 3.58:* Give an interpretation of the stream computed by
;; the following procedure:

;;      (define (expand num den radix)
;;        (cons-stream
;;         (quotient (* num radix) den)
;;         (expand (remainder (* num radix) den) den radix)))

;; (`Quotient' is a primitive that returns the integer quotient of two
;; integers.)  What are the successive elements produced by `(expand
;; 1 7 10)'?  What is produced by `(expand 3 8 10)'?

(load "./stream.scm")

;; これは分数 num/den の小数部 radix 進法のもとでを展開する手続き
(define (expand num den radix)
  (cons-stream
   (quotient (* num radix) den)
   (expand (remainder (* num radix) den) den radix)))

(use gauche.test)
(test-start "exercise 3.58")
(test* "1/7 = 0.14285714..."
       '(1 4 2 8 5 7 1 4)
       (stream->list (stream-take (expand 1 7 10) 8)))
(test* "3/8 = 0.375"
       '(3 7 5 0 0 0 0 0)
       (stream->list (stream-take (expand 3 8 10) 8)))
(test-end)
