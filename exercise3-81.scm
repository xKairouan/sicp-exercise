;; -*- coding: utf-8 -*-

;; *Exercise 3.81:* *Note Exercise 3-6:: discussed generalizing the
;; random-number generator to allow one to reset the random-number
;; sequence so as to produce repeatable sequences of "random"
;; numbers.  Produce a stream formulation of this same generator that
;; operates on an input stream of requests to `generate' a new random
;; number or to `reset' the sequence to a specified value and that
;; produces the desired stream of random numbers.  Don't use
;; assignment in your solution.

(load "./stream.scm")


(define random-init 1)
(define (random-update x)
  (modulo (+ (* 69069 x) 1) #x100000000))

(define (generate-random-numbers request-stream)
  (define (next stream prev-value)
    (let ((current-value (stream-car stream)))
      (cond [(number? current-value)
             (cons-stream current-value
                          (next (stream-cdr stream) current-value))]
            [(eq? current-value 'generate)
             (let ((v (random-update prev-value)))
               (cons-stream v
                            (next (stream-cdr stream) v)))])))
  (next request-stream random-init))

(define (list->stream lis)
  (if (null? lis)
      the-empty-stream
      (cons-stream (car lis) (list->stream (cdr lis)))))