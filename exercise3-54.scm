;; -*- coding: utf-8 -*-

;; *Exercise 3.54:* Define a procedure `mul-streams', analogous to
;; `add-streams', that produces the elementwise product of its two
;; input streams.  Use this together with the stream of `integers' to
;; complete the following definition of the stream whose nth element
;; (counting from 0) is n + 1 factorial:

;;      (define factorials (cons-stream 1 (mul-streams <??> <??>)))


(use util.stream)

(define ones (stream-cons 1 ones))

(define (add-streams s1 s2)
  (stream-map + s1 s2))

(define integers (cons-stream 1 (add-streams ones integers)))

(define (mul-streams s1 s2)
  (stream-map * s1 s2))

(define factorials
  (stream-cons 1 (mul-streams (stream-cdr integers) factorials)))

;; test
(use gauche.test)
(test-start "exercise 3.54")
(test* "(1 2 6 24 120 720 5040 40320 362880 3628800)"
       '(1 2 6 24 120 720 5040 40320 362880 3628800)
       (stream->list (stream-take-while (lambda (x) (< x 5000000)) factorials)))
(test-end)
