;; -*- coding: utf-8 -*-

;; *Exercise 2.39:* Complete the following definitions of `reverse'
;; (*Note Exercise 2-18::) in terms of `fold-right' and `fold-left'
;; from *Note Exercise 2-38:::

;;      (define (reverse sequence)
;;        (fold-right (lambda (x y) <??>) nil sequence))

;;      (define (reverse sequence)
;;        (fold-left (lambda (x y) <??>) nil sequence))

(define (fold-left op init seq)
  (if (null? seq)
      init
      (fold-left op (op init (car seq)) (cdr seq))))

(define (my-reverse sequence)
  (fold-right (lambda (x y) (append y (list x))) '() sequence))

(define (my-reverse2 sequence)
  (fold-left (lambda (x y) (cons y x)) '() sequence))

;; test
(use gauche.test)
(test-start "exercise 2.39")
(test* "my-reverse (fold-right)" '(5 4 3 2 1) (my-reverse '(1 2 3 4 5)))
(test* "my-reverse2 (fold-left)" '(5 4 3 2 1) (my-reverse2 '(1 2 3 4 5)))
(test-end)
