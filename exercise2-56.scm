;; -*- coding: utf-8 -*-

;; *Exercise 2.56:* Show how to extend the basic differentiator to
;; handle more kinds of expressions.  For instance, implement the
;; differentiation rule

;;      n_1   n_2
;;      --- = ---  if and only if n_1 d_2 = n_2 d_1
;;      d_1   d_2

;; by adding a new clause to the `deriv' program and defining
;; appropriate procedures `exponentiation?', `base', `exponent', and
;; `make-exponentiation'.  (You may use the symbol `**' to denote
;; exponentiation.)  Build in the rules that anything raised to the
;; power 0 is 1 and anything raised to the power 1 is the thing
;; itself.

(define (variable? x) (symbol? x))

(define (same-variable? v1 v2)
  (and (variable? v1) (variable? v2) (eq? v1 v2)))

(define (make-sum a1 a2) (list '+ a1 a2))

(define (make-product m1 m2) (list '* m1 m2))

(define (sum? x)
  (and (pair? x) (eq? (car x) '+)))

(define (addend s) (cadr s))

(define (augend s) (caddr s))

(define (product? x)
  (and (pair? x) (eq? (car x) '*)))

(define (multiplier p) (cadr p))

(define (multiplicand p) (caddr p))

(define (make-sum a1 a2)
  (cond ((=number? a1 0) a2)
        ((=number? a2 0) a1)
        ((and (number? a1) (number? a2)) (+ a1 a2))
        (else (list '+ a1 a2))))

(define (=number? exp num)
  (and (number? exp) (= exp num)))

(define (make-product m1 m2)
  (cond ((or (=number? m1 0) (=number? m2 0)) 0)
        ((=number? m1 1) m2)
        ((=number? m2 1) m1)
        ((and (number? m1) (number? m2)) (* m1 m2))
        (else (list '* m1 m2))))


;; answer from here
(define (deriv exp var)
  (cond [(number? exp) 0]
        [(variable? exp)
         (if (same-variable? exp var) 1 0)]
        [(sum? exp)
         (make-sum (deriv (addend exp) var)
                   (deriv (augend exp) var))]
        [(product? exp)
         (make-sum
          (make-product (multiplier exp)
                        (deriv (multiplicand exp) var))
          (make-product (deriv (multiplier exp) var)
                        (multiplicand exp)))]
        [(exponentiation? exp)
         (let ((n (exponent exp))
               (u (base exp)))
           (make-product (make-product n (make-exponentiation u (- n 1)))
                         (deriv u var)))]
        [else
         (error "unknown expression type -- DERIV" exp)]))

(define (make-exponentiation b n)
  (cond [(=number? n 0) 0]
        [(=number? n 1) b]
        [else (list '** b n)]))
(define (exponentiation? exp)
  (and (pair? exp) (eq? (car exp) '**)))
(define (base x)
  (cadr x))
(define (exponent x)
  (caddr x))


(use gauche.test)
(test-start "exercise 2.56")
(test* "d(x^6)/dx = 6*x^5"
       (make-product 6 (make-exponentiation 'x 5))
       (deriv (make-exponentiation 'x 6) 'x))
(test* "d(x^0)/dx = 0" 0 (deriv (make-exponentiation 'x 0) 'x))
(test* "d(x^1)/dx = 1" 1 (deriv (make-exponentiation 'x 1) 'x))
(test-end)
