;; -*- coding: utf-8 -*-

;; *Exercise 3.34:* Louis Reasoner wants to build a squarer, a
;; constraint device with two terminals such that the value of
;; connector `b' on the second terminal will always be the square of
;; the value `a' on the first terminal.  He proposes the following
;; simple device made from a multiplier:

;;      (define (squarer a b)
;;        (multiplier a a b))

;; There is a serious flaw in this idea.  Explain.

(when (find (cut string=? <> ".") *load-path*)
      (add-load-path "."))
(use constraint)

(define (squarer a b)
  (multiplier a a b))

;; test
(define a (make-connector))
(define b (make-connector))
(squarer a b)
(probe "A" a)
(probe "B" b)
(set-value! a 13 'user)
(forget-value! a 'user)
(set-value! b 169 'user)

;; b が決定されても a が定まらない