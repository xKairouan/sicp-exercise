;; -*- coding: utf-8 -*-

;; *Exercise 3.18:* Write a procedure that examines a list and
;; determines whether it contains a cycle, that is, whether a program
;; that tried to find the end of the list by taking successive `cdr's
;; would go into an infinite loop.  *Note Exercise 3-13:: constructed
;; such lists.

(define (loop? lis)
  (let ((checked '()))
    (define (loop-inner lis)
      (cond [(null? lis) #f]
            [(memq lis checked) #t]
            [else
             (write/ss checked)
             (set! checked (cons lis checked))
             (loop-inner (cdr lis))]))
    (loop-inner lis)))

;; test data
(define x '(1 2 3))
(set-cdr! (last-pair x) x)

(define y '(1 2 3))
(set-cdr! (last-pair y) (cdr y))

(use gauche.test)
(test-start "exercise 3.18")
(test* "loop test -- false" #f (loop? '(1 2 3)))
(test* "loop test -- true" #t (loop? x))
(test* "loop test -- true" #t (loop? y))
(test-end)
