;; -*- coding: utf-8 -*-

;; *Exercise 2.2:* Consider the problem of representing line segments
;; in a plane.  Each segment is represented as a pair of points: a
;; starting point and an ending point.  Define a constructor
;; `make-segment' and selectors `start-segment' and `end-segment'
;; that define the representation of segments in terms of points.
;; Furthermore, a point can be represented as a pair of numbers: the
;; x coordinate and the y coordinate.  Accordingly, specify a
;; constructor `make-point' and selectors `x-point' and `y-point'
;; that define this representation.  Finally, using your selectors
;; and constructors, define a procedure `midpoint-segment' that takes
;; a line segment as argument and returns its midpoint (the point
;; whose coordinates are the average of the coordinates of the
;; endpoints).  To try your procedures, you'll need a way to print
;; points:

;;      (define (print-point p)
;;        (newline)
;;        (display "(")
;;        (display (x-point p))
;;        (display ",")
;;        (display (y-point p))
;;        (display ")"))

;; point
(define (make-point x y)
  (cons x y))

(define (x-point p)
  (car p))

(define (y-point p)
  (cdr p))

;; line segment
(define (make-segment starting-point ending-point)
  (cons starting-point ending-point))

(define (start-segment segment)
  (car segment))
(define (end-segment segment)
  (cdr segment))

(define (midpoint-segment segment)
  (define (average x y) (/ (+ x y) 2))
  (let ([p1 (start-segment segment)]
        [p2 (end-segment segment)])
    (make-point (average (x-point p1) (x-point p2))
                (average (y-point p1) (y-point p2)))))


;; test
(use gauche.test)
(test-start "exercise 2.2")
(let* ([s (make-point 1 2)]
       [e (make-point 3 2)]
       [segment (make-segment s e)]
       [midpoint (midpoint-segment segment)])
  (test* "midpoint ((1, 2), (3, 2)) => (2, 2)" 2 (x-point midpoint))
  (test* "midpoint ((1, 2), (3, 2)) => (2, 2)" 2 (y-point midpoint)))
(test-end)
