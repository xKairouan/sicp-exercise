;; -*- coding: utf-8 -*-

;; *Exercise 3.55:* Define a procedure `partial-sums' that takes as
;; argument a stream S and returns the stream whose elements are S_0,
;; S_0 + S_1, S_0 + S_1 + S_2, ....  For example, `(partial-sums
;; integers)' should be the stream 1, 3, 6, 10, 15, ....

(use util.stream)

(define ones (stream-cons 1 ones))
(define (add-streams s1 s2)
  (stream-map + s1 s2))
(define integers (cons-stream 1 (add-streams ones integers)))


(define (partial-sums stream)
  (stream-cons (stream-car stream)
               (add-streams (stream-cdr stream) (partial-sums stream))))

;; test
(use gauche.test)
(test-start "exercise 3.55")
(test* "(1 3 6 10 15)"
       '(1 3 6 10 15)
       (stream->list (stream-take-while (lambda (x) (< x 20))
                                        (partial-sums integers))))
(test-end)
