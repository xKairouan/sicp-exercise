;; -*- coding: utf-8 -*-

;; *Exercise 2.82:* Show how to generalize `apply-generic' to handle
;; coercion in the general case of multiple arguments.  One strategy
;; is to attempt to coerce all the arguments to the type of the first
;; argument, then to the type of the second argument, and so on.
;; Give an example of a situation where this strategy (and likewise
;; the two-argument version given above) is not sufficiently general.
;; (Hint: Consider the case where there are some suitable mixed-type
;; operations present in the table that will not be tried.)

(define (apply-generic op . args)
  (define (exec-coerce x type)
    (let ((coercion (get-coercion (type-tag x) type)))
      (and coercion (coercion x))))
  (define (find-proc-coerceds op args types)
    (if (null? types)
        #f
        (let* ((coerceds (map (lambda (x)
                                (exec-coerce x (car types)))
                              args))
               (proc (get op (map type-tag coerceds))))
          (if (and coerced proc)
              (cons proc coerceds)
              (find-proc-coerceds op args (cdr types))))))
  (let* ((type-tags (map type-tag args))
         (proc (get op type-tags)))
    (if proc
        (apply proc (map contents args))
        (let (proc-ceerceds (find-proc-coerceds op args type-tags))
          (if proc-ceerceds
              (apply (car proc-ceerceds) (map content (cdr proc-coerceds)))
              (error "No method for these types -- APPLY-GENERIC"
                     (list op type-tags)))))))