;; -*- coding: utf-8 -*-

;; *Exercise 2.30:* Define a procedure `square-tree' analogous to the
;; `square-list' procedure of *Note Exercise 2-21::.  That is,
;; `square-list' should behave as follows:

;;      (square-tree
;;       (list 1
;;             (list 2 (list 3 4) 5)
;;             (list 6 7)))
;;      (1 (4 (9 16) 25) (36 49))

;; Define `square-tree' both directly (i.e., without using any
;; higher-order procedures) and also by using `map' and recursion.

(define (square-tree tree)
  (define (square x) (* x x))
  (cond [(null? tree) '()]
        [(not (pair? tree)) (square tree)]
        [else (cons (square-tree (car tree)) (square-tree (cdr tree)))]))

(define (square-tree2 tree)
  (define (square x) (* x x))
  (map (lambda (sub-tree)
         (if (pair? sub-tree)
             (square-tree sub-tree)
             (square sub-tree)))
       tree))

(use gauche.test)
(test-start "exercise 2.30")
(test* "square-tree (without higher-order procedures)"
       '(1 (4 (9 16) 25) (36 49))
       (square-tree '(1 (2 (3 4) 5) (6 7))))
(test* "square-tree (using map and recursion)"
       '(1 (4 (9 16) 25) (36 49))
       (square-tree2 '(1 (2 (3 4) 5) (6 7))))
(test-end)
