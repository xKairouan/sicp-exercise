;; -*- coding: utf-8 -*-

;; *Exercise 3.69:* Write a procedure `triples' that takes three
;; infinite streams, S, T, and U, and produces the stream of triples
;; (S_i,T_j,U_k) such that i <= j <= k.  Use `triples' to generate
;; the stream of all Pythagorean triples of positive integers, i.e.,
;; the triples (i,j,k) such that i <= j and i^2 + j^2 = k^2.


(load "./stream.scm")

(define (interleave s1 s2)
  (if (stream-null? s1)
      s2
      (cons-stream (stream-car s1)
                   (interleave s2 (stream-cdr s1)))))

(define (pairs s t)
  (cons-stream
   (list (stream-car s) (stream-car t))
   (interleave (stream-map (lambda (x) (list (stream-car s) x))
                            (stream-cdr t))
               (pairs (stream-cdr s) (stream-cdr t)))))

(define (triples s t u)
  (cons-stream
   (map stream-car (list s t u))
   (interleave (stream-map (lambda (x) (cons (stream-car s) x))
                           (pairs (stream-cdr t) (stream-cdr u)))
               (triples (stream-cdr s) (stream-cdr t) (stream-cdr u)))))

(define ones (cons-stream 1 ones))
(define integers (cons-stream 1 (stream-map + ones integers)))

(define (pythagorean? triple)
  (define (square x) (* x x))
  (let ((i (car triple))
        (j (cadr triple))
        (k (caddr triple)))
    (= (+ (square i) (square j))
       (square k))))

;; take some time to evaluate.
(print (stream->list (stream-take (stream-filter pythagorean? (triples integers integers integers))
                                  5)))