;; -*- coding: utf-8 -*-

;; *Exercise 2.46:* A two-dimensional vector v running from the
;; origin to a point can be represented as a pair consisting of an
;; x-coordinate and a y-coordinate.  Implement a data abstraction for
;; vectors by giving a constructor `make-vect' and corresponding
;; selectors `xcor-vect' and `ycor-vect'.  In terms of your selectors
;; and constructor, implement procedures `add-vect', `sub-vect', and
;; `scale-vect' that perform the operations vector addition, vector
;; subtraction, and multiplying a vector by a scalar:

;;      (x_1, y_1) + (x_2, y_2) = (x_1 + x_2, y_1 + y_2)
;;      (x_1, y_1) - (x_2, y_2) = (x_1 - x_2, y_1 - y_2)
;;                   s * (x, y) = (sx, sy)


(define (make-vect x y) (list x y))
(define (xcor-vect v) (car v))
(define (ycor-vect v) (cadr v))

(define (add-vect v1 v2)
  (make-vect (+ (xcor-vect v1) (xcor-vect v2))
             (+ (ycor-vect v1) (ycor-vect v2))))
(define (scale-vect s v)
  (make-vect (* s (xcor-vect v))
             (* s (ycor-vect v))))
(define (sub-vect v1 v2)
  (add-vect v1 (scale-vect -1 v2)))

(define (equal-vect? v1 v2)
  (and (= (xcor-vect v1) (xcor-vect v2))
       (= (ycor-vect v1) (ycor-vect v2))))

;; test
(use gauche.test)
(test-start "exercise 2.46")
(let ((v (make-vect 1 2)))
  (test* "xcor-vect" 1 (xcor-vect v))
  (test* "ycor-vect" 2 (ycor-vect v)))
(let ((v1 (make-vect 1 2))
      (v2 (make-vect 4 8)))
  (test* "add-vect" (make-vect 5 10) (add-vect v1 v2) equal-vect?)
  (test* "sub-vect" (make-vect -3 -6) (sub-vect v1 v2) equal-vect?)
  (test* "scale-vect" (make-vect 3 6) (scale-vect 3 v1)))
(test-end)