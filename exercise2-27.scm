;; -*- coding: utf-8 -*-

;; *Exercise 2.27:* Modify your `reverse' procedure of *Note Exercise
;; 2-18:: to produce a `deep-reverse' procedure that takes a list as
;; argument and returns as its value the list with its elements
;; reversed and with all sublists deep-reversed as well.  For example,

;;      (define x (list (list 1 2) (list 3 4)))

;;      x
;;      ((1 2) (3 4))

;;      (reverse x)
;;      ((3 4) (1 2))

;;      (deep-reverse x)
;;      ((4 3) (2 1))

(define (deep-reverse lis)
  (cond [(null? lis) '()]
        [(pair? (car lis))
         (append (deep-reverse (cdr lis)) (list (deep-reverse (car lis))))]
        [else (append (deep-reverse (cdr lis)) (list (car lis)))]))
;; test
(use gauche.test)
(test-start "exercise 2.27")
(test* "((1 2) (3 4)) => ((4 3) (2 1))" '((4 3) (2 1)) (deep-reverse '((1 2) (3 4))))
(test* "((1 2) 3 (4 5)) => ((5 4) 3 (2 1))" '((5 4) 3 (2 1)) (deep-reverse '((1 2) 3 (4 5))))
(test-end)
