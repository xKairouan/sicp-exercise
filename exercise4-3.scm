;; -*- coding: utf-8 -*-

;; *Exercise 4.3:* Rewrite `eval' so that the dispatch is done in
;; data-directed style.  Compare this with the data-directed
;; differentiation procedure of *Note Exercise 2-73::.  (You may use
;; the `car' of a compound expression as the type of the expression,
;; as is appropriate for the syntax implemented in this section.)


(define *eval-table* (make-hash-table equal?))
(define (get type) (hash-table-get *eval-table* type #f))
(define (put! type item)
  (hash-table-put! *eval-table* type item))

(define (install-eval-package)
  (put! lookup-variable-value)
  (put! 'quote (lambda (exp env) (text-of-quotation exp)))
  (put! 'set! eval-assignment)
  (put! 'define eval-definition)
  (put! 'if eval-if)
  (put! 'lambda (lambda (exp env)
                  (make-procedure (lambda-parameters exp)
                                  (lambda-body exp)
                                  env)))
  (put! 'begin (lambda (exp env)
                 (eval-sequence (begin-actions exp) env)))
  (put! 'cond (lambda) (exp env) (eval cond->if exp) env))

(define (eval exp env)
  (cond [(self-evaluating? exp) exp]
        [(variable? exp) (lookup-variable-value exp env)]
        [(get (car exp))
         ((get (car exp)) exp env)]
        [(application? exp)
         (apply (eval (operator exp) env)
                (list-of-values (operands exp) env))]
        [else (error "Unknown expression type -- EVAL" exp)]))
