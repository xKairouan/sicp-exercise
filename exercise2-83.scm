;; -*- coding: utf-8 -*-

;; *Exercise 2.83:* Suppose you are designing a generic arithmetic
;; system for dealing with the tower of types shown in *Note Figure
;; 2-25::: integer, rational, real, complex.  For each type (except
;; complex), design a procedure that raises objects of that type one
;; level in the tower.  Show how to install a generic `raise'
;; operation that will work for each type (except complex).

(load "./arithmetic")

(define (attach-tag type-tag contents)
  (cond [(eq? type-tag 'integer) contents]
        [(eq? type-tag 'real) contents]
        [else (cons type-tag contents)]))

(define (type-tag datum)
  (cond [(and (integer? datum) (exact? datum)) 'integer]
        [(number? datum) 'real]
        [(pair? datum) (car datum)]
        [else (error "Bad tagged datum -- TYPE-TAG" datum)]))

(define (contents datum)
  (cond [(and (integer? datum) (exact? datum)) datum]
        [(number? datum) datum]
        [(pair? datum) (cdr datum)]
        [else (error "Bad tagged datum -- CONTENTS" datum)]))

(define (install-integer-package)
  ;; internals
  (define (integer->rational n)
    (make-rational n 1))
  ;; definition
  (put '=zero? '(integer) (lambda (x) (= x 0)))
  (put 'equ? '(integer integer) (lambda (x y) (= x y)))
  (put 'add '(integer integer) (lambda (x y) (+ x y)))
  (put 'sub '(integer integer) (lambda (x y) (- x y)))
  (put 'mul '(integer integer) (lambda (x y) (* x y)))
  (put 'div '(integer integer) (lambda (x y) (/ x y)))
  (put 'raise '(integer) integer->rational)
  'done)

(define (install-rational-patch)
  ;; internals
  (define (numer x) (car x))
  (define (denom x) (cdr x))
  (define (rational->real x)
    (make-real (/. (numer x) (denom x))))
  ;; definition
  (put 'raise '(rational) rational->real)
  'done)

(define (install-real-package)
  ;; internals
  (define (real->complex x)
    (make-complex-from-real-imag x 0))
  ;; definition
  (put '=zero? '(real) (lambda (x) (= x 0)))
  (put 'equ? '(real real)
       (lambda (x y) (= x y)))
  (put 'add '(real real) +)
  (put 'sub '(real real) -)
  (put 'mul '(real real) *)
  (put 'div '(real real) /.)
  (put 'make '(real) identity)
  (put 'raise '(real) real->complex)
  'done)

(define (make-real n) ((get 'make '(real)) n))

(define (raise x) (apply-generic 'raise x))
(define (equ? x y) (apply-generic 'equ? x y))


(install-integer-package)
(install-rational-patch)
(install-real-package)

;; test
(use gauche.test)
(test-start "exercise2-83.scm")
(test* "integer->rational" 'rational (type-tag (raise 7)))
(test* "rational->real" 'real (type-tag (raise (raise 7))))
(test* "real->complex" 'complex (type-tag (raise(raise (raise 7)))))
(test-end)
