;; -*- coding: utf-8 -*-

;; *Exercise 2.31:* Abstract your answer to *Note Exercise 2-30:: to
;; produce a procedure `tree-map' with the property that
;; `square-tree' could be defined as

;;      (define (square-tree tree) (tree-map square tree))

(define (tree-map proc tree)
  (map (lambda (sub-tree)
         (if (pair? sub-tree)
             (tree-map proc sub-tree)
             (proc sub-tree)))
       tree))
(define (square x) (* x x))
(define (square-tree tree) (tree-map square tree))

(use gauche.test)
(test-start "exercise 2.31")
(test* "square-tree (using tree-map)"
       '(1 (4 (9 16) 25) (36 49))
       (square-tree '(1 (2 (3 4) 5) (6 7))))
(test-end)
