;; -*- coding: utf-8 -*-

;; *Exercise 3.23:* A "deque" ("double-ended queue") is a sequence in
;; which items can be inserted and deleted at either the front or the
;; rear.  Operations on deques are the constructor `make-deque', the
;; predicate `empty-deque?', selectors `front-deque' and
;; `rear-deque', and mutators `front-insert-deque!',
;; `rear-insert-deque!', `front-delete-deque!', and
;; `rear-delete-deque!'.  Show how to represent deques using pairs,
;; and give implementations of the operations.(2)  All operations
;; should be accomplished in [theta](1) steps.

;; constructor
(define (make-deque) (cons '() '()))

;; predicate
(define (empty-deque? deque) (null? (front-ptr deque)))

;; selectors
(define (front-ptr deque) (car deque))
(define (rear-ptr deque) (cdr deque))
(define (front-deque deque)
  (if (empty-deque? deque)
      (error "FRONT called with empty deque" deque)
      (car (front-ptr deque))))
(define (rear-deque deque)
  (if (empty-deque? deque)
      (error "REAR called with empy deque" deque)
      (car (rear-ptr deque))))

;; mutators
(define (set-front-ptr! deque item) (set-car! deque item))
(define (set-rear-ptr! deque item) (set-cdr! deque item))
(define (front-insert-deque! deque item)
  (let ((new-item (make-item item)))
    (cond [(empty-deque? deque)
           (set-front-ptr! deque new-item)
           (set-rear-ptr! deque new-item)
           deque]
          [else
           (set-next-item! new-item (front-ptr deque))
           (set-front-ptr! deque new-item)
           deque])))
(define (rear-insert-deque! deque item)
  (let ((new-item (make-item item)))
    (cond [(empty-deque? deque)
           (set-front-ptr! deque new-item)
           (set-rear-ptr! deque new-item)
           deque]
          [else
           (set-prev-item! new-item (rear-ptr deque))
           (set-rear-ptr! deque new-item)
           deque])))
(define (front-delete-deque! deque)
  (cond [(empty-deque? deque)
         (error "FRONT-DELETE-DEQUE! called with empty deque" deque)]
        [else
         (set-front-ptr! deque (next-item (front-ptr deque)))
         deque]))
(define (rear-delete-deque! deque)
  (cond [(empty-deque? deque)
         (error "REAR-DELETE-DEQUE! called with empty deque" deque)]
        [else
         (set-rear-ptr! deque (prev-item (rear-ptr deque)))
         deque]))

;; util
(define (make-item v) (cons v (cons '() '())))
(define (item-value item) (car item))
(define (next-item item) (cadr item))
(define (prev-item item) (cddr item))
(define (set-next-item! item1 item2)
  (set-car! (cdr item1) item2)
  (set-cdr! (cdr item2) item1))
(define (set-prev-item! item1 item2)
  (set-car! (cdr item2) item1)
  (set-cdr! (cdr item1) item2))


;; test
(use gauche.test)
(test-start "exercise 3.23")
(define deque (make-deque))
(front-insert-deque! deque 1)
(front-insert-deque! deque 2)
(front-insert-deque! deque 3)
(test* "now front is 3" 3 (front-deque deque))
(test* "now rear is 1" 1 (rear-deque deque))
(rear-insert-deque! deque 4)
(test* "now front is 3" 3 (front-deque deque))
(test* "now rear is 4" 4 (rear-deque deque))
(rear-delete-deque! deque)
(rear-delete-deque! deque)
(test* "now front is 2" 3 (front-deque deque))
(test* "now rear is 1" 2 (rear-deque deque))
(front-delete-deque! deque)
(test* "now front is 1" 2 (front-deque deque))
(test* "now rear is 1" 2 (rear-deque deque))
(test-end)
