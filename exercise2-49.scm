;; -*- coding: utf-8 -*-

;; *Exercise 2.49:* Use `segments->painter' to define the following
;; primitive painters:

;;   a. The painter that draws the outline of the designated frame.

;;   b. The painter that draws an "X" by connecting opposite corners
;;      of the frame.

;;   c. The painter that draws a diamond shape by connecting the
;;      midpoints of the sides of the frame.

;;   d. The `wave' painter.

(use gl)
(use gl.glut)

(add-load-path ".")

(use painter)

(define outline
  (let ((p0 (make-vect 0 0))
        (p1 (make-vect 0 1))
        (p2 (make-vect 1 1))
        (p3 (make-vect 1 0)))
    (let ((seg0 (make-segment p0 p1))
          (seg1 (make-segment p1 p2))
          (seg2 (make-segment p2 p3))
          (seg3 (make-segment p3 p0)))
      (segments->painter (list seg0 seg1 seg2 seg3)))))

(define x-mark
  (let ((p0 (make-vect 0 0))
        (p1 (make-vect 0 1))
        (p2 (make-vect 1 1))
        (p3 (make-vect 1 0)))
    (let ((seg0 (make-segment p0 p2))
          (seg1 (make-segment p1 p3)))
      (segments->painter (list seg0 seg1)))))

(define diamond
  (let ((p0 (make-vect 0.5 0.0))
        (p1 (make-vect 1.0 0.5))
        (p2 (make-vect 0.5 1.0))
        (p3 (make-vect 0.0 0.5)))
    (let ((seg0 (make-segment p0 p1))
          (seg1 (make-segment p1 p2))
          (seg2 (make-segment p2 p3))
          (seg3 (make-segment p3 p0)))
      (segments->painter (list seg0 seg1 seg2 seg3)))))

(define wave
  (let ((p00 (make-vect 0.40 1.00))
        (p01 (make-vect 0.60 1.00))
        (p02 (make-vect 0.00 0.80))
        (p03 (make-vect 0.35 0.80))
        (p04 (make-vect 0.65 0.80))
        (p05 (make-vect 0.00 0.60))
        (p06 (make-vect 0.30 0.60))
        (p07 (make-vect 0.40 0.60))
        (p08 (make-vect 0.60 0.60))
        (p09 (make-vect 0.70 0.60))
        (p10 (make-vect 0.20 0.55))
        (p11 (make-vect 0.30 0.55))
        (p12 (make-vect 0.35 0.50))
        (p13 (make-vect 0.65 0.50))
        (p14 (make-vect 0.20 0.45))
        (p15 (make-vect 1.00 0.40))
        (p16 (make-vect 0.50 0.20))
        (p17 (make-vect 1.00 0.20))
        (p18 (make-vect 0.25 0.00))
        (p19 (make-vect 0.40 0.00))
        (p20 (make-vect 0.60 0.00))
        (p21 (make-vect 0.75 0.00)))
    (let ((seg00 (make-segment p00 p03))
          (seg01 (make-segment p03 p07))
          (seg02 (make-segment p07 p06))
          (seg03 (make-segment p06 p10))
          (seg04 (make-segment p10 p02))
          (seg05 (make-segment p05 p14))
          (seg06 (make-segment p14 p11))
          (seg07 (make-segment p11 p12))
          (seg08 (make-segment p12 p18))
          (seg09 (make-segment p19 p16))
          (seg10 (make-segment p16 p20))
          (seg11 (make-segment p21 p13))
          (seg12 (make-segment p13 p17))
          (seg13 (make-segment p15 p09))
          (seg14 (make-segment p09 p08))
          (seg15 (make-segment p08 p04))
          (seg16 (make-segment p04 p01)))
      (segments->painter (list seg00 seg01 seg02 seg03 seg04
                               seg05 seg06 seg07 seg08 seg09
                               seg10 seg11 seg12 seg13 seg14
                               seg15 seg16)))))

;; drawing
(define (init)
  (gl-clear-color 1.0 1.0 1.0 1.0))

(define (draw)
  (gl-clear GL_COLOR_BUFFER_BIT)
  (gl-color 0.0 0.0 0.0 0.0)
  (gl-begin GL_LINE_LOOP)
  ;(outline frame)
  ;(x-mark frame)
  ;(diamond frame)
  (wave frame)
  (gl-end)
  (gl-flush))

(define (main args)
  (glut-init args)
  (glut-create-window "Painter")
  (glut-display-func draw)
  (init)
  (glut-main-loop))
