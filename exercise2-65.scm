;; -*- coding: utf-8 -*-

;; *Exercise 2.65:* Use the results of *Note Exercise 2-63:: and
;; *Note Exercise 2-64:: to give [theta](n) implementations of
;; `union-set' and `intersection-set' for sets implemented as
;; (balanced) binary trees.(5)

(define (entry tree) (car tree))

(define (left-branch tree) (cadr tree))

(define (right-branch tree) (caddr tree))

(define (make-tree entry left right)
  (list entry left right))

(define (tree->list tree)
  (define (copy-to-list tree result-list)
    (if (null? tree)
        result-list
        (copy-to-list (left-branch tree)
                      (cons (entry tree)
                            (copy-to-list (right-branch tree)
                                          result-list)))))
  (copy-to-list tree '()))

(define (list->tree elements)
  (define (partial-tree elts n)
    (if (= n 0)
        (cons '() elts)
        (let ((left-size (quotient (- n 1) 2)))
          (let ((left-result (partial-tree elts left-size)))
            (let ((left-tree (car left-result))
                  (non-left-elts (cdr left-result))
                  (right-size (- n (+ left-size 1))))
              (let ((this-entry (car non-left-elts))
                    (right-result (partial-tree (cdr non-left-elts)
                                                right-size)))
                (let ((right-tree (car right-result))
                      (remaining-elts (cdr right-result)))
                  (cons (make-tree this-entry left-tree right-tree)
                        remaining-elts))))))))
  (car (partial-tree elements (length elements))))


;; answer
(define (union-set set1 set2)
  (let ((s1 (tree->list set1))
        (s2 (tree->list set2)))
    (define (union-set-iter s1 s2 result)
      (cond [(null? s1) (append result s2)]
            [(null? s2) (append result s1)]
            [(< (car s1) (car s2))
             (union-set-iter (cdr s1) s2 (append result (list (car s1))))]
            [(= (car s1) (car s2))
             (union-set-iter (cdr s1) (cdr s2) (append result (list (car s1))))]
            [(> (car s1) (car s2))
             (union-set-iter s1 (cdr s2) (append result (list (car s2))))]))
    (list->tree (union-set-iter s1 s2 '()))))

(define (intersection-set set1 set2)
  (let ((s1 (tree->list set1))
        (s2 (tree->list set2)))
    (define (intersection-set-iter s1 s2 result)
      (cond [(or (null? s1) (null? s2)) result]
            [(< (car s1) (car s2))
             (intersection-set-iter (cdr s1)  s2 result)]
            [(= (car s1) (car s2))
             (intersection-set-iter (cdr s1) (cdr s2) (append result (list (car s1))))]
            [(> (car s1) (car s2))
             (intersection-set-iter s1 (cdr s2) result)]))
    (list->tree (intersection-set-iter s1 s2 '()))))

(use gauche.test)
(test-start "exercise 2.65")
(test* "union-set"
       (list->tree '())
       (union-set (list->tree '()) (list->tree '())))
(test* "union-set"
       (list->tree '(1 2))
       (union-set (list->tree '(1 2)) (list->tree '())))
(test* "union-set"
       (list->tree '(1 2))
       (union-set (list->tree '()) (list->tree '(1 2))))
(test* "union-set"
       (list->tree '(1 2 3 4 5))
       (union-set (list->tree '(1 3 5)) (list->tree '(2 4))))
(test* "union-set"
       (list->tree '(1 2 3 4 5))
       (union-set (list->tree '(2 4)) (list->tree '(1 3 5))))
(test* "union-set"
       (list->tree '(1 2 3 4 5))
       (union-set (list->tree '(1 2 3 4)) (list->tree '(2 3 4 5))))
(test* "union-set"
       (list->tree '(1 2 3 4 5))
       (union-set (list->tree '(2 3 4 5)) (list->tree '(1 2 3 4))))
(test* "intersection-set"
       (list->tree '())
       (intersection-set (list->tree '()) (list->tree '())))
(test* "intersection-set"
       (list->tree '())
       (intersection-set (list->tree '(1 2)) (list->tree '())))
(test* "intersection-set"
       (list->tree '())
       (intersection-set (list->tree '()) (list->tree '(1 2))))
(test* "intersection-set"
       (list->tree '())
       (intersection-set (list->tree '(1 3 5)) (list->tree '(2 4))))
(test* "intersection-set"
       (list->tree '(2 3 4))
       (intersection-set (list->tree '(1 2 3 4)) (list->tree '(2 3 4 5))))
(test* "intersection-set"
       (list->tree '(2 3 4))
       (intersection-set (list->tree '(2 3 4 5)) (list->tree '(1 2 3 4))))
(test-end)
