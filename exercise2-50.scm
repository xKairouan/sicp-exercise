;; -*- coding: utf-8 -*-

;; *Exercise 2.50:* Define the transformation `flip-horiz', which
;; flips painters horizontally, and transformations that rotate
;; painters counterclockwise by 180 degrees and 270 degrees.

(use gl)
(use gl.glut)

(add-load-path ".")

(use painter)

(define (flip-horiz painter)
  (let ((origin (make-vect 1.0 0.0))
        (edge1 (make-vect 0.0 0.0))
        (edge2 (make-vect 0.0 1.0)))
    (transform-painter painter origin edge1 edge2)))

(define (rotate90 painter)
  (let ((origin (make-vect 1.0 0.0))
        (edge1 (make-vect 1.0 1.0))
        (edge2 (make-vect 0.0 0.0)))
    (transform-painter painter origin edge1 edge2)))

(define (rotate180 painter)
  (rotate90 (rotate90 painter)))

(define (rotate270 painter)
  (rotate90 (rotate180 painter)))

;; drawing test
(define (draw)
  (gl-clear GL_COLOR_BUFFER_BIT)
  (gl-color 0.0 0.0 0.0 0.0)
  (gl-begin GL_LINE_LOOP)
  ;((flip-horiz wave) frame)
  ((rotate270 wave) frame)
  (gl-end)
  (gl-flush))

(define (init)
  (gl-clear-color 1.0 1.0 1.0 1.0))

(define (main args)
  (glut-init args)
  (glut-create-window "Painter")
  (glut-display-func draw)
  (init)
  (glut-main-loop))
