;; -*- coding: utf-8 -*-

;; *Exercise 2.85:* This section mentioned a method for "simplifying"
;; a data object by lowering it in the tower of types as far as
;; possible.  Design a procedure `drop' that accomplishes this for the
;; tower described in *Note Exercise 2-83::.  The key is to decide,
;; in some general way, whether an object can be lowered.  For
;; example, the complex number 1.5 + 0i can be lowered as far as
;; `real', the complex number 1 + 0i can be lowered as far as
;; `integer', and the complex number 2 + 3i cannot be lowered at all.
;; Here is a plan for determining whether an object can be lowered:
;; Begin by defining a generic operation `project' that "pushes" an
;; object down in the tower.  For example, projecting a complex
;; number would involve throwing away the imaginary part.  Then a
;; number can be dropped if, when we `project' it and `raise' the
;; result back to the type we started with, we end up with something
;; equal to what we started with.  Show how to implement this idea in
;; detail, by writing a `drop' procedure that drops an object as far
;; as possible.  You will need to design the various projection
;; operations(5) and install `project' as a generic operation in the
;; system.  You will also need to make use of a generic equality
;; predicate, such as described in *Note Exercise 2-79::.  Finally,
;; use `drop' to rewrite `apply-generic' from *Note Exercise 2-84::
;; so that it "simplifies" its answers.

(load "./arithmetic")

(define (attach-tag type-tag contents)
  (cond [(eq? type-tag 'integer) contents]
        [(eq? type-tag 'real) contents]
        [else (cons type-tag contents)]))

(define (type-tag datum)
  (cond [(and (integer? datum) (exact? datum)) 'integer]
        [(number? datum) 'real]
        [(pair? datum) (car datum)]
        [else (error "Bad tagged datum -- TYPE-TAG" datum)]))

(define (contents datum)
  (cond [(and (integer? datum) (exact? datum)) datum]
        [(number? datum) datum]
        [(pair? datum) (cdr datum)]
        [else (error "Bad tagged datum -- CONTENTS" datum)]))

(define (install-integer-package)
  ;; internals
  (define (integer->rational n)
    (make-rational n 1))
  ;; definition
  (put '=zero? '(integer) (lambda (x) (= x 0)))
  (put 'equ? '(integer integer) (lambda (x y) (= x y)))
  (put 'add '(integer integer) (lambda (x y) (+ x y)))
  (put 'sub '(integer integer) (lambda (x y) (- x y)))
  (put 'mul '(integer integer) (lambda (x y) (* x y)))
  (put 'div '(integer integer) (lambda (x y) (/ x y)))
  (put 'raise '(integer) integer->rational)
  'done)

(define (install-rational-patch)
  ;; internals
  (define (numer x) (car x))
  (define (denom x) (cdr x))
  (define (rational->real x)
    (make-real (/. (numer x) (denom x))))
  (define (equ? x y)
    (= (* (numer x) (denom y))
       (* (numer y) (denom x))))
  ;; definition
  (put 'raise '(rational) rational->real)
  ;; for ex. 2.85
  (put 'project '(rational)
       (lambda (x)
         (round->exact (/ (numer x) (denom x)))))
  (put 'equ? '(rational rational) equ?)
  'done)

(define (install-real-package)
  ;; internals
  (define (real->complex x)
    (make-complex-from-real-imag x 0))
  ;; definition
  (put '=zero? '(real) (lambda (x) (= x 0)))
  (put 'equ? '(real real)
       (lambda (x y) (= x y)))
  (put 'add '(real real) +)
  (put 'sub '(real real) -)
  (put 'mul '(real real) *)
  (put 'div '(real real) /.)
  (put 'make '(real) identity)
  (put 'raise '(real) real->complex)
  ;; for ex 2.85
  (put 'project '(real)
       (lambda (x) (make-rational (round x) 1)))
  'done)

(define (make-real n) ((get 'make '(real)) (inexact n)))

(define (install-complex-patch)
  ;; internals
  (define (equ? x y)
    (and (= (real-part x) (real-part y))
         (= (imag-part x) (imag-part y))))
  ;; for ex 2.85
  (put 'project '(complex)
       (lambda (x)
         (make-real (real-part x))))
  (put 'equ? '(complex complex) equ?)
  'done)

;; generic proc
(define (raise x) (apply-generic 'raise x))
(define (equ? x y) (apply-generic 'equ? x y))
(define (add x y) (apply-generic 'add x y))
(define (sub x y) (apply-generic 'sub x y))
(define (mul x y) (apply-generic 'mul x y))
(define (div x y) (apply-generic 'div x y))
;; for ex 2.85
(define (project x) (apply-generic 'project x))

(install-integer-package)
(install-rational-patch)
(install-real-package)
(install-complex-patch)

;; ========== answer from here ==========
(define *type-order* '(integer rational real complex))
(define (type<? x y)
  (define (iter tower x y)
    (cond [(null? tower) #f]
          [(and (eq? x (car tower))
                (not (eq? y (car tower))))
           #t]
          [(and (not (eq? x (car tower)))
                (eq? y (car tower)))
           #f]
          [else (iter (cdr tower) x y)]))
  (iter *type-order* x y))
(define (type=? x y)
  (eq? x y))
(define (type>? x y)
  (not (or (type=? x y)
           (type<? x y))))

(define (drop x)
  (if (eq? (type-tag x) (car *type-order*))
      x
      (let ((dropped (project x)))
        (if (equ? (raise dropped) x)
            (drop dropped)
            x))))

(define (apply-generic op . args)
  (define (find-highest-type types)
    (fold (lambda (x y)
            (if (type>? x y)
                x
                y))
          'integer
          types))
  (define (x->type x type)
    (if (eq? (type-tag x) type)
        x
        (x->type (raise x) type)))
  (let ((proc (get op (map type-tag args)))
        (maybe-drop (if (memq op '(equ? =zero? raise project))
                        identity
                        drop)))
    (if proc
        (maybe-drop (apply proc (map contents args)))
        (let* ((highest-type (find-highest-type (map type-tag args)))
               (new-args (map (lambda (x) (x->type x highest-type)) args))
               (proc (get op (map type-tag new-args))))
          (if proc
              (maybe-drop (apply proc (map contents new-args)))
              (error "no such method -- APPLY-GENERIC"
                     (list op args)))))))

;; test
(use gauche.test)
(test-start "exercise 2.85")
(test* "drop" 3 (add 1.0 2.0))
(test* "drop" 2 (add (make-complex-from-real-imag 1.0 1.0)
                     (make-complex-from-real-imag 1.0 -1.0)))
(test-end)
