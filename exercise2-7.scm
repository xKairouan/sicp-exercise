;; -*- coding: utf-8 -*-

;; *Exercise 2.7:* Alyssa's program is incomplete because she has not
;; specified the implementation of the interval abstraction.  Here is
;; a definition of the interval constructor:

;;      (define (make-interval a b) (cons a b))

;; Define selectors `upper-bound' and `lower-bound' to complete the
;; implementation.

(define (make-interval a b) (cons a b))

(define (upper-bound interval) (cdr interval))
(define (lower-bound interval) (car interval))

(use gauche.test)
(test-start "exercise 2.7")
(test* "(upper-bound (make-interval 2 8)) => 8" 8 (upper-bound (make-interval 2 8)))
(test* "(lower-bound (make-interval 2 8)) => 2" 2 (lower-bound (make-interval 2 8)))
(test-end)
