;; -*- coding: utf-8 -*-

;; *Exercise 3.70:* It would be nice to be able to generate streams
;; in which the pairs appear in some useful order, rather than in the
;; order that results from an _ad hoc_ interleaving process.  We can
;; use a technique similar to the `merge' procedure of *Note Exercise
;; 3-56::, if we define a way to say that one pair of integers is
;; "less than" another.  One way to do this is to define a "weighting
;; function" W(i,j) and stipulate that (i_1,j_1) is less than
;; (i_2,j_2) if W(i_1,j_1) < W(i_2,j_2).  Write a procedure
;; `merge-weighted' that is like `merge', except that
;; `merge-weighted' takes an additional argument `weight', which is a
;; procedure that computes the weight of a pair, and is used to
;; determine the order in which elements should appear in the
;; resulting merged stream.(5)  Using this, generalize `pairs' to a
;; procedure `weighted-pairs' that takes two streams, together with a
;; procedure that computes a weighting function, and generates the
;; stream of pairs, ordered according to weight.  Use your procedure
;; to generate

;;   a. the stream of all pairs of positive integers (i,j) with i <= j
;;      ordered according to the sum i + j

;;   b. the stream of all pairs of positive integers (i,j) with i <=
;;      j, where neither i nor j is divisible by 2, 3, or 5, and the
;;      pairs are ordered according to the sum 2 i + 3 j + 5 i j.


(load "./stream.scm")

(define (merge-weighted s1 s2 weight)
  (cond ((stream-null? s1) s2)
        ((stream-null? s2) s1)
        (else
         (let ((s1car (stream-car s1))
               (s2car (stream-car s2)))
           (let ((w1 (apply weight s1car))
                 (w2 (apply weight s2car)))
             (if (< w1 w2)
                 (cons-stream s1car
                              (merge-weighted (stream-cdr s1) s2 weight))
                 (cons-stream s2car
                              (merge-weighted s1 (stream-cdr s2) weight))))))))

(define (weighted-pairs s t weight)
  (cons-stream
   (list (stream-car s) (stream-car t))
   (merge-weighted (stream-map (lambda (x) (list (stream-car s) x))
                               (stream-cdr t))
                   (weighted-pairs (stream-cdr s) (stream-cdr t) weight)
                   weight)))

(define ones (cons-stream 1 ones))
(define integers (cons-stream 1
                              (stream-map + ones integers)))

;; a.
(print (stream->list (stream-take (weighted-pairs integers
                                                  integers
                                                  (lambda (i j) (+ i j)))
                                  100)))

;; b.
(let ((needed-numbers (stream-filter (lambda (x)
                                       (define (devides? x n)
                                         (= (remainder x n) 0))
                                       (not (or (devides? x 2)
                                                (devides? x 3)
                                                (devides? x 5))))
                                     integers)))
  (print (stream->list (stream-take (weighted-pairs needed-numbers
                                                    needed-numbers
                                                    (lambda (i j)
                                                      (+ (* 2 i)
                                                         (* 3 j)
                                                         (* 5 i j))))
                                    100))))
