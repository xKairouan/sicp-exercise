;; -*- coding: utf-8 -*-

;; *Exercise 2.10:* Ben Bitdiddle, an expert systems programmer,
;; looks over Alyssa's shoulder and comments that it is not clear what
;; it means to divide by an interval that spans zero.  Modify
;; Alyssa's code to check for this condition and to signal an error
;; if it occurs.

(define (make-interval a b) (cons a b))

(define (upper-bound interval) (cdr interval))
(define (lower-bound interval) (car interval))

(define (mul-interval x y)
  (let ((p1 (* (lower-bound x) (lower-bound y)))
        (p2 (* (lower-bound x) (upper-bound y)))
        (p3 (* (upper-bound x) (lower-bound y)))
        (p4 (* (upper-bound x) (upper-bound y))))
    (make-interval (min p1 p2 p3 p4)
                   (max p1 p2 p3 p4))))

(define (div-interval x y)
  (let ([lower-y (lower-bound y)]
        [upper-y (upper-bound y)])
    (cond [(or (= lower-y 0) (= upper-y 0))
           (error "interval must not span zero")]
          [(and (< lower-y 0) (< 0 upper-y))
           (error "interval must not span zero")]
          [else
           (mul-interval x (make-interval (/ 1.0 upper-y)
                                          (/ 1.0 lower-y)))])))

;; test
(use gauche.test)
(test-start "exercise 2.10")
(let ([interval (make-interval 4 8)])
  (test* "error when divide by interval begins with 0"
         *test-error*
         (div-interval interval (make-interval 0 4)))
  (test* "error when divide by interval ends with 0"
         *test-error*
         (div-interval interval (make-interval -4 0)))
  (test* "error when divide by interval spans"
         *test-error*
         (div-interval interval (make-interval -1 1)))
  (test* "(4,8)/(1,2) => (2,8)"
         2.0
         (lower-bound (div-interval interval (make-interval 1 2))))
  (test* "(4,8)/(1,2) => (2,8)"
         8.0
         (upper-bound (div-interval interval (make-interval 1 2)))))
(test-end)
