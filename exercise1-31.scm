;; -*- coding: utf-8 -*-

;; *Exercise 1.31:*
;; a. The `sum' procedure is only the simplest of a vast number of
;; similar abstractions that can be captured as higher-order
;; procedures.(3)  Write an analogous procedure called `product'
;; that returns the product of the values of a function at
;; points over a given range.  Show how to define `factorial' in
;; terms of `product'.  Also use `product' to compute
;; approximations to [pi] using the formula(4)

;; pi   2 * 4 * 4 * 6 * 6 * 8 ...
;; -- = -------------------------
;; 4   3 * 3 * 5 * 5 * 7 * 7 ...

;; b. If your `product' procedure generates a recursive process,
;; write one that generates an iterative process.  If it
;; generates an iterative process, write one that generates a
;; recursive process.

(define (product term a next b)
  (define (iter a result)
    (if (> a b)
        result
        (iter (next a) (* (term a) result))))
  (iter a 1))

(define (product2 term a next b)
  (if (> a b)
      1
      (* (term a)
         (product term (next a) next b))))

(define (factorial n)
  (define (term x) x)
  (define (next x) (+ x 1))
  (product term 1 next n))

(define pi
  (begin
    (define (square x) (* x x))
    (define (term i)
      (/. (* (* 2 i) (* 2 (+ i 1)))
         (square (+ (* 2 i) 1))))
    (define (next i) (+ i 1))
    (* 4 (product term 1 next 10000))))
