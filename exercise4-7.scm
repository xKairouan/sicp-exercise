;; -*- coding: utf-8 -*-

;; *Exercise 4.7:* `Let*' is similar to `let', except that the
;; bindings of the `let' variables are performed sequentially from
;; left to right, and each binding is made in an environment in which
;; all of the preceding bindings are visible.  For example

;;      (let* ((x 3)
;;             (y (+ x 2))
;;             (z (+ x y 5)))
;;        (* x z))

;; returns 39.  Explain how a `let*' expression can be rewritten as a
;; set of nested `let' expressions, and write a procedure
;; `let*->nested-lets' that performs this transformation.  If we have
;; already implemented `let' (*Note Exercise 4-6::) and we want to
;; extend the evaluator to handle `let*', is it sufficient to add a
;; clause to `eval' whose action is

;;      (eval (let*->nested-lets exp) env)

;; or must we explicitly expand `let*' in terms of non-derived
;; expressions?

(load "./metacircular.scm")

;; let
(define (let? exp) (tagged-list? exp 'let))
(define (let-vars exp)
  (let ((var-exp-list (cadr exp)))
    (fold-right (lambda (x result) (cons (car x) result))
                '()
                var-exp-list)))
(define (let-var-values exp)
  (let ((var-exp-list (cadr exp)))
    (fold-right (lambda (x result) (cons (cadr x) result))
                '()
                var-exp-list)))
(define (let-body exp) (cddr exp))
(define (let->combination exp)
  (cons (make-lambda (let-vars exp)
                     (let-body exp))
        (let-var-values exp)))
(define (make-let vars values body)
  (if (= (length vars) (length values))
      (cons 'let (cons (map list vars values) body))
      (if (< (length vars) (length values))
          (error "too many exp for variables" vars values)
          (error "too few exp for variables" vars values))))

;; let*
(define (let*? exp) (tagged-list? exp 'let*))
(define (let*-vars exp) (let-vars exp))
(define (let*-var-values exp) (let-var-values exp))
(define (let*-body exp) (let-body exp))
(define (let*->nested-lets exp)
  (define (last-var? vars) (null? (cdr vars)))
  (define (loop vars values)
    (if (last-var? vars)
        (make-let vars values (let*-body exp))
        (make-let (list (car vars))
                  (list (car values))
                  (list (loop (cdr vars) (cdr values))))))
  (let ((vars (let*-vars exp))
        (values (let*-var-values exp)))
    (if (= (length vars) (length values))
        (loop vars values))
        (if (< (length vars) (length values))
            (error "too many exp for variables" vars values)
            (error "too few exp for variables" vars values)))))


;; eval
(define (eval exp env)
  (cond [(self-evaluating? exp) exp]
        [(variable? exp) (lookup-variable-value exp env)]
        [(quoted? exp) (text-of-quotation exp)]
        [(assignment? exp) (eval-assignment exp env)]
        [(definition? exp) (eval-definition exp env)]
        [(if? exp) (eval-if exp env)]
        [(lambda? exp)
         (make-procedure (lambda-parameters exp)
                         (lambda-body exp)
                         env)]
        [(begin? exp)
         (eval-sequence (begin-actions exp) env)]
        [(cond? exp) (eval (cond->if exp) env)]
        [(let? exp) (eval (let->combination exp) env)]
        [(let*? exp) (eval (let*->nested-lets exp) env)]
        [(application? exp)
         (apply (eval (operator exp) env)
                (list-of-values (operands exp) env))]
        [else
         (error "Unknown expression type -- EVAL" exp)]))

(define primitive-procedures
  (list (list 'car car)
        (list 'cdr cdr)
        (list 'cons cons)
        (list 'null? null?)
        (list '+ +)
        ;;<MORE PRIMITIVES>
        ))

(define the-global-environment (setup-environment))
(driver-loop)


;; test
;; (let* ((a 1) (b (+ a 1)) (c (+ b 2))) (+ a b c)) => 7