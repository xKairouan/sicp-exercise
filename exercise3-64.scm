;; -+- coding; utf-8 -*-

;; *Exercise 3.64:* Write a procedure `stream-limit' that takes as
;; arguments a stream and a number (the tolerance).  It should
;; examine the stream until it finds two successive elements that
;; differ in absolute value by less than the tolerance, and return
;; the second of the two elements.  Using this, we could compute
;; square roots up to a given tolerance by

;;      (define (sqrt x tolerance)
;;        (stream-limit (sqrt-stream x) tolerance))


(load "./stream.scm")

(define (stream-limit stream tolerance)
  (let ((s1 (stream-ref stream 0))
        (s2 (stream-ref stream 1)))
    (if (< (abs (- s1 s2)) tolerance)
        s2
        (stream-limit (stream-cdr stream) tolerance))))


(define (sqrt-improve guess x)
  (define (average a b) (/. (+ a b) 2))
  (average guess (/ x guess)))
(define (sqrt-stream x)
  (define guesses
    (cons-stream 1.0
                 (stream-map (lambda (guess)
                               (sqrt-improve guess x))
                             guesses)))
  guesses)

(define (sqrt x tolerance)
  (stream-limit (sqrt-stream x) tolerance))
