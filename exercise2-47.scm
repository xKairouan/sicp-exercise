;; -*- coding: utf-8 -*-

;; *Exercise 2.47:* Here are two possible constructors for frames:

;;      (define (make-frame origin edge1 edge2)
;;        (list origin edge1 edge2))

;;      (define (make-frame origin edge1 edge2)
;;        (cons origin (cons edge1 edge2)))

;; For each constructor supply the appropriate selectors to produce an
;; implementation for frames.

(use gauche.test)

(define (make-vect x y) (list x y))
(define (equal-vect? v1 v2)
  (and (= (xcor-vect v1) (xcor-vect v2))
       (= (ycor-vect v1) (ycor-vect v2))))

(define (make-frame origin edge1 edge2)
  (list origin edge1 edge2))
(define (frame-origin frame)
  (car frame))
(define (frame-edge1 frame)
  (cadr frame))
(define (frame-edge2 frame)
  (caddr frame))

(test-start "exercise 2.47 (use list)")
(let ((frame (make-frame (make-vect 0 0) (make-vect 1 1) (make-vect 2 2))))
  (test* "frame-origin" (make-vect 0 0) (frame-origin frame) equal-vect?)
  (test* "frame-edge1" (make-vect 1 1) (frame-edge1 frame) equal-vect?)
  (test* "frame-edge2" (make-vect 2 2) (frame-edge2 frame) equal-vect?))
(test-end)

(define (make-frame origin edge1 edge2)
  (cons origin (cons edge1 edge2)))
(define (frame-origin frame)
  (car frame))
(define (frame-edge1 frame)
  (cadr frame))
(define (frame-edge2 frame)
  (cddr frame))

(test-start "exercise 2.47 (use cons)")
(let ((frame (make-frame (make-vect 0 0) (make-vect 1 1) (make-vect 2 2))))
  (test* "frame-origin" (make-vect 0 0) (frame-origin frame) equal-vect?)
  (test* "frame-edge1" (make-vect 1 1) (frame-edge1 frame) equal-vect?)
  (test* "frame-edge2" (make-vect 2 2) (frame-edge2 frame) equal-vect?))
(test-end)
