;; -*- coding: utf-8 -*-

;; *Exercise 4.1:* Notice that we cannot tell whether the
;; metacircular evaluator evaluates operands from left to right or
;; from right to left.  Its evaluation order is inherited from the
;; underlying Lisp: If the arguments to `cons' in `list-of-values'
;; are evaluated from left to right, then `list-of-values' will
;; evaluate operands from left to right; and if the arguments to
;; `cons' are evaluated from right to left, then `list-of-values'
;; will evaluate operands from right to left.

;; Write a version of `list-of-values' that evaluates operands from
;; left to right regardless of the order of evaluation in the
;; underlying Lisp.  Also write a version of `list-of-values' that
;; evaluates operands from right to left.

(define no-operands? null?)
(define first-operand car)
(define rest-operands cdr)

(define (list-of-values-left-to-right exps env)
  (define (iter exps result)
    (if (no-operands? exps)
        result
        (iter (rest-operands exps)
              (append result (list (eval (first-operand exps) env))))))
  (iter exps '()))

(define (list-of-values-right-to-left exps env)
  (list-of-values-left-to-right (reverse exps) env))


;; test
(define v 10)
(define expression '((begin (set! v (+ v 2)) v)
                     (begin (set! v (* v 2)) v)))
(list-of-values-left-to-right expression (interaction-environment)) ; => (12 24)

(set! v 10)
(list-of-values-right-to-left expression (interaction-environment)) ; => (20 22)
