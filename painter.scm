;; -*- coding: utf-8 -*-

(define-module painter
  (use gl)
  (use gl.glut)
  (export-all))
(select-module painter)

;; vector
(define (make-vect x y) (list x y))
(define (xcor-vect v) (car v))
(define (ycor-vect v) (cadr v))

(define (add-vect v1 v2)
  (make-vect (+ (xcor-vect v1) (xcor-vect v2))
             (+ (ycor-vect v1) (ycor-vect v2))))
(define (scale-vect s v)
  (make-vect (* s (xcor-vect v))
             (* s (ycor-vect v))))
(define (sub-vect v1 v2)
  (add-vect v1 (scale-vect -1 v2)))

(define (equal-vect? v1 v2)
  (and (= (xcor-vect v1) (xcor-vect v2))
       (= (ycor-vect v1) (ycor-vect v2))))

;; segment
(define (make-segment start-point end-point)
  (list start-point end-point))
(define (start-segment segment)
  (car segment))
(define (end-segment segment)
  (cadr segment))

;; frame
(define (make-frame origin edge1 edge2)
  (list origin edge1 edge2))
(define (origin-frame frame)
  (car frame))
(define (edge1-frame frame)
  (cadr frame))
(define (edge2-frame frame)
  (caddr frame))

;; painter
(define (segments->painter segment-list)
  (lambda (frame)
    (for-each
     (lambda (segment)
       (draw-line
        ((frame-coord-map frame) (start-segment segment))
        ((frame-coord-map frame) (end-segment segment))))
     segment-list)))

(define outline
  (let ((p0 (make-vect 0 0))
        (p1 (make-vect 0 1))
        (p2 (make-vect 1 1))
        (p3 (make-vect 1 0)))
    (let ((seg0 (make-segment p0 p1))
          (seg1 (make-segment p1 p2))
          (seg2 (make-segment p2 p3))
          (seg3 (make-segment p3 p0)))
      (segments->painter (list seg0 seg1 seg2 seg3)))))

(define x-mark
  (let ((p0 (make-vect 0 0))
        (p1 (make-vect 0 1))
        (p2 (make-vect 1 1))
        (p3 (make-vect 1 0)))
    (let ((seg0 (make-segment p0 p2))
          (seg1 (make-segment p1 p3)))
      (segments->painter (list seg0 seg1)))))

(define diamond
  (let ((p0 (make-vect 0.5 0.0))
        (p1 (make-vect 1.0 0.5))
        (p2 (make-vect 0.5 1.0))
        (p3 (make-vect 0.0 0.5)))
    (let ((seg0 (make-segment p0 p1))
          (seg1 (make-segment p1 p2))
          (seg2 (make-segment p2 p3))
          (seg3 (make-segment p3 p0)))
      (segments->painter (list seg0 seg1 seg2 seg3)))))

(define wave
  (let ((p00 (make-vect 0.40 1.00))
        (p01 (make-vect 0.60 1.00))
        (p02 (make-vect 0.00 0.80))
        (p03 (make-vect 0.35 0.80))
        (p04 (make-vect 0.65 0.80))
        (p05 (make-vect 0.00 0.60))
        (p06 (make-vect 0.30 0.60))
        (p07 (make-vect 0.40 0.60))
        (p08 (make-vect 0.60 0.60))
        (p09 (make-vect 0.70 0.60))
        (p10 (make-vect 0.20 0.55))
        (p11 (make-vect 0.30 0.55))
        (p12 (make-vect 0.35 0.50))
        (p13 (make-vect 0.65 0.50))
        (p14 (make-vect 0.20 0.45))
        (p15 (make-vect 1.00 0.40))
        (p16 (make-vect 0.50 0.20))
        (p17 (make-vect 1.00 0.20))
        (p18 (make-vect 0.25 0.00))
        (p19 (make-vect 0.40 0.00))
        (p20 (make-vect 0.60 0.00))
        (p21 (make-vect 0.75 0.00)))
    (let ((seg00 (make-segment p00 p03))
          (seg01 (make-segment p03 p07))
          (seg02 (make-segment p07 p06))
          (seg03 (make-segment p06 p10))
          (seg04 (make-segment p10 p02))
          (seg05 (make-segment p05 p14))
          (seg06 (make-segment p14 p11))
          (seg07 (make-segment p11 p12))
          (seg08 (make-segment p12 p18))
          (seg09 (make-segment p19 p16))
          (seg10 (make-segment p16 p20))
          (seg11 (make-segment p21 p13))
          (seg12 (make-segment p13 p17))
          (seg13 (make-segment p15 p09))
          (seg14 (make-segment p09 p08))
          (seg15 (make-segment p08 p04))
          (seg16 (make-segment p04 p01)))
      (segments->painter (list seg00 seg01 seg02 seg03 seg04
                               seg05 seg06 seg07 seg08 seg09
                               seg10 seg11 seg12 seg13 seg14
                               seg15 seg16)))))

(define (frame-coord-map frame)
  (lambda (v)
    (add-vect
     (origin-frame frame)
     (add-vect (scale-vect (xcor-vect v)
                           (edge1-frame frame))
               (scale-vect (ycor-vect v)
                           (edge2-frame frame))))))

;; painter manipulation
(define (transform-painter painter origin corner1 corner2)
  (lambda (frame)
    (let ((m (frame-coord-map frame)))
      (let ((new-origin (m origin)))
        (painter
         (make-frame new-origin
                     (sub-vect (m corner1) new-origin)
                     (sub-vect (m corner2) new-origin)))))))

(define (beside painter1 painter2)
  (let ((split-point (make-vect 0.5 0.0)))
    (let ((paint-left
           (transform-painter painter1
                              (make-vect 0.0 0.0)
                              split-point
                              (make-vect 0.0 1.0)))
          (paint-right
           (transform-painter painter2
                              split-point
                              (make-vect 1.0 0.0)
                              (make-vect 0.5 1.0))))
      (lambda (frame)
        (paint-left frame)
        (paint-right frame)))))

(define (rotate90 painter)
  (transform-painter painter
                     (make-vect 1.0 0.0)
                     (make-vect 1.0 1.0)
                     (make-vect 0.0 0.0)))

(define (rotate180 painter)
  (rotate90 (rotate90 painter)))

(define (rotate270 painter)
  (rotate90 (rotate180 painter)))

(define (below painter1 painter2)
  (rotate270 (beside (rotate90 painter2) (rotate90 painter1))))

(define (flip-vert painter)
  (let ((origin (make-vect 0.0 1.0))
        (edge1 (make-vect 1.0 1.0))
        (edge2 (make-vect 0.0 0.0)))
    (transform-painter painter origin edge1 edge2)))

(define (flip-horiz painter)
  (let ((origin (make-vect 1.0 0.0))
        (edge1 (make-vect 0.0 0.0))
        (edge2 (make-vect 1.0 1.0)))
    (transform-painter painter origin edge1 edge2)))

(define (right-split painter n)
  (if (= n 0)
      painter
      (let ((smaller (right-split painter (- n 1))))
        (beside painter (below smaller smaller)))))

(define (up-split painter n)
  (if (= n 0)
      painter
      (let ((smaller (up-split painter (- n 1))))
        (below painter (beside smaller smaller)))))

(define (corner-split painter n)
  (if (= n 0)
      painter
      (let ((up (up-split painter (- n 1)))
            (right (right-split painter (- n 1))))
        (let ((top-left (beside up up))
              (bottom-right (below right right))
              (corner (corner-split painter (- n 1))))
          (beside (below painter top-left)
                  (below bottom-right corner))))))

(define (square-limit painter n)
  (let ((quarter (corner-split painter n)))
    (let ((half (beside (flip-horiz quarter) quarter)))
      (below (flip-vert half) half))))

(define (square-of-four tl tr bl br)
  (lambda (painter)
    (let ((top (beside (tl painter) (tr painter)))
          (bottom (beside (bl painter) (br painter))))
      (below bottom top))))

;; drawing to frame
(define (draw-line start end)
  (define (t z) (- (* 2 z) 1))
  (gl-begin GL_LINE_LOOP)
  (gl-vertex (t (xcor-vect start)) (t (ycor-vect start)))
  (gl-vertex (t (xcor-vect end)) (t (ycor-vect end)))
  (gl-end))


(define frame (make-frame (make-vect 0 0)
                          (make-vect 1 0)
                          (make-vect 0 1)))

(provide "painter")
