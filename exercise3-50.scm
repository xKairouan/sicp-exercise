;; -*- coding: utf-8 -*-

;; *Exercise 3.50:* Complete the following definition, which
;; generalizes `stream-map' to allow procedures that take multiple
;; arguments, analogous to `map' in section *Note 2-2-3::, footnote
;; *Note Footnote 12::.

;;      (define (stream-map proc . argstreams)
;;        (if (<??> (car argstreams))
;;            the-empty-stream
;;            (<??>
;;             (apply proc (map <??> argstreams))
;;             (apply stream-map
;;                    (cons proc (map <??> argstreams))))))


(use util.stream)
(define the-empty-stream stream-null)

(define (my-stream-map proc . argstreams)
  (if (stream-null? (car argstreams))
      the-empty-stream
      (stream-cons
       (apply proc (map stream-car argstreams))
       (apply stream-map
              (cons proc (map stream-cdr argstreams))))))

;; test
(use gauche.test)
(test-start "exercise3-50")
(test* "+ (1 2 3) (40 50 60) (700 800 900) => (741 852 963)"
       '(741 852 963)
       (stream->list (my-stream-map +
                                    (list->stream '(1 2 3))
                                    (list->stream '(40 50 60))
                                    (list->stream '(700 800 900)))))
(test-end)
