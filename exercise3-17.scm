;; -*- coding: utf-8 -*-

;; *Exercise 3.17:* Devise a correct version of the `count-pairs'
;; procedure of *Note Exercise 3-16:: that returns the number of
;; distinct pairs in any structure.  (Hint: Traverse the structure,
;; maintaining an auxiliary data structure that is used to keep track
;; of which pairs have already been counted.)


(define (count-pairs x)
  (define (counted? needle heystock)
    (cond [(null? heystock) #f]
          [(eq? needle (car heystock)) #t]
          [else (counted? needle (cdr heystock))]))
  (let ((counted-pairs '()))
    (define (count-pairs-inner x)
      (cond [(not (pair? x)) 0]
            [(counted? x counted-pairs) 0]
            [else
             (set! counted-pairs (cons x counted-pairs))
             (+ (count-pairs-inner (car x))
                (count-pairs-inner (cdr x))
                1)]))
    (count-pairs-inner x)))

;; test data
;; return 4
(define a '(1))
(define b (cons 2 a))
(define x (cons a b))
(count-pairs x)

;; return 7
(define c (cons a a))
(define y (cons c c))
(count-pairs y)

;; never return at all
(define z '(1 2 3))
(set-car! (last-pair z) z)
(count-pairs z)

;; test
(use gauche.test)
(test-start "exercise 3.17")
(test* "return 3" 3 (count-pairs '(1 2 3)))
(test* "return 3" 3 (count-pairs x))
(test* "return 3" 3 (count-pairs y))
(test* "return 3" 3 (count-pairs z))
(test-end)
