;; -*- coding: utf-8 -*-

;; *Exercise 3.22:* Instead of representing a queue as a pair of
;; pointers, we can build a queue as a procedure with local state.
;; The local state will consist of pointers to the beginning and the
;; end of an ordinary list.  Thus, the `make-queue' procedure will
;; have the form

;;      (define (make-queue)
;;        (let ((front-ptr ... )
;;              (rear-ptr ... ))
;;          <DEFINITIONS OF INTERNAL PROCEDURES>
;;          (define (dispatch m) ...)
;;          dispatch))


(define (make-queue)
  (let ((front-ptr '())
        (rear-ptr '()))
    (define (front) front-ptr)
    (define (rear) rear-ptr)
    (define (set-front-ptr! item) (set! front-ptr item))
    (define (set-rear-ptr! item) (set! rear-ptr item))
    (define (empty-queue?) (null? front-ptr))
    (define (front-queue)
      (if (empty-queue?)
          (error "FRONT called with empty queue" front-ptr)
          (car front-ptr)))
    (define (insert-queue! item)
      (let ((new-pair (cons item '())))
        (cond [(empty-queue?)
               (set-front-ptr! new-pair)
               (set-rear-ptr! new-pair)
               dispatch]
              [else
               (set-cdr! (rear) new-pair)
               (set-rear-ptr! new-pair)
               dispatch])))
    (define (delete-queue!)
      (cond [(empty-queue?)
             (error "DELETE! called with an empty queue" front-ptr)]
            [else
             (set-front-ptr! (cdr (front)))
             dispatch]))
    (define (print-queue)
      (display (front))
      (newline))
    (define (dispatch m)
      (cond [(eq? m 'front-ptr) front]
            [(eq? m 'rear-ptr) rear]
            [(eq? m 'set-front-ptr!) set-front-ptr!]
            [(eq? m 'set-rear-ptr!) set-rear-ptr!]
            [(eq? m 'empty-queue?) empty-queue?]
            [(eq? m 'front-queue) front-queue]
            [(eq? m 'insert-queue!) insert-queue!]
            [(eq? m 'delete-queue!) delete-queue!]
            [(eq? m 'print-queue) print-queue]))
    dispatch))

(define (insert-queue! queue item)
  ((queue 'insert-queue!) item))
(define (delete-queue! queue)
  ((queue 'delete-queue!)))
(define (print-queue queue)
  ((queue 'print-queue)))
