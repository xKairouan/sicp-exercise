;; -*- coding: utf-8 -*-

;; *Exercise 2.1:* Define a better version of `make-rat' that handles
;; both positive and negative arguments.  `Make-rat' should normalize
;; the sign so that if the rational number is positive, both the
;; numerator and denominator are positive, and if the rational number
;; is negative, only the numerator is negative.

(define (make-rat n d)
  (define (gcd a b)
    (if (= b 0)
        a
        (gcd b (remainder a b))))
  (define (need-sign? a b)
    (< (* a b) 0))
  (let ([g (gcd n d)])
    (let ([numerator (abs (/ n g))]
          [denominator (abs (/ d g))])
      (if (need-sign? n d)
          (cons (- numerator) denominator)
          (cons numerator denominator)))))

(define numer car)
(define denom cdr)

(use gauche.test)
(test-start "exercise 2.1")
(test* "numerator of 2/3 is 2" 2 (numer (make-rat 2 3)))
(test* "denominator of 2/3 is 3" 3 (denom (make-rat 2 3)))
(test* "numerator of -4/6 is -2" -2 (numer (make-rat -4 6)))
(test* "denominator of -4/6 is 3" 3 (denom (make-rat 4 -6)))
(test* "numerator of (-4)/(-6) is 2" 2 (numer (make-rat -4 -6)))
(test* "denominator of (-4)/(-6) is 3" 3 (denom (make-rat -4 -6)))
(test-end)