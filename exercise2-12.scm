;; -*- coding: utf-8 -*-

;; *Exercise 2.12:* Define a constructor `make-center-percent' that
;; takes a center and a percentage tolerance and produces the desired
;; interval.  You must also define a selector `percent' that produces
;; the percentage tolerance for a given interval.  The `center'
;; selector is the same as the one shown above.

(define make-interval cons)
(define lower-bound car)
(define upper-bound cdr)

(define (make-center-percent center percent)
  (let ((width (* center (/. percent 100))))
    (make-interval (- center width)
                   (+ center width))))
(define (center interval)
  (/. (+ (lower-bound interval) (upper-bound interval)) 2))
(define (percent interval)
  (let ((width (/. (- (upper-bound interval) (lower-bound interval)) 2)))
    (/. (* 100 width) (center interval))))

(use gauche.test)
(define (nearly-equal? a b)
  (< (abs (- a b)) 1.0e-11))
(test-start "exercise 2.12")
(test* "percent" 10 (percent (make-center-percent 68 10)) nearly-equal?)
(test* "percent" 20 (percent (make-center-percent 68 20)) nearly-equal?)
(test* "percent" 1 (percent (make-center-percent 68 1)) nearly-equal?)
(test-end)
