;; -*- coding: utf-8 -*-

;; *Exercise 3.79:* Generalize the `solve-2nd' procedure of *Note
;; Exercise 3-78:: so that it can be used to solve general
;; second-order differential equations d^2 y/dt^2 = f(dy/dt, y).

(load "./stream.scm")

(define (add-stream s1 s2) (stream-map + s1 s2))
(define (scale-map s factor) (stream-map (lambda (x) (* x factor)) s))

(define (integral delayed-integrand initial-value dt)
  (cons-stream initial-value
               (let ((integrand (force delayed-integrand)))
                 (if (stream-null? integrand)
                     the-empty-stream
                     (integral (delay (stream-cdr integrand))
                               (+ (* dt (stream-car integrand))
                                  initial-value)
                               dt)))))

(define (solve-2nd f dt y0 dy0)
  (define y (integral (delay dy) y0 dt))
  (define dy (integral (delay ddy) dy0 dt))
  (define ddy (stream-map f dy y))
  y)

(print (stream-ref (solve-2nd (lambda (dy y) (+ (* 1 dy) (* -1 y))) 0.001 0 1) 1000))