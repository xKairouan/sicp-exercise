;; -*- coding: utf-8 -*-

;; *Exercise 3.68:* Louis Reasoner thinks that building a stream of
;; pairs from three parts is unnecessarily complicated.  Instead of
;; separating the pair (S_0,T_0) from the rest of the pairs in the
;; first row, he proposes to work with the whole first row, as
;; follows:

;;      (define (pairs s t)
;;        (interleave
;;         (stream-map (lambda (x) (list (stream-car s) x))
;;                     t)
;;         (pairs (stream-cdr s) (stream-cdr t))))

;; Does this work?  Consider what happens if we evaluate `(pairs
;; integers integers)' using Louis's definition of `pairs'.

(load "./stream.scm")

(define (interleave s1 s2)
  (if (stream-null? s1)
      s2
      (cons-stream (stream-car s1)
                   (interleave s2 (stream-cdr s1)))))

(define (pairs s t)
  (interleave
   (stream-map (lambda (x) (list (stream-car s) x))
               t)
   (pairs (stream-cdr s) (stream-cdr t))))

(define ones (cons-stream 1 ones))
(define integers (cons-stream 1 (stream-map + ones integers)))
(print (stream->list (stream-take (pairs integers integers) 100)))


;; 無限ループ
;; pairs が再帰的に呼び出されるが、
;; cons-stream の cdr 部になってないので遅延評価されない
;; => なので無限ループ