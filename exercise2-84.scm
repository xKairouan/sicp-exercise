;; -*- coding: utf-8 -*-

;; *Exercise 2.84:* Using the `raise' operation of *Note Exercise
;; 2-83::, modify the `apply-generic' procedure so that it coerces
;; its arguments to have the same type by the method of successive
;; raising, as discussed in this section.  You will need to devise a
;; way to test which of two types is higher in the tower.  Do this in
;; a manner that is "compatible" with the rest of the system and will
;; not lead to problems in adding new levels to the tower.

(load "./arithmetic")

(define (attach-tag type-tag contents)
  (cond [(eq? type-tag 'integer) contents]
        [(eq? type-tag 'real) contents]
        [else (cons type-tag contents)]))

(define (type-tag datum)
  (cond [(and (integer? datum) (exact? datum)) 'integer]
        [(number? datum) 'real]
        [(pair? datum) (car datum)]
        [else (error "Bad tagged datum -- TYPE-TAG" datum)]))

(define (contents datum)
  (cond [(and (integer? datum) (exact? datum)) datum]
        [(number? datum) datum]
        [(pair? datum) (cdr datum)]
        [else (error "Bad tagged datum -- CONTENTS" datum)]))

(define (install-integer-package)
  ;; internals
  (define (integer->rational n)
    (make-rational n 1))
  ;; definition
  (put '=zero? '(integer) (lambda (x) (= x 0)))
  (put 'equ? '(integer integer) (lambda (x y) (= x y)))
  (put 'add '(integer integer) (lambda (x y) (+ x y)))
  (put 'sub '(integer integer) (lambda (x y) (- x y)))
  (put 'mul '(integer integer) (lambda (x y) (* x y)))
  (put 'div '(integer integer) (lambda (x y) (/ x y)))
  (put 'raise '(integer) integer->rational)
  'done)

(define (install-rational-patch)
  ;; internals
  (define (numer x) (car x))
  (define (denom x) (cdr x))
  (define (rational->real x)
    (make-real (/. (numer x) (denom x))))
  ;; definition
  (put 'raise '(rational) rational->real)
  'done)

(define (install-real-package)
  ;; internals
  (define (real->complex x)
    (make-complex-from-real-imag x 0))
  ;; definition
  (put '=zero? '(real) (lambda (x) (= x 0)))
  (put 'equ? '(real real)
       (lambda (x y) (= x y)))
  (put 'add '(real real) +)
  (put 'sub '(real real) -)
  (put 'mul '(real real) *)
  (put 'div '(real real) /.)
  (put 'make '(real) identity)
  (put 'raise '(real) real->complex)
  'done)

(define (make-real n) ((get 'make '(real)) n))

;; generic proc
(define (raise x) (apply-generic 'raise x))
(define (equ? x y) (apply-generic 'equ? x y))
(define (add x y) (apply-generic 'add x y))
(define (sub x y) (apply-generic 'sub x y))
(define (mul x y) (apply-generic 'mul x y))
(define (div x y) (apply-generic 'div x y))

(install-integer-package)
(install-rational-patch)
(install-real-package)


;; answer
(define *type-order* '(integer rational real complex))
(define (type<? x y)
  (define (iter tower x y)
    (cond [(null? tower) #f]
          [(and (eq? x (car tower))
                (not (eq? y (car tower))))
           #t]
          [(and (not (eq? x (car tower)))
                (eq? y (car tower)))
           #f]
          [else (iter (cdr tower) x y)]))
  (iter *type-order* x y))
(define (type=? x y)
  (eq? x y))
(define (type>? x y)
  (not (or (type=? x y)
           (type<? x y))))

(define (apply-generic op . args)
  (define (find-highest-type types)
    (fold (lambda (x y)
            (if (type>? x y)
                x
                y))
          'integer
          types))
  (define (x->type x type)
    (if (eq? (type-tag x) type)
        x
        (x->type (raise x) type)))
  (let ((proc (get op (map type-tag args))))
    (if proc
        (apply proc (map contents args))
        (let* ((highest-type (find-highest-type (map type-tag args)))
               (new-args (map (lambda (x) (x->type x highest-type)) args))
               (proc (get op new-args)))
          (if proc
              (apply proc (map contents new-args))
              (error "no such method -- APPLY-GENERIC"
                     (list op args)))))))

;; test
(use gauche.test)
(test-start "exercise 2.84")
(test* "same typex" 3 (add 1 2))
(test* "different types" (make-real 3) (add 1 (make-real 2)))
(test* "different types" (make-real 3) (add (make-real 1) 2))
(test-end)
