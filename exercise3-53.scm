;; -*- coding: utf-8 -*-

;; *Exercise 3.53:* Without running the program, describe the
;; elements of the stream defined by

;;      (define s (cons-stream 1 (add-streams s s)))


;; (1 2 4 8 16 32 64 ...)
;; つまり 2^(n-1) からなる数列
(use util.stream)
(define cons-stream stream-cons)

(define (add-streams s1 s2)
  (stream-map + s1 s2))

(define s (cons-stream 1 (add-streams s s)))

(use gauche.test)
(test-start "exercise 3.53")
(test* "(1 2 4 8 16 32 64 128 256 512)"
       '(1 2 4 8 16 32 64 128 256 512)
       (stream->list (stream-take-while (lambda (x) (< x 1000)) s)))
(test-end)
