;; -*- coding: utf-8 -*-

;; *Exercise 1.39:* A continued fraction representation of the
;; tangent function was published in 1770 by the German mathematician
;; J.H. Lambert:

;;                    x
;;      tan x = ---------------
;;                      x^2
;;              1 - -----------
;;                        x^2
;;                  3 - -------
;;                      5 - ...

;; where x is in radians.  Define a procedure `(tan-cf x k)' that
;; computes an approximation to the tangent function based on
;; Lambert's formula.  `K' specifies the number of terms to compute,
;; as in *Note Exercise 1-37::.

(define (cont-frac n d k)
  (define (cont-frac-iterative i result)
    (if (= i 0)
        result
        (cont-frac-iterative (- i 1)
                             (/. (n i) (+ (d i) result)))))
  (cont-frac-iterative (- k 1) (/. (n k) (d k))))

(define (tan-cf x k)
  (define (square x) (* x x))
  (cont-frac (lambda (i)
               (if (= i 1)
                   x
                   (- (square x))))
             (lambda (i) (- (* 2 i) 1))
             k))
