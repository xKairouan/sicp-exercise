;; -*- coding: utf-8 -*-

;; *Exercise 4.5:* Scheme allows an additional syntax for `cond'
;; clauses, `(<TEST> => <RECIPIENT>)'.  If <TEST> evaluates to a true
;; value, then <RECIPIENT> is evaluated.  Its value must be a
;; procedure of one argument; this procedure is then invoked on the
;; value of the <TEST>, and the result is returned as the value of
;; the `cond' expression.  For example

;;      (cond ((assoc 'b '((a 1) (b 2))) => cadr)
;;            (else false))

;; returns 2.  Modify the handling of `cond' so that it supports this
;; extended syntax.

(load "./metacircular.scm")


;; cond definitions
(define (cond? exp) (tagged-list? exp 'cond))

(define (cond-clauses exp) (cdr exp))

(define (cond-else-clause? clause)
  (eq? (cond-predicate clause) 'else))

(define (cond-predicate clause) (car clause))

(define (cond-actions clause)
  (if (= (length clause) 2)
      (cdr clause)
      (caddr clause)))                  ; (cond [test => recipent])

(define (cond->if exp)
  (expand-clauses (cond-clauses exp)))

(define (expand-clauses clauses)
  (if (null? clauses)
      'false                            ; no `else' clause
      (let ((first (car clauses))
            (rest (cdr clauses)))
        (if (cond-else-clause? first)
            (if (null? rest)
                (sequence->exp (cond-actions first))
                (error "ELSE clause isn't last -- COND->IF"
                       clauses))
            (cond [(= (length first) 2)
                   (make-if (cond-predicate first)
                            (sequence->exp (cond-actions first))
                            (expand-clauses rest))]
                  [(and (= (length first) 3)
                        (eq? (cadr first) '=>))
                   (make-if (cond-predicate first)
                            (list (cond-actions first) (cond-predicate first))
                            (expand-clauses rest))])))))


;; add assoc and cadr
(define primitive-procedures
  (list (list 'car car)
        (list 'cdr cdr)
        (list 'cons cons)
        (list 'null? null?)
        (list 'assoc assoc)
        (list 'cadr cadr)
        ;;<MORE PRIMITIVES>
        ))

(define the-global-environment (setup-environment))
(driver-loop)
