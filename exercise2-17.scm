;; -*- coding: utf-8 -*-

;; *Exercise 2.17:* Define a procedure `last-pair' that returns the
;; list that contains only the last element of a given (nonempty)
;; list:

;;      (last-pair (list 23 72 149 34))
;;      (34)

(define (my-last-pair lis)
  (if (pair? (cdr lis))
      (my-last-pair (cdr lis))
      lis))

(use gauche.test)
(test-start "exercise 2.17")
(test* "(1) => (1)" '(1) (my-last-pair '(1)))
(test* "(1 2 3) => (3)" '(3) (my-last-pair '(1 2 3)))
(test* "(1 2 . 3) => (2 . 3)" '(2 . 3) (my-last-pair '(1 2 . 3)))
(test-end)
