;; -*- coding: utf-8 -*-

;; *Exercise 2.11:* In passing, Ben also cryptically comments: "By
;; testing the signs of the endpoints of the intervals, it is
;; possible to break `mul-interval' into nine cases, only one of which
;; requires more than two multiplications."  Rewrite this procedure
;; using Ben's suggestion.

(use util.match)

(define make-interval cons)
(define lower-bound car)
(define upper-bound cdr)

(define (mul-interval x y)
  (let ([x1 (lower-bound x)]
        [x2 (upper-bound x)]
        [y1 (lower-bound y)]
        [y2 (upper-bound y)])
    (match (list (negative? x1)
                 (negative? x2)
                 (negative? y1)
                 (negative? y2))
           [(#t #t #t #t) (make-interval (* x2 y2) (* x1 y1))]
           [(#t #t #t #f) (make-interval (* x1 y2) (* x1 y1))]
           [(#t #t #f #f) (make-interval (* x1 y2) (* x2 y1))]
           [(#t #f #t #t) (make-interval (* x2 y1) (* x1 y1))]
           [(#t #f #t #f) (make-interval (min (* x1 y2) (* x2 y1))
                                         (max (* x1 y1) (* x2 y2)))]
           [(#t #f #f #f) (make-interval (* x1 y2) (* x2 y2))]
           [(#f #f #t #t) (make-interval (* x2 y1) (* x1 y2))]
           [(#f #f #t #f) (make-interval (* x2 y1) (* x2 y2))]
           [(#f #f #f #f) (make-interval (* x1 y1) (* x2 y2))])))

(define (equal-interval? x y)
  (and (= (lower-bound x) (lower-bound y))
       (= (upper-bound x) (upper-bound y))))

(use gauche.test)
(test-start "exercise 2.11")
(test* "(1, 2) * (8, 9) = (8, 18)"
       #t
       (equal-interval? (make-interval 8 18)
                        (mul-interval (make-interval 1 2)
                                      (make-interval 8 9))))
(test* "(1, 2) * (-8, 9) = (-16, 18)"
       #t
       (equal-interval? (make-interval -16 18)
                        (mul-interval (make-interval 1 2)
                                      (make-interval -8 9))))
(test* "(1, 2) * (-9, -8) = (-18, -8)"
       #t
       (equal-interval? (make-interval -18 -8)
                        (mul-interval (make-interval 1 2)
                                      (make-interval -9 -8))))
(test* "(-1, 2) * (8, 9) = (-9, 18)"
       #t
       (equal-interval? (make-interval -9 18)
                        (mul-interval (make-interval -1 2)
                                      (make-interval 8 9))))
(test* "(-1, 2) * (-8, 9) = (-16, 18)"
       #t
       (equal-interval? (make-interval -16 18)
                        (mul-interval (make-interval -1 2)
                                      (make-interval -8 9))))
(test* "(-3, 2) * (-8, 9) = (-27, 24)"
       #t
       (equal-interval? (make-interval -27 24)
                        (mul-interval (make-interval -3 2)
                                      (make-interval -8 9))))
(test* "(-1, 2) * (-9, -8) = (-18, 9)"
       #t
       (equal-interval? (make-interval -18 9)
                        (mul-interval (make-interval -1 2)
                                      (make-interval -9 -8))))
(test* "(-2, -1) * (8, 9) = (-18, -8)"
       #t
       (equal-interval? (make-interval -18 -8)
                        (mul-interval (make-interval -2 -1)
                                      (make-interval 8 9))))
(test* "(-2, -1) * (-8, 9) = (-18, 16)"
       #t
       (equal-interval? (make-interval -18 16)
                        (mul-interval (make-interval -2 1)
                                      (make-interval -8 9))))
(test* "(-2, -1) * (-9, -8) = (8, 18)"
       #t
       (equal-interval? (make-interval 8 18)
                        (mul-interval (make-interval -2 -1)
                                      (make-interval -9 -8))))
(use srfi-27)
(define (random-test count)
  (define (mul-interval-ref x y)
    (let ((p1 (* (lower-bound x) (lower-bound y)))
          (p2 (* (lower-bound x) (upper-bound y)))
          (p3 (* (upper-bound x) (lower-bound y)))
          (p4 (* (upper-bound x) (upper-bound y))))
      (make-interval (min p1 p2 p3 p4)
                     (max p1 p2 p3 p4))))
  (define (random-from--10-to-10)
    (- (random-integer 21) 10))
  (if (= count 0)
      #t
      (let* ((x1 (random-from--10-to-10))
             (x2 (+ x1 (random-integer 10) 1))
             (y1 (random-from--10-to-10))
             (y2 (+ y1 (random-integer 10) 1))
             (interval-x (make-interval x1 x2))
             (interval-y (make-interval y1 y2)))
        (test* "random-test"
               #t
               (equal-interval? (mul-interval-ref interval-x interval-y)
                                (mul-interval interval-x interval-y)))
        (random-test (- count 1)))))
(random-test 1000)
(test-end)
