;; -*- coding: utf-8 -*-

;; *Exercise 1.24:* Modify the `timed-prime-test' procedure of *Note
;; Exercise 1-22:: to use `fast-prime?' (the Fermat method), and test
;; each of the 12 primes you found in that exercise.  Since the
;; Fermat test has [theta](`log' n) growth, how would you expect the
;; time to test primes near 1,000,000 to compare with the time needed
;; to test primes near 1000?  Do your data bear this out?  Can you
;; explain any discrepancy you find?

(use srfi-11)

(define (runtime)
  (let-values ([(a b) (sys-gettimeofday)])
    (+ (* a 1000000) b)))

(define (fast-prime? n times)
  (define (expmod base exp m)
    (cond ((= exp 0) 1)
          ((even? exp)
           (remainder (square (expmod base (/ exp 2) m))
                      m))
          (else
           (remainder (* base (expmod base (- exp 1) m))
                      m))))
  (define (fermat-test n)
    (define (try-it a)
      (= (expmod a n n) a))
    (try-it (+ 1 (random (- n 1)))))
  (cond ((= times 0) #t)
        ((fermat-test n) (fast-prime? n (- times 1)))
        (else #f)))

(define (timed-prime-test n)
  (define (start-prime-test n start-time)
    (if (fast-prime? n 2)
        (report-prime (- (runtime) start-time))))
  (define (report-prime elapsed-time)
    (display " *** ")
    (display elapsed-time))
  (newline)
  (display n)
  (start-prime-test n (runtime)))

;; solution
(define (search-for-primes from n)
  (cond [(= n 0) (newline) 'done]
        [(even? from) (search-for-primes (+ from 1) n)]
        [(prime? from) (timed-prime-test from) (search-for-primes (+ from 1) (- n 1))]
        [else (search-for-primes (+ from 1) n)]))
