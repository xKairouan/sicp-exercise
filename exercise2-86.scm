;; -*- coding: utf-8 -*-

;; *Exercise 2.86:* Suppose we want to handle complex numbers whose
;; real parts, imaginary parts, magnitudes, and angles can be either
;; ordinary numbers, rational numbers, or other numbers we might wish
;; to add to the system.  Describe and implement the changes to the
;; system needed to accommodate this.  You will have to define
;; operations such as `sine' and `cosine' that are generic over
;; ordinary numbers and rational numbers.

(define *proc-table* (make-hash-table 'equal?))
(define (get op type) (hash-table-get *proc-table* (list op type) #f))
(define (put op type item) (hash-table-put! *proc-table* (list op type) item))

(define *coercion-table* (make-hash-table 'equal?))
(define (get-coercion type1 type2)
  (hash-table-get *coercion-table* (list type1 type2) #f))
(define (put-coercion type1 type2 item)
  (hash-table-put! *coercion-table* (list type1 type2) item))

(define (add x y) (apply-generic 'add x y))
(define (sub x y) (apply-generic 'sub x y))
(define (mul x y) (apply-generic 'mul x y))
(define (div x y) (apply-generic 'div x y))
(define (real-part z) (apply-generic 'real-part z))
(define (imag-part z) (apply-generic 'imag-part z))
(define (magnitude z) (apply-generic 'magnitude z))
(define (angle z) (apply-generic 'angle z))
(define (square x) (mul x x))

(define (install-scheme-number-package)
  (put 'add '(scheme-number scheme-number)
       (lambda (x y) (+ x y)))
  (put 'sub '(scheme-number scheme-number)
       (lambda (x y) (- x y)))
  (put 'mul '(scheme-number scheme-number)
       (lambda (x y) (* x y)))
  (put 'div '(scheme-number scheme-number)
       (lambda (x y) (/ x y)))
  (put 'exp '(scheme-number scheme-number)
       (lambda (x y) (tag (expt x y)))) ; using primitive `expt'
  'done)

(define (install-rational-package)
  ;; internal procedures
  (define (numer x) (car x))
  (define (denom x) (cdr x))
  (define (make-rat n d)
    (define (gcd a b)
      (if (= b 0)
          a
          (gcd b (remainder a b))))
    (let ((g (gcd n d)))
      (cons (/ n g) (/ d g))))
  (define (add-rat x y)
    (make-rat (+ (* (numer x) (denom y))
                 (* (numer y) (denom x)))
              (* (denom x) (denom y))))
  (define (sub-rat x y)
    (make-rat (- (* (numer x) (denom y))
                 (* (numer y) (denom x)))
              (* (denom x) (denom y))))
  (define (mul-rat x y)
    (make-rat (* (numer x) (numer y))
              (* (denom x) (denom y))))
  (define (div-rat x y)
    (make-rat (* (numer x) (denom y))
              (* (denom x) (numer y))))

  ;; interface to rest of the system
  (define (tag x) (attach-tag 'rational x))
  (put 'add '(rational rational)
       (lambda (x y) (tag (add-rat x y))))
  (put 'sub '(rational rational)
       (lambda (x y) (tag (sub-rat x y))))
  (put 'mul '(rational rational)
       (lambda (x y) (tag (mul-rat x y))))
  (put 'div '(rational rational)
       (lambda (x y) (tag (div-rat x y))))

  (put 'make 'rational
       (lambda (n d) (tag (make-rat n d))))
  'done)

(define (make-rational n d)
  ((get 'make 'rational) n d))

(define (install-rectangular-package)
  ;; internal procedures
  (define (real-part z) (car z))
  (define (imag-part z) (cdr z))
  (define (make-from-real-imag x y) (cons x y))
  (define (magnitude z)
    (sqrt (add (square (real-part z))
               (square (imag-part z)))))
  (define (angle z)
    (atan (imag-part z) (real-part z)))
  (define (make-from-mag-ang r a)
    (cons (* r (cos a)) (* r (sin a))))

  ;; interface to the rest of the system
  (define (tag x) (attach-tag 'rectangular x))
  (put 'real-part '(rectangular) real-part)
  (put 'imag-part '(rectangular) imag-part)
  (put 'magnitude '(rectangular) magnitude)
  (put 'angle '(rectangular) angle)
  (put 'make-from-real-imag 'rectangular
       (lambda (x y) (tag (make-from-real-imag x y))))
  (put 'make-from-mag-ang 'rectangular
       (lambda (r a) (tag (make-from-mag-ang r a))))
  'done)

(define (install-polar-package)
  ;; internal procedures
  (define (magnitude z) (car z))
  (define (angle z) (cdr z))
  (define (make-from-mag-ang r a) (cons r a))
  (define (real-part z)
    (* (magnitude z) (cos (angle z))))
  (define (imag-part z)
    (* (magnitude z) (sin (angle z))))
  (define (make-from-real-imag x y)
    (cons (sqrt (+ (square x) (square y)))
          (atan y x)))

  ;; interface to the rest of the system
  (define (tag x) (attach-tag 'polar x))
  (put 'real-part '(polar) real-part)
  (put 'imag-part '(polar) imag-part)
  (put 'magnitude '(polar) magnitude)
  (put 'angle '(polar) angle)
  (put 'make-from-real-imag 'polar
       (lambda (x y) (tag (make-from-real-imag x y))))
  (put 'make-from-mag-ang 'polar
       (lambda (r a) (tag (make-from-mag-ang r a))))
  'done)


(define (install-complex-package)
  ;; imported procedures from rectangular and polar packages
  (define (make-from-real-imag x y)
    ((get 'make-from-real-imag 'rectangular) x y))
  (define (make-from-mag-ang r a)
    ((get 'make-from-mag-ang 'polar) r a))

  ;; internal procedures
  (define (add-complex z1 z2)
    (make-from-real-imag (+ (real-part z1) (real-part z2))
                         (+ (imag-part z1) (imag-part z2))))
  (define (sub-complex z1 z2)
    (make-from-real-imag (- (real-part z1) (real-part z2))
                         (- (imag-part z1) (imag-part z2))))
  (define (mul-complex z1 z2)
    (make-from-mag-ang (* (magnitude z1) (magnitude z2))
                       (+ (angle z1) (angle z2))))
  (define (div-complex z1 z2)
    (make-from-mag-ang (/ (magnitude z1) (magnitude z2))
                       (- (angle z1) (angle z2))))

  ;; interface to rest of the system
  (define (tag z) (attach-tag 'complex z))
  (put 'add '(complex complex)
       (lambda (z1 z2) (tag (add-complex z1 z2))))
  (put 'sub '(complex complex)
       (lambda (z1 z2) (tag (sub-complex z1 z2))))
  (put 'mul '(complex complex)
       (lambda (z1 z2) (tag (mul-complex z1 z2))))
  (put 'div '(complex complex)
       (lambda (z1 z2) (tag (div-complex z1 z2))))
  (put 'make-from-real-imag 'complex
       (lambda (x y) (tag (make-from-real-imag x y))))
  (put 'make-from-mag-ang 'complex
       (lambda (r a) (tag (make-from-mag-ang r a))))
  (put 'real-part '(complex) real-part)
  (put 'imag-part '(complex) imag-part)
  (put 'magnitude '(complex) magnitude)
  (put 'angle '(complex) angle)
  'done)

(define (make-complex-from-real-imag x y)
  ((get 'make-from-real-imag 'complex) x y))

(define (make-complex-from-mag-ang r a)
  ((get 'make-from-mag-ang 'complex) r a))

(install-rectangular-package)
(install-polar-package)
(install-scheme-number-package)
(install-rational-package)
(install-complex-package)

(define (attach-tag type-tag contents)
  (cond [(eq? type-tag 'integer) contents]
        [(eq? type-tag 'real) contents]
        [else (cons type-tag contents)]))

(define (type-tag datum)
  (cond [(and (integer? datum) (exact? datum)) 'integer]
        [(number? datum) 'real]
        [(pair? datum) (car datum)]
        [else (error "Bad tagged datum -- TYPE-TAG" datum)]))

(define (contents datum)
  (cond [(and (integer? datum) (exact? datum)) datum]
        [(number? datum) datum]
        [(pair? datum) (cdr datum)]
        [else (error "Bad tagged datum -- CONTENTS" datum)]))

(define (install-integer-package)
  ;; internals
  (define (integer->rational n)
    (make-rational n 1))
  ;; definition
  (put '=zero? '(integer) (lambda (x) (= x 0)))
  (put 'equ? '(integer integer) (lambda (x y) (= x y)))
  (put 'add '(integer integer) (lambda (x y) (+ x y)))
  (put 'sub '(integer integer) (lambda (x y) (- x y)))
  (put 'mul '(integer integer) (lambda (x y) (* x y)))
  (put 'div '(integer integer) (lambda (x y) (/ x y)))
  (put 'raise '(integer) integer->rational)
  'done)

(define (install-rational-patch)
  ;; internals
  (define (numer x) (car x))
  (define (denom x) (cdr x))
  (define (rational->real x)
    (make-real (/. (numer x) (denom x))))
  (define (equ? x y)
    (= (* (numer x) (denom y))
       (* (numer y) (denom x))))
  ;; definition
  (put 'raise '(rational) rational->real)
  ;; for ex. 2.85
  (put 'project '(rational)
       (lambda (x)
         (round->exact (/ (numer x) (denom x)))))
  (put 'equ? '(rational rational) equ?)
  'done)

(define (install-real-package)
  ;; internals
  (define (real->complex x)
    (make-complex-from-real-imag x 0))
  ;; definition
  (put '=zero? '(real) (lambda (x) (= x 0)))
  (put 'equ? '(real real)
       (lambda (x y) (= x y)))
  (put 'add '(real real) +)
  (put 'sub '(real real) -)
  (put 'mul '(real real) *)
  (put 'div '(real real) /.)
  (put 'make '(real) identity)
  (put 'raise '(real) real->complex)
  ;; for ex 2.85
  (put 'project '(real)
       (lambda (x) (make-rational (round x) 1)))
  'done)

(define (make-real n) ((get 'make '(real)) (inexact n)))

(define (install-complex-patch)
  ;; internals
  (define (equ? x y)
    (and (= (real-part x) (real-part y))
         (= (imag-part x) (imag-part y))))
  ;; for ex 2.85
  (put 'project '(complex)
       (lambda (x)
         (make-real (real-part x))))
  (put 'equ? '(complex complex) equ?)
  'done)

;; generic proc
(define (raise x) (apply-generic 'raise x))
(define (equ? x y) (apply-generic 'equ? x y))
(define (add x y) (apply-generic 'add x y))
(define (sub x y) (apply-generic 'sub x y))
(define (mul x y) (apply-generic 'mul x y))
(define (div x y) (apply-generic 'div x y))
;; for ex 2.85
(define (project x) (apply-generic 'project x))

(install-integer-package)
(install-rational-patch)
(install-real-package)
(install-complex-patch)

;; ========== answer from here ==========
(define *type-order* '(integer rational real complex))
(define (type<? x y)
  (define (iter tower x y)
    (cond [(null? tower) #f]
          [(and (eq? x (car tower))
                (not (eq? y (car tower))))
           #t]
          [(and (not (eq? x (car tower)))
                (eq? y (car tower)))
           #f]
          [else (iter (cdr tower) x y)]))
  (iter *type-order* x y))
(define (type=? x y)
  (eq? x y))
(define (type>? x y)
  (not (or (type=? x y)
           (type<? x y))))

(define (drop x)
  (if (eq? (type-tag x) (car *type-order*))
      x
      (let ((dropped (project x)))
        (if (equ? (raise dropped) x)
            (drop dropped)
            x))))

(define (apply-generic op . args)
  (define (find-highest-type types)
    (fold (lambda (x y)
            (if (type>? x y)
                x
                y))
          'integer
          types))
  (define (x->type x type)
    (if (eq? (type-tag x) type)
        x
        (x->type (raise x) type)))
  (let ((proc (get op (map type-tag args)))
        (maybe-drop (if (memq op '(equ? =zero? raise project))
                        identity
                        drop)))
    (if proc
        (maybe-drop (apply proc (map contents args)))
        (let* ((highest-type (find-highest-type (map type-tag args)))
               (new-args (map (lambda (x) (x->type x highest-type)) args))
               (proc (get op (map type-tag new-args))))
          (if proc
              (maybe-drop (apply proc (map contents new-args)))
              (error "no such method -- APPLY-GENERIC"
                     (list op args)))))))
