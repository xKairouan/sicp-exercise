;; -*- coding: utf-8 -*-

;; *Exercise 3.26:* To search a table as implemented above, one needs
;; to scan through the list of records.  This is basically the
;; unordered list representation of section *Note 2-3-3::.  For large
;; tables, it may be more efficient to structure the table in a
;; different manner.  Describe a table implementation where the (key,
;; value) records are organized using a binary tree, assuming that
;; keys can be ordered in some way (e.g., numerically or
;; alphabetically).  (Compare *Note Exercise 2-66:: of *Note Chapter
;; 2::.)

;; Tree
(define (make-tree entry left right) (list entry left right))
(define (entry tree) (car tree))
(define (left-branch tree) (cadr tree))
(define (right-branch tree) (caddr tree))
(define (set-entry! tree entry) (set-car! tree entry))
(define (set-left-branch! tree1 tree2) (set-car! (cdr tree1) tree2))
(define (set-right-branch! tree1 tree2) (set-car! (cddr tree1) tree2))

;; Table
(define (make-table)
  (let ((local-tree '()))
    (define (lookup key)
      (define (iter tree key)
        (cond [(null? tree) #f]
              [(< key (car (entry tree))) (iter (left-branch tree) key)]
              [(> key (car (entry tree))) (iter (right-branch tree) key)]
              [else (cdr (entry tree))]))
      (iter local-tree key))
    (define (insert! key value)
      (define (iter! tree key value)
        (let ((current-key (car (entry tree))))
          (cond [(< key current-key)
                 (if (null? (left-branch tree))
                     (set-left-branch! tree
                                       (make-tree (cons key value) '() '()))
                     (iter! (left-branch tree) key value))]
                [(> key current-key)
                 (if (null? (right-branch tree))
                     (set-right-branch! tree
                                        (make-tree (cons key value) '() '()))
                     (iter! (right-branch tree) key value))]
                [else (set-entry! tree (cons key value))])))
      (if (null? local-tree)
          (set! local-tree (make-tree (cons key value) '() '()))
          (iter! local-tree key value)))
    (define (dispatch m)
      (cond [(eq? m 'lookup) lookup]
            [(eq? m 'insert!) insert!]
            [else (error "no such method" m)]))
    dispatch))

(define (lookup table key) ((table 'lookup) key))
(define (insert! table key value) ((table 'insert!) key value))


;; test
(use gauche.test)
(test-start "exercise 3.26")
(define table (make-table))
(insert! table 7 'a)
(insert! table 3 'b)
(insert! table 5 'c)
(insert! table 9 'd)
(insert! table 1 'e)
(test* "7 => a" 'a (lookup table 7))
(test* "3 => b" 'b (lookup table 3))
(test* "9 => d" 'd (lookup table 9))
(test* "11 => #f" #f (lookup table 11))
(test* "2 => #f" #f (lookup table 2))
(insert! table 7 'aa)
(insert! table 5 'cc)
(insert! table 1 'ee)
(test* "7 => aa" 'aa (lookup table 7))
(test* "5 => cc" 'cc (lookup table 5))
(test* "1 => ee" 'ee (lookup table 1))
(test-end)
