;; -*- coding: utf-8 -*-

(define *proc-table* (make-hash-table 'equal?))
(define (get op type) (hash-table-get *proc-table* (list op type) #f))
(define (put op type item) (hash-table-put! *proc-table* (list op type) item))

;; arithmetic
(define (attach-tag type-tag contents)
  (cond [(eq? type-tag 'integer) contents]
        [(eq? type-tag 'real) contents]
        [else (cons type-tag contents)]))

(define (type-tag datum)
  (cond [(and (integer? datum) (exact? datum)) 'integer]
        [(number? datum) 'real]
        [(pair? datum) (car datum)]
        [else (error "Bad tagged datum -- TYPE-TAG" datum)]))

(define (contents datum)
  (cond [(and (integer? datum) (exact? datum)) datum]
        [(number? datum) datum]
        [(pair? datum) (cdr datum)]
        [else (error "Bad tagged datum -- CONTENTS" datum)]))

(define (install-integer-package)
  ;; internals
  (define (integer->rational n)
    (make-rational n 1))
  ;; definition
  (put '=zero? '(integer) (lambda (x) (= x 0)))
  (put 'equ? '(integer integer) (lambda (x y) (= x y)))
  (put 'add '(integer integer) (lambda (x y) (+ x y)))
  (put 'sub '(integer integer) (lambda (x y) (- x y)))
  (put 'mul '(integer integer) (lambda (x y) (* x y)))
  (put 'div '(integer integer) (lambda (x y) (/ x y)))
  (put 'raise '(integer) integer->rational)
  'done)


;; polynomial
(define (install-polynomial-package)
  ;; internal procedures
  ;; representation of poly
  (define (make-poly variable term-list)
    (cons variable term-list))
  (define (variable p) (car p))
  (define (term-list p) (cdr p))
  (define (same-variable? x y) (and (eq? x y)))
  (define (variable? v) (symbol? v))

  ;; representation of terms and term lists
  (define (adjoin-term term term-list)
    (if (=zero? (coeff term))
        term-list
        (cons term term-list)))

  (define (the-empty-termlist) '())
  (define (first-term term-list) (car term-list))
  (define (rest-terms term-list) (cdr term-list))
  (define (empty-termlist? term-list) (null? term-list))

  (define (make-term order coeff) (list order coeff))
  (define (order term) (car term))
  (define (coeff term) (cadr term))

  ;; continued on next page
  (define (add-poly p1 p2)
    (if (same-variable? (variable p1) (variable p2))
        (make-poly (variable p1)
                   (add-terms (term-list p1)
                              (term-list p2)))
        (error "Polys not in same var -- ADD-POLY"
               (list p1 p2))))

  (define (mul-poly p1 p2)
    (if (same-variable? (variable p1) (variable p2))
        (make-poly (variable p1)
                   (mul-terms (term-list p1)
                              (term-list p2)))
        (error "Polys not in same var -- MUL-POLY"
               (list p1 p2))))

  ;; term manipulation
  (define (add-terms L1 L2)
    (cond ((empty-termlist? L1) L2)
          ((empty-termlist? L2) L1)
          (else
           (let ((t1 (first-term L1)) (t2 (first-term L2)))
             (cond ((> (order t1) (order t2))
                    (adjoin-term
                     t1 (add-terms (rest-terms L1) L2)))
                   ((< (order t1) (order t2))
                    (adjoin-term
                     t2 (add-terms L1 (rest-terms L2))))
                   (else
                    (adjoin-term
                     (make-term (order t1)
                                (add (coeff t1) (coeff t2)))
                     (add-terms (rest-terms L1)
                                (rest-terms L2)))))))))

  (define (mul-terms L1 L2)
    (if (empty-termlist? L1)
        (the-empty-termlist)
        (add-terms (mul-term-by-all-terms (first-term L1) L2)
                   (mul-terms (rest-terms L1) L2))))

  (define (mul-term-by-all-terms t1 L)
    (if (empty-termlist? L)
        (the-empty-termlist)
        (let ((t2 (first-term L)))
          (adjoin-term
           (make-term (+ (order t1) (order t2))
                      (mul (coeff t1) (coeff t2)))
           (mul-term-by-all-terms t1 (rest-terms L))))))

  (define (zero-poly? poly)
    (define (zero-term? term)
      (if (empty-termlist? term)
          #t
          (and (=zero? (coeff (first-term term)))
               (=zero-term? (rest-terms term)))))
    (zero-term? (term-list poly)))

  ;; interface to rest of the system
  (define (tag p) (attach-tag 'polynomial p))
  (put 'add '(polynomial polynomial)
       (lambda (p1 p2) (tag (add-poly p1 p2))))
  (put 'mul '(polynomial polynomial)
       (lambda (p1 p2) (tag (mul-poly p1 p2))))
  (put 'make 'polynomial
       (lambda (var terms) (tag (make-poly var terms))))
  (put '=zero? '(polynomial) zero-poly?)
  'done)

(install-polynomial-package)

(define (make-polynomial var terms)
  ((get 'make 'polynomial) var terms))

(define *type-order* '(integer rational real complex))
(define (type<? x y)
  (define (iter tower x y)
    (cond [(null? tower) #f]
          [(and (eq? x (car tower))
                (not (eq? y (car tower))))
           #t]
          [(and (not (eq? x (car tower)))
                (eq? y (car tower)))
           #f]
          [else (iter (cdr tower) x y)]))
  (iter *type-order* x y))
(define (type=? x y)
  (eq? x y))
(define (type>? x y)
  (not (or (type=? x y)
           (type<? x y))))

(define (apply-generic op . args)
  (define (find-highest-type types)
    (fold (lambda (x y)
            (if (type>? x y)
                x
                y))
          'integer
          types))
  (define (x->type x type)
    (if (eq? (type-tag x) type)
        x
        (x->type (raise x) type)))
  (let ((proc (get op (map type-tag args))))
    (if proc
        (apply proc (map contents args))
        (let* ((highest-type (find-highest-type (map type-tag args)))
               (new-args (map (lambda (x) (x->type x highest-type)) args))
               (proc (get op (map type-tag new-args))))
          (if proc
              (apply proc (map contents new-args))
              (error "no such method -- APPLY-GENERIC"
                     (list op args)))))))

(define (=zero? x) (apply-generic '=zero? x))

;; test
(use gauche.test)
(test-start "exercise 2.87")
(test* "=zero?" #f (=zero? (make-polynomial 'x '((1 1)))))
(test-end)
