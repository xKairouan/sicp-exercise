;; -*- coding: utf-8 -*-

;; *Exercise 3.51:* In order to take a closer look at delayed
;; evaluation, we will use the following procedure, which simply
;; returns its argument after printing it:

;;      (define (show x)
;;        (display-line x)
;;        x)

;; What does the interpreter print in response to evaluating each
;; expression in the following sequence?(7)

;;      (define x (stream-map show (stream-enumerate-interval 0 10)))

;;      (stream-ref x 5)

;;      (stream-ref x 7)

(use util.stream)


(define (display-line x)
  (newline)
  (display x))
(define (show x)
  (display-line x)
  x)

(define (stream-enumerate-interval low high)
  (if (> low high)
      stream-null
      (stream-cons low (stream-enumerate-interval (+ low 1) high))))

(define x (stream-map show (stream-enumerate-interval 0 10)))

;; 1
;; 2
;; 3
;; 4
;; 5
(stream-ref x 5)

;; 6
;; 7
(stream-ref x 7)
