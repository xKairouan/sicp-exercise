;; -*- coding: utf-8 -*-

;; *Exercise 2.75:* Implement the constructor `make-from-mag-ang' in
;; message-passing style.  This procedure should be analogous to the
;; `make-from-real-imag' procedure given above.

(define (make-from-mag-ang r a)
  (define (dispatch op)
    (cond [(eq? op 'real-part) (* r (cos a))]
          [(eq? op 'imag-part) (* r (sin a))]
          [(eq? op 'magnitude) r]
          [(eq? op 'angle) a]
          [else (error "Unknown OP -- MAKE-FROM-MAG-ANG" op)]))
  dispatch)

(define (apply-generic op arg) (arg op))

;; test
(use math.const)
(define (nealy-equal? x y)
  (< (abs (- x y)) 1e-10))
(use gauche.test)
(test-start "exercise 2.75")
(let ((z (make-from-mag-ang 2 (/ pi 6))))
  (test* "real-part" (sqrt 3) (apply-generic 'real-part z) nealy-equal?)
  (test* "imag-part" 1 (apply-generic 'imag-part z) nealy-equal?)
  (test* "magnitude" 2 (apply-generic 'magnitude z) nealy-equal?)
  (test* "angle" (/ pi 6) (apply-generic 'angle z) nealy-equal?))
(test-end)