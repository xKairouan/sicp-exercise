;; -*- coding: utf-8 -*-

;; *Exercise 2.42:* The "eight-queens puzzle" asks how to place eight
;; queens on a chessboard so that no queen is in check from any other
;; (i.e., no two queens are in the same row, column, or diagonal).
;; One possible solution is shown in *Note Figure 2-8::.  One way to
;; solve the puzzle is to work across the board, placing a queen in
;; each column.  Once we have placed k - 1 queens, we must place the
;; kth queen in a position where it does not check any of the queens
;; already on the board.  We can formulate this approach recursively:
;; Assume that we have already generated the sequence of all possible
;; ways to place k - 1 queens in the first k - 1 columns of the
;; board.  For each of these ways, generate an extended set of
;; positions by placing a queen in each row of the kth column.  Now
;; filter these, keeping only the positions for which the queen in
;; the kth column is safe with respect to the other queens.  This
;; produces the sequence of all ways to place k queens in the first k
;; columns.  By continuing this process, we will produce not only one
;; solution, but all solutions to the puzzle.

;; We implement this solution as a procedure `queens', which returns a
;; sequence of all solutions to the problem of placing n queens on an
;; n*n chessboard.  `Queens' has an internal procedure `queen-cols'
;; that returns the sequence of all ways to place queens in the first
;; k columns of the board.

;;      (define (queens board-size)
;;        (define (queen-cols k)
;;          (if (= k 0)
;;              (list empty-board)
;;              (filter
;;               (lambda (positions) (safe? k positions))
;;               (flatmap
;;                (lambda (rest-of-queens)
;;                  (map (lambda (new-row)
;;                         (adjoin-position new-row k rest-of-queens))
;;                       (enumerate-interval 1 board-size)))
;;                (queen-cols (- k 1))))))
;;        (queen-cols board-size))

;; In this procedure `rest-of-queens' is a way to place k - 1 queens
;; in the first k - 1 columns, and `new-row' is a proposed row in
;; which to place the queen for the kth column.  Complete the program
;; by implementing the representation for sets of board positions,
;; including the procedure `adjoin-position', which adjoins a new
;; row-column position to a set of positions, and `empty-board',
;; which represents an empty set of positions.  You must also write
;; the procedure `safe?', which determines for a set of positions,
;; whether the queen in the kth column is safe with respect to the
;; others.  (Note that we need only check whether the new queen is
;; safe--the other queens are already guaranteed safe with respect to
;; each other.)

(use srfi-1)

(define (flatmap proc seq)
  (fold-right append '() (map proc seq)))

(define (enumerate-interval low high)
  (define (iter n result)
    (if (< n low)
        result
        (iter (- n 1) (cons n result))))
  (iter high '()))

;; answer
(define (position-row position) (car position))
(define (position-col position) (cadr position))
(define (position-equal? pos1 pos2) (and (= (position-row pos1)
                                            (position-row pos2))
                                         (= (position-col pos1)
                                            (position-col pos2))))

(define (adjoin-position row col positions)
  (cons (list row col) positions))

(define empty-board '())

(define (safe? col positions)
  (let ((here (car (filter (lambda (pos) (= (position-col pos) col))
                           positions)))
        (others (filter (lambda (pos) (not (= (position-col pos) col)))
                        positions)))
    (define (generate-candidates here proc)
      (define (iter here result)
        (cond [(= (position-col here) 0) result]
              [(= (position-row here) 0) result]
              [else (iter (proc here) (cons here result))]))
      (iter here '()))
    (define (match-candidate? positions candidates)
      (fold (lambda (x y) (or x y))
            #f
            (map (lambda (candidate)
                   (fold (lambda (x y) (or x y))
                         #f
                         (map (lambda (pos)
                                (position-equal? pos candidate))
                              positions)))
                 candidates)))
    (define (go-left pos)
      (list (position-row pos)
            (- (position-col pos) 1)))
    (define (go-up-left pos)
      (list (- (position-row pos) 1)
            (- (position-col pos) 1)))
    (define (go-down-left pos)
      (list (+ (position-row pos) 1)
            (- (position-col pos) 1)))
    (define (direction-safe? direct-proc)
      (let ((candidates (generate-candidates here direct-proc)))
        (not (match-candidate? others candidates))))
    (and (direction-safe? go-left)
         (direction-safe? go-up-left)
         (direction-safe? go-down-left))))

(define (queens board-size)
  (define (queen-cols k)
    (if (= k 0)
        (list empty-board)
        (filter
         (lambda (positions) (safe? k positions))
         (flatmap
          (lambda (rest-of-queens)
            (map (lambda (new-row)
                   (adjoin-position new-row k rest-of-queens))
                 (enumerate-interval 1 board-size)))
          (queen-cols (- k 1))))))
  (queen-cols board-size))

(define (print-board positions)
  )
(for-each print (queens 8))
