;; -*- coding: utf-8 -*-

;; *Exercise 4.6:* `Let' expressions are derived expressions, because

;;      (let ((<VAR_1> <EXP_1>) ... (<VAR_N> <EXP_N>))
;;        <BODY>)

;; is equivalent to

;;      ((lambda (<VAR_1> ... <VAR_N>)
;;         <BODY>)
;;       <EXP_1>
;;       ...
;;       <EXP_N>)

;; Implement a syntactic transformation `let->combination' that
;; reduces evaluating `let' expressions to evaluating combinations of
;; the type shown above, and add the appropriate clause to `eval' to
;; handle `let' expressions.

(load "./metacircular.scm")

(define (let? exp) (tagged-list? exp 'let))
(define (let-vars exp)
  (let ((var-exp-list (cadr exp)))
    (fold-right (lambda (x result) (cons (car x) result))
                '()
                var-exp-list)))
(define (let-var-values exp)
  (let ((var-exp-list (cadr exp)))
    (fold-right (lambda (x result) (cons (cadr x) result))
                '()
                var-exp-list)))
(define (let-body exp) (cddr exp))
(define (let->combination exp)
  (cons (make-lambda (let-vars exp)
                     (let-body exp))
        (let-var-values exp)))

(define (eval exp env)
  (cond [(self-evaluating? exp) exp]
        [(variable? exp) (lookup-variable-value exp env)]
        [(quoted? exp) (text-of-quotation exp)]
        [(assignment? exp) (eval-assignment exp env)]
        [(definition? exp) (eval-definition exp env)]
        [(if? exp) (eval-if exp env)]
        [(lambda? exp)
         (make-procedure (lambda-parameters exp)
                         (lambda-body exp)
                         env)]
        [(begin? exp)
         (eval-sequence (begin-actions exp) env)]
        [(cond? exp) (eval (cond->if exp) env)]
        [(let? exp) (eval (let->combination exp) env)]
        [(application? exp)
         (apply (eval (operator exp) env)
                (list-of-values (operands exp) env))]
        [else
         (error "Unknown expression type -- EVAL" exp)]))

(define primitive-procedures
  (list (list 'car car)
        (list 'cdr cdr)
        (list 'cons cons)
        (list 'null? null?)
        (list '+ +)
        ;;<MORE PRIMITIVES>
        ))

(define the-global-environment (setup-environment))
(driver-loop)
