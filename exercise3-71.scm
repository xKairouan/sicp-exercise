;; -*- coding: utf-8 -*-

;; *Exercise 3.71:* Numbers that can be expressed as the sum of two
;; cubes in more than one way are sometimes called "Ramanujan
;; numbers", in honor of the mathematician Srinivasa Ramanujan.(6)
;; Ordered streams of pairs provide an elegant solution to the
;; problem of computing these numbers.  To find a number that can be
;; written as the sum of two cubes in two different ways, we need
;; only generate the stream of pairs of integers (i,j) weighted
;; according to the sum i^3 + j^3 (see *Note Exercise 3-70::), then
;; search the stream for two consecutive pairs with the same weight.
;; Write a procedure to generate the Ramanujan numbers.  The first
;; such number is 1,729.  What are the next five?

(load "./stream.scm")

(define (merge-weighted s1 s2 weight)
  (cond ((stream-null? s1) s2)
        ((stream-null? s2) s1)
        (else
         (let ((s1car (stream-car s1))
               (s2car (stream-car s2)))
           (let ((w1 (apply weight s1car))
                 (w2 (apply weight s2car)))
             (if (< w1 w2)
                 (cons-stream s1car
                              (merge-weighted (stream-cdr s1) s2 weight))
                 (cons-stream s2car
                              (merge-weighted s1 (stream-cdr s2) weight))))))))

(define (weighted-pairs s t weight)
  (cons-stream
   (list (stream-car s) (stream-car t))
   (merge-weighted (stream-map (lambda (x) (list (stream-car s) x))
                               (stream-cdr t))
                   (weighted-pairs (stream-cdr s) (stream-cdr t) weight)
                   weight)))

(define ones (cons-stream 1 ones))
(define integers (cons-stream 1
                              (stream-map + ones integers)))


;; answer
(define (generate-ramanujan-stream s)
  (define (cubic x) (* x x x))
  (define (sum-of-cubic x y) (+ (cubic x) (cubic y)))
  (define (sum-of-cubic-equal? pair1 pair2)
    (= (sum-of-cubic (car pair1) (cadr pair1))
       (sum-of-cubic (car pair2) (cadr pair2))))
  (let ((s0 (stream-ref s 0))
        (s1 (stream-ref s 1)))
    (if (sum-of-cubic-equal? s0 s1)
        (cons-stream (sum-of-cubic (car s0) (cadr s0))
                     (generate-ramanujan-stream
                      (stream-filter (lambda (pair)
                                       (not (sum-of-cubic-equal? s0 pair)))
                                     s)))
        (generate-ramanujan-stream (stream-cdr s)))))



(define ramanujan-stream
  (generate-ramanujan-stream (weighted-pairs integers
                                             integers
                                             (lambda (i j) (+ (* i i i)
                                                              (* j j j))))))
;; => (1729 4104 13832 20683 32832 39312 40033 46683 64232 65728)
(print (stream->list (stream-take ramanujan-stream 10)))
