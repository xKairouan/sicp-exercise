;; -*- coding: utf-8 -*-

;; *Exercise 3.52:* Consider the sequence of expressions

;;      (define sum 0)

;;      (define (accum x)
;;        (set! sum (+ x sum))
;;        sum)

;;      (define seq (stream-map accum (stream-enumerate-interval 1 20)))
;;      (define y (stream-filter even? seq))
;;      (define z (stream-filter (lambda (x) (= (remainder x 5) 0))
;;                               seq))

;;      (stream-ref y 7)

;;      (display-stream z)

;; What is the value of `sum' after each of the above expressions is
;; evaluated?  What is the printed response to evaluating the
;; `stream-ref' and `display-stream' expressions?  Would these
;; responses differ if we had implemented `(delay <EXP>)' simply as
;; `(lambda () <EXP>)' without using the optimization provided by
;; `memo-proc'?  Explain


(use util.stream)

(define sum 0)
(define (accum x)
  (set! sum (+ x sum))
  sum)
(define (stream-enumerate-interval low high)
  (if (> low high)
      stream-null
      (stream-cons low (stream-enumerate-interval (+ low 1) high))))
(define (display-stream stream)
  (define (display-line x)
    (newline)
    (display x))
  (stream-for-each display-line stream))

;; sum = 0
(define seq (stream-map accum (stream-enumerate-interval 1 20)))
(define y (stream-filter even? seq))
(define z (stream-filter (lambda (x) (= (remainder x 5) 0))
                         seq))
;; sum = 136, response = 136
(stream-ref y 7)

;; sum = 210
;; response:
;; 10
;; 15
;; 45
;; 55
;; 105
;; 120
;; 190
;; 210
;; done
(display-stream z)


;; delay がメモ化していないと、zの結果が変わる
;; accum が複数回実行されるので
;; sum = (fold + 136 (iota 20 1)) = 376
;; response:
;; done