;; -*- coding: utf-8 -*-

;; *Exercise 2.14:* Demonstrate that Lem is right.  Investigate the
;; behavior of the system on a variety of arithmetic expressions.
;; Make some intervals A and B, and use them in computing the
;; expressions A/A and A/B.  You will get the most insight by using
;; intervals whose width is a small percentage of the center value.
;; Examine the results of the computation in center-percent form (see
;; *Note Exercise 2-12::).

(define make-interval cons)
(define lower-bound car)
(define upper-bound cdr)

(define (make-center-percent center percent)
  (let ((width (* center (/. percent 100))))
    (make-interval (- center width)
                   (+ center width))))
(define (center interval)
  (/. (+ (lower-bound interval) (upper-bound interval)) 2))
(define (percent interval)
  (let ((width (/. (- (upper-bound interval) (lower-bound interval)) 2)))
    (/. (* 100 width) (center interval))))

(define (add-interval x y)
  (make-interval (+ (lower-bound x) (lower-bound y))
                 (+ (upper-bound x) (upper-bound y))))
(define (mul-interval x y)
  (let ((p1 (* (lower-bound x) (lower-bound y)))
        (p2 (* (lower-bound x) (upper-bound y)))
        (p3 (* (upper-bound x) (lower-bound y)))
        (p4 (* (upper-bound x) (upper-bound y))))
    (make-interval (min p1 p2 p3 p4)
                   (max p1 p2 p3 p4))))
(define (div-interval x y)
  (mul-interval x
                (make-interval (/ 1.0 (upper-bound y))
                               (/ 1.0 (lower-bound y)))))

(define (par1 r1 r2)
  (div-interval (mul-interval r1 r2)
                (add-interval r1 r2)))

(define (par2 r1 r2)
  (let ((one (make-interval 1 1)))
    (div-interval one
                  (add-interval (div-interval one r1)
                                (div-interval one r2)))))

;; test
(let ((A (make-center-percent 10 1))
      (B (make-center-percent 20 2)))
  (display (par1 A B))
  (newline)
  (display (par2 A B)))
