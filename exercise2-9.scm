;; -*- coding: utf-8 -*-

;; *Exercise 2.9:* The "width" of an interval is half of the
;; difference between its upper and lower bounds.  The width is a
;; measure of the uncertainty of the number specified by the
;; interval.  For some arithmetic operations the width of the result
;; of combining two intervals is a function only of the widths of the
;; argument intervals, whereas for others the width of the
;; combination is not a function of the widths of the argument
;; intervals.  Show that the width of the sum (or difference) of two
;; intervals is a function only of the widths of the intervals being
;; added (or subtracted).  Give examples to show that this is not
;; true for multiplication or division.


;; answer:
;; width((x1, x2) + (y1, y2))
;; = width((x1+y1), (x2+y2))
;; = 1/2 * ((x2+y2) - (x1+y1))
;; = 1/2 * (x2 - x1) + 1/2 * (y2 - y1)
;; = width(x1, x2) + width(y1, y2)
;;
;; width((x1, x2) - (y1, y2))
;; = width((x1-y2), (x2-y1))
;; = 1/2 * ((x2-y1) - (x1-y2))
;; = 1/2 * (x2 - x1) - 1/2 * (y2 - y1)
;; = width(x1, x2) - width(y1, y2)
;;                             (Q.E.D)

;; multiplication example
;;  width((1, 2) * (8, 9)) = width(8, 18) = 10
;;  width((0, 1) * (8, 9)) = width(0, 9) = 9
;;  同じ幅同士の乗算でも結果の幅が異なる。
