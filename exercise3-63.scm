;; -*- coding: utf-8 -*-

;; *Exercise 3.63:* Louis Reasoner asks why the `sqrt-stream'
;; procedure was not written in the following more straightforward
;; way, without the local variable `guesses':

;;      (define (sqrt-stream x)
;;        (cons-stream 1.0
;;                     (stream-map (lambda (guess)
;;                                   (sqrt-improve guess x))
;;                                 (sqrt-stream x))))

;; Alyssa P. Hacker replies that this version of the procedure is
;; considerably less efficient because it performs redundant
;; computation.  Explain Alyssa's answer.  Would the two versions
;; still differ in efficiency if our implementation of `delay' used
;; only `(lambda () <EXP>)' without using the optimization provided
;; by `memo-proc' (section *Note 3-5-1::)?


;; original
(define (sqrt-improve guess x)
  (average guess (/ x guess)))

(define (sqrt-stream x)
  (define guesses
    (cons-stream 1.0
                 (stream-map (lambda (guess)
                               (sqrt-improve guess x))
                             guesses)))
  guesses)

;; Louis の方だと毎回 sqrt-stream が呼ばれて stream が生成されてしまう。
;; original では guesses が参照するストリームだけがつくられるため、効率が良い。
