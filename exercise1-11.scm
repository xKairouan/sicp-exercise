;; -*- coding: utf-8 -*-

;; *Exercise 1.11:* A function f is defined by the rule that f(n) = n
;; if n<3 and f(n) = f(n - 1) + 2f(n - 2) + 3f(n - 3) if n>= 3.
;; Write a procedure that computes f by means of a recursive process.
;; Write a procedure that computes f by means of an iterative
;; process.

(define (f n)
  (if (< n 3)
      n
      (+ (f (- n 1)) (* 2 (f (- n 2))) (* 3 (f (- n 3))))))

(define (f2 n)
  (define (f-iter a b c count)
    (if (< count 1)
        c
        (f-iter (+ a (* 2 b) (* 3 c)) a b (- count 1))))
  (f-iter 2 1 0 n))

(use gauche.test)
(test-start "sicp exercise 1.11")
(test* "f and f2 are same function" (f 0) (f2 0))
(test* "f and f2 are same function" (f 1) (f2 1))
(test* "f and f2 are same function" (f 3) (f2 3))
(test* "f and f2 are same function" (f 4) (f2 4))
(test* "f and f2 are same function" (f 10) (f2 10))
(test-end)
