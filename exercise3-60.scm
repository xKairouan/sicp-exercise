;; -*- coding: utf-8 -*-

;; *Exercise 3.60:* With power series represented as streams of
;; coefficients as in *Note Exercise 3-59::, adding series is
;; implemented by `add-streams'.  Complete the definition of the
;; following procedure for multiplying series:

;;      (define (mul-series s1 s2)
;;        (cons-stream <??> (add-streams <??> <??>)))

;; You can test your procedure by verifying that sin^2 x + cos^2 x =
;; 1, using the series from *Note Exercise 3-59::.

(load "./stream.scm")

;; from ex. 3.59
(define ones (cons-stream 1 ones))
(define integers  (cons-stream 1 (stream-map + ones integers)))
(define (integrate-series a-stream)
  (stream-map * (stream-map (lambda (x) (/ 1 x)) integers)
                a-stream))

(define cosine-series
  (cons-stream 1 (stream-map - (integrate-series sine-series))))
(define sine-series
  (cons-stream 0 (integrate-series cosine-series)))

;; answer from here
(define (add-streams s1 s2)
  (stream-map + s1 s2))

(define (scale-stream stream factor)
  (stream-map (lambda (x) (* x factor)) stream))

(define (mul-series s1 s2)
  (cons-stream (* (stream-car s1)
                  (stream-car s2))
               (add-streams (scale-stream (stream-cdr s2) (stream-car s1))
                            (mul-series (stream-cdr s1) s2))))
