;; -*- coding: utf-8 -*-

;; *Exercise 1.12:* The following pattern of numbers is called "Pascal's
;; triangle".

;;              1
;;            1   1
;;          1   2   1
;;        1   3   3   1
;;      1   4   6   4   1

;; The numbers at the edge of the triangle are all 1, and each number
;; inside the triangle is the sum of the two numbers above it.(4)
;; Write a procedure that computes elements of Pascal's triangle by
;; means of a recursive process.

(define (pascal-triangle n r)
  (cond ((= n 0) 1)
        ((= r 0) 1)
        ((= r n) 1)
        (else (+ (pascal-triangle (- n 1) (- r 1))
                 (pascal-triangle (- n 1) r)))))

(use gauche.test)
(test-start "exercise 1.12")
(test* "(0, 0)" 1 (pascal-triangle 0 0))
(test* "(1, 0)" 1 (pascal-triangle 1 0))
(test* "(0, 1)" 1 (pascal-triangle 0 1))
(test* "(10, 0)" 1 (pascal-triangle 10 0))
(test* "(0, 10)" 1 (pascal-triangle 0 10))
(test* "(3, 2)" 3 (pascal-triangle 3 2))
(test* "(4, 2)" 6 (pascal-triangle 4 2))
(test-end)
