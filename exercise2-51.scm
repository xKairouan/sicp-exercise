;; -*- coding: utf-8 -*-

;; *Exercise 2.51:* Define the `below' operation for painters.
;; `Below' takes two painters as arguments.  The resulting painter,
;; given a frame, draws with the first painter in the bottom of the
;; frame and with the second painter in the top.  Define `below' in
;; two different ways--first by writing a procedure that is analogous
;; to the `beside' procedure given above, and again in terms of
;; `beside' and suitable rotation operations (from *Note Exercise
;; 2-50::).

(use gl)
(use gl.glut)

(add-load-path ".")

(use painter)

;; below -- analogous to the beside procedure
(define (below painter1 painter2)
  (let ((split-point (make-vect 0.0 0.5)))
    (define paint-bottom
      (transform-painter painter1
                         (make-vect 0.0 0.0)
                         (make-vect 1.0 0.0)
                         split-point))
    (define paint-up
      (transform-painter painter2
                         split-point
                         (make-vect 1.0 0.5)
                         (make-vect 0.0 1.0)))
    (lambda (frame)
      (paint-bottom frame)
      (paint-up frame))))

;; below2 -- use rotation
(define (below2 painter1 painter2)
  (rotate270 (beside (rotate90 painter2) (rotate90 painter1))))

;; drawing
(define (init)
  (gl-clear-color 1.0 1.0 1.0 1.0))

(define (draw)
  (gl-clear GL_COLOR_BUFFER_BIT)
  (gl-color 0.0 0.0 0.0 0.0)
  (gl-begin GL_LINE_LOOP)
  ;; !!!write painter here!!!
  ;; ((below wave x-mark) frame)
  ((below2 wave x-mark) frame)
  (gl-end)
  (gl-flush))

(define (main args)
  (glut-init args)
  (glut-create-window "Painter")
  (glut-display-func draw)
  (init)
  (glut-main-loop))
