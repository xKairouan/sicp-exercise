;; -*- coding: utf-8 -*-

;; *Exercise 2.35:* Redefine `count-leaves' from section *Note
;; 2-2-2:: as an accumulation:

;;      (define (count-leaves t)
;;        (accumulate <??> <??> (map <??> <??>)))

(define (accumulate op initial sequence)
  (if (null? sequence)
      initial
      (op (car sequence)
          (accumulate op initial (cdr sequence)))))

(define (count-leaves t)
  (accumulate + 0 (map (lambda (x)
                         (if (pair? x)
                             (count-leaves x)
                             1)) t)))

(use gauche.test)
(test-start "exercise 2.35")
(test* "count-leaves" 5 (count-leaves '(1 (2 (3) 4) 5)))
(test* "count-leaves 2" 6 (count-leaves '(((1 2) (3 4)) (5 6))))
(test-end)