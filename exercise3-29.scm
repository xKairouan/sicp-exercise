;; -*- coding: utf-8 -*-

;; *Exercise 3.29:* Another way to construct an or-gate is as a
;; compound digital logic device, built from and-gates and inverters.
;; Define a procedure `or-gate' that accomplishes this.  What is the
;; delay time of the or-gate in terms of `and-gate-delay' and
;; `inverter-delay'?

(define (or-gate a b output)
  (let ((c (make-wrre))
        (d (make-wire))
        (e (make-wire)))
    (inverter a c)
    (inverter b d)
    (and-gate c d e)
    (inverter e output)
    'ok))

;; or-delay = 2 * inverter-delay + and-delay
