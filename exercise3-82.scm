;; -*- coding: utf-8 -*-

;; *Exercise 3.82:* Redo *Note Exercise 3-5:: on Monte Carlo
;; integration in terms of streams.  The stream version of
;; `estimate-integral' will not have an argument telling how many
;; trials to perform.  Instead, it will produce a stream of estimates
;; based on successively more trials.

(use srfi-27)

(load "./stream.scm")

(define random-init 1)

(define (random-in-range-stream low high)
  (define (random-in-range low high)
    (let ((range (- high low)))
      (+ low (random-integer range))))
  (cons-stream (random-in-range low high)
               (random-in-range-stream low high)))

(define (map-successive-pair f s)
  (cons-stream
   (f (stream-ref s 0) (stream-ref s 1))
   (map-successive-pair f
                        (stream-cdr (stream-cdr s)))))

(define (interleave s1 s2)
  (cond [(stream-null? s1) s2]
        [(stream-null? s2) s1]
        [else (cons-stream (stream-car s1)
                           (interleave s2 (stream-cdr s1)))]))

(define (estimate-integral pred x1 x2 y1 y2)
  (define (next stream passed failed)
    (cons-stream (/ passed (+ passed failed))
                 (if (stream-car stream)
                     (next (stream-cdr stream) (+ passed 1) failed)
                     (next (stream-cdr stream) passed (+ failed 1)))))
  (let ((x-stream (random-in-range-stream x1 x2))
        (y-stream (random-in-range-stream y1 y2)))
    (let ((coord-stream (interleave x-stream y-stream)))
      (next (map-successive-pair pred coord-stream) 0 0))))


(define pi
  (stream-map (lambda (s) (exact->inexact (* s 4)))
              (estimate-integral (lambda (x y)
                                   (define (square x) (* x x))
                                   (<= (+ (square (- x 5)) (square (- y 7)))
                                       (square 3)))
                                 2 8 4 10)))
