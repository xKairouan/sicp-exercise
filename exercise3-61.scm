;; -*- coding: utf-8 -*-

;; *Exercise 3.61:* Let S be a power series (*Note Exercise 3-59::)
;; whose constant term is 1.  Suppose we want to find the power
;; series 1/S, that is, the series X such that S * X = 1.  Write S =
;; 1 + S_R where S_R is the part of S after the constant term.  Then
;; we can solve for X as follows:

;;              S * X = 1
;;      (1 + S_R) * X = 1
;;        X + S_R * X = 1
;;                  X = 1 - S_R * X

;; In other words, X is the power series whose constant term is 1 and
;; whose higher-order terms are given by the negative of S_R times X.
;; Use this idea to write a procedure `invert-unit-series' that
;; computes 1/S for a power series S with constant term 1.  You will
;; need to use `mul-series' from *Note Exercise 3-60::.

(load "./stream.scm")

(define (add-streams s1 s2)
  (stream-map + s1 s2))

(define (scale-stream stream factor)
  (stream-map (lambda (x) (* x factor)) stream))

(define (mul-series s1 s2)
  (cons-stream (* (stream-car s1)
                  (stream-car s2))
               (add-streams (scale-stream (stream-cdr s2) (stream-car s1))
                            (mul-series (stream-cdr s1) s2))))

(define (invert-unit-series s)
  (if (not (= (stream-car s) 1))
      (error "not a power series with constant 1 -- INVERT-UNIT-SERIES")
      (cons-stream 1 (stream-map - (mul-series (stream-cdr s)
                                               (invert-unit-series s))))))
