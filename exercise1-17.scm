;; -*- coding: utf-8 -*-

;; *Exercise 1.17:* The exponentiation algorithms in this section are
;; based on performing exponentiation by means of repeated
;; multiplication.  In a similar way, one can perform integer
;; multiplication by means of repeated addition.  The following
;; multiplication procedure (in which it is assumed that our language
;; can only add, not multiply) is analogous to the `expt' procedure:

;;      (define (* a b)
;;        (if (= b 0)
;;            0
;;            (+ a (* a (- b 1)))))

;; This algorithm takes a number of steps that is linear in `b'.  Now
;; suppose we include, together with addition, operations `double',
;; which doubles an integer, and `halve', which divides an (even)
;; integer by 2.  Using these, design a multiplication procedure
;; analogous to `fast-expt' that uses a logarithmic number of steps.

(define (double x)
  (* x 2))
(define (halve x)
  (/ x 2))

(define (fast-mul a b)
  (cond [(= b 0) 0]
        [(even? b) (double (fast-mul a (halve b)))]
        [else (+ a (fast-mul a (- b 1)))]))

(use gauche.test)
(test-start "exercise 1.17")
(test* "2 * 0 = 0" (* 1 0) (fast-mul 1 0))
(test* "0 * 2 = 0" (* 0 2) (fast-mul 0 2))
(test* "3 * 3 = 9" (* 3 3) (fast-mul 3 3))
(test* "3 * 4 = 12" (* 3 4) (fast-mul 3 4))
(test* "3 * 9 = 27" (* 3 9) (fast-mul 3 9))
(test* "3 * 10 = 30" (* 3 10) (fast-mul 3 10))
(test-end)
