;; -*- coding: utf-8 -*-

;; *Exercise 1.33:* You can obtain an even more general version of
;; `accumulate' (*Note Exercise 1-32::) by introducing the notion of
;; a "filter" on the terms to be combined.  That is, combine only
;; those terms derived from values in the range that satisfy a
;; specified condition.  The resulting `filtered-accumulate'
;; abstraction takes the same arguments as accumulate, together with
;; an additional predicate of one argument that specifies the filter.
;; Write `filtered-accumulate' as a procedure.  Show how to express
;; the following using `filtered-accumulate':

;;   a. the sum of the squares of the prime numbers in the interval a
;;      to b (assuming that you have a `prime?' predicate already
;;      written)

;;   b. the product of all the positive integers less than n that are
;;      relatively prime to n (i.e., all positive integers i < n such
;;      that GCD(i,n) = 1).

(define (filtered-accumulate pred combiner null-value term a next b)
  (cond [(> a b) null-value]
        [(pred a)
         (combiner (term a)
                   (filtered-accumulate pred combiner null-value term (next a) next b))]
        [else (filtered-accumulate pred combiner null-value term (next a) next b)]))

(define (prime? n)
  (define (square x) (* x x))
  (define (divides? a b)
    (= (remainder a b) 0))
  (define (iter i result)
    (cond [(> (square i ) n) result]
          [(not result) #f]
          [else (iter (+ i 1) (and (not (divides? n i)) result))]))
  (iter 2 #t))

(define (sum-prime-square a b)
  (define (id x) x)
  (define (next n) (+ n 1))
  (filtered-accumulate prime? + 0 id a next b))

(define (gcd a b)
  (if (= b 0)
      a
      (gcd b (remainder a b))))

(define (product-prime-to-n n)
  (define (prime-to-n? m)
    (= (gcd n m) 1))
  (define (id x) x)
  (filtered-accumulate prime-to-n? * 1 id 1 next n))
