;; -*- coding: utf-8 -*-

;; *Exercise 3.25:* Generalizing one- and two-dimensional tables,
;; show how to implement a table in which values are stored under an
;; arbitrary number of keys and different values may be stored under
;; different numbers of keys.  The `lookup' and `insert!' procedures
;; should take as input a list of keys used to access the table.

(define (make-table)
  (let ((local-table (list '*table*)))
    (define (lookup keys)
      (define (iter table keys)
        (if (null? keys)
            #f
            (let ((subtable (assoc (car keys) (cdr table))))
              (cond [(not subtable) #f]
                    [(not (pair? (cdr subtable)))
                     (if (null? (cdr keys))
                         (cdr subtable)
                         #f)]
                    [else (iter subtable (cdr keys))]))))
      (iter local-table keys))
    (define (insert! keys value)
      (define (make-subtable keys value)
        (if (null? (cdr keys))
            (cons (car keys) value)
            (list (car keys) (make-subtable (cdr keys) value))))
      (define (iter! table keys value)
        (let ((subtable (assoc (car keys) (cdr table))))
          (if (not subtable)
              (set-cdr! table (cons (make-subtable keys value)
                                    (cdr table)))
              (iter! subtable (cdr keys) value))))
      (if (null? keys)
          (error "at least 1 key needed -- INSERT")
          (iter! local-table keys value)))
    (define (print-table) (print local-table))
    (define (dispatch m)
      (cond [(eq? m 'lookup) lookup]
            [(eq? m 'insert!) insert!]
            [(eq? m 'print-table) print-table]
            [else (error "no such method" m)]))
    dispatch))

(define (lookup table keys) ((table 'lookup) keys))
(define (insert! table keys value) ((table 'insert!) keys value))
(define (print-table table) ((table 'print-table)))

;; test
(use gauche.test)
(test-start "exercise 3.25")
(define table (make-table))
(insert! table '(a b c) 1)
(insert! table '(b c d) 2)
(insert! table '(c d e) 3)
(test* "(a b c) => 1" 1 (lookup table '(a b c)))
(test* "(c d e) => 3" 3 (lookup table '(c d e)))
(test* "(a b d) => #f" #f (lookup table '(a b d)))
(test* "(a b) => #f" #f (lookup table '(a b)))
(test* "(a b c d) = #f" #f (lookup table '(a b c d)))
(test-end)