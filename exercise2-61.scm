;; -*- coding: utf-8 -*-

;; *Exercise 2.61:* Give an implementation of `adjoin-set' using the
;; ordered representation.  By analogy with `element-of-set?' show
;; how to take advantage of the ordering to produce a procedure that
;; requires on the average about half as many steps as with the
;; unordered representation.

(define (adjoin-set x set)
  (cond [(null? set) (list x)]
        [(< x (car set)) (cons x set)]
        [(= x (car set)) set]
        [else (cons (car set) (adjoin-set x (cdr set)))]))

(use gauche.test)
(test-start "exercise 2.61")
(test* "adjoin-set" '(1) (adjoin-set 1 '()))
(test* "adjoin-set" '(1 2 3 4) (adjoin-set 1 '(2 3 4)))
(test* "adjoin-set" '(1 2 3 4) (adjoin-set 2 '(1 3 4)))
(test* "adjoin-set" '(1 2 3 4) (adjoin-set 4 '(1 2 3)))
(test* "adjoin-set" '(1 2 3 4) (adjoin-set 3 '(1 2 3 4)))
(test-end)
