;; -*- coding: utf-8 -*-

;; *Exercise 2.59:* Implement the `union-set' operation for the
;; unordered-list representation of sets.


(define (element-of-set? x set)
  (cond ((null? set) #f)
        ((equal? x (car set)) #t)
        (else (element-of-set? x (cdr set)))))

(define (union-set set1 set2)
  (define (union-set-iter set1 set2 result)
    (cond [(and (null? set1) (null? set2)) result]
          [(null? set1) (union-set-iter set2 '() result)]
          [(not (element-of-set? (car set1) result))
           (union-set-iter (cdr set1) set2 (cons (car set1) result))]
          [else (union-set-iter (cdr set1) set2 result)]))
  (union-set-iter set1 set2 '()))

;; test
(use gauche.test)
(test-start "exercise 2.59")
(test* "union-set" 3 (length (union-set '(1 2) '(2 3))))
(test* "union-set" 3 (length (union-set '(1 2 2 2 2) '(2 3))))
(test-end)
