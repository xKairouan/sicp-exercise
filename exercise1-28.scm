;; -*- coding: utf-8 -*-

;; *Exercise 1.28:* One variant of the Fermat test that cannot be
;; fooled is called the "Miller-Rabin test" (Miller 1976; Rabin
;; 1980).  This starts from an alternate form of Fermat's Little
;; Theorem, which states that if n is a prime number and a is any
;; positive integer less than n, then a raised to the (n - 1)st power
;; is congruent to 1 modulo n.  To test the primality of a number n
;; by the Miller-Rabin test, we pick a random number a<n and raise a
;; to the (n - 1)st power modulo n using the `expmod' procedure.
;; However, whenever we perform the squaring step in `expmod', we
;; check to see if we have discovered a "nontrivial square root of 1
;; modulo n," that is, a number not equal to 1 or n - 1 whose square
;; is equal to 1 modulo n.  It is possible to prove that if such a
;; nontrivial square root of 1 exists, then n is not prime.  It is
;; also possible to prove that if n is an odd number that is not
;; prime, then, for at least half the numbers a<n, computing a^(n-1)
;; in this way will reveal a nontrivial square root of 1 modulo n.
;; (This is why the Miller-Rabin test cannot be fooled.)  Modify the
;; `expmod' procedure to signal if it discovers a nontrivial square
;; root of 1, and use this to implement the Miller-Rabin test with a
;; procedure analogous to `fermat-test'.  Check your procedure by
;; testing various known primes and non-primes.  Hint: One convenient
;; way to make `expmod' signal is to have it return 0.

(define (square x) (* x x))
(define (expmod base exp m)
  (define (check-nontrivial-square-root base m)
    (if (and (not (= base 1))
             (not (= base (- m 1)))
             (= (remainder (square base) m) 1))
        0
        base))
  (cond [(= exp 0) 1]
        [(even? exp)
         (remainder
          (square
           (check-nontrivial-square-root (expmod base (/ exp 2) m) m))
          m)]
        [else
         (remainder (* base (expmod base (- exp 1) m))
                    m)]))

(define (miller-rabin-test n)
  (define (try-it a)
    (= (expmod a (- n 1) n) 1))
  (define (iter count result)
    (cond [(> count (/ n 2)) result]
          [else (iter (+ count 1) (and result (try-it count)))]))
  (iter 1 #t))

(use gauche.test)
(test-start "exercise 1.28")
(test* "199^560 is nontrivial square root of 1 modulo 561" 0 (expmod 19 560 561))
(test* "2 is prime" #t (miller-rabin-test 2))
(test* "3 is prime" #t (miller-rabin-test 3))
(test* "4 is not prime" #f (miller-rabin-test 4))
(test* "5 is prime" #t (miller-rabin-test 5))
(test* "9 is not prime" #f (miller-rabin-test 9))
(test* "199 is prime" #t (miller-rabin-test 199))
(test* "561 is not prime" #f (miller-rabin-test 561))
(test* "1105 is not prime" #f (miller-rabin-test 1105))
(test* "1105 is not prime" #f (miller-rabin-test 1105))
(test* "1729 is not prime" #f (miller-rabin-test 1729))
(test* "2465 is not prime" #f (miller-rabin-test 2465))
(test* "2821 is not prime" #f (miller-rabin-test 2821))
(test* "6601 is not prime" #f (miller-rabin-test 6601))
(test-end)
