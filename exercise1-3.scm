;; -*- coding: utf-8 -*-

;; *Exercise 1.3:* Define a procedure that takes three numbers as
;; arguments and returns the sum of the squares of the two larger
;; numbers.

(define (sum-of-larger-two-square x y z)
  (define (sum-of-square x y) (+ (* x x) (* y y)))
  (cond ((and (<= x y) (<= x z)) (sum-of-square y z))
        ((and (<= y x) (<= y z)) (sum-of-square x z))
        (else (sum-of-square x y))))

;; test
(and (= (sum-of-larger-two-square 1 2 3) 13)
     (= (sum-of-larger-two-square 2 1 3) 13)
     (= (sum-of-larger-two-square 3 1 2) 13)
     (= (sum-of-larger-two-square 2 1 1) 5)
     (= (sum-of-larger-two-square 1 2 1) 5)
     (= (sum-of-larger-two-square 1 1 2) 5)) ; => #t
