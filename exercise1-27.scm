;; -*- coding: utf-8 -*-

;; *Exercise 1.27:* Demonstrate that the Carmichael numbers listed in
;; *Note Footnote 1-47:: really do fool the Fermat test.  That is,
;; write a procedure that takes an integer n and tests whether a^n is
;; congruent to a modulo n for every a<n, and try your procedure on
;; the given Carmichael numbers.

(define (test-carmicheal-numbers n)
  (define (square x) (* x x))
  (define (expmod base exp m)
    (cond ((= exp 0) 1)
          ((even? exp)
           (remainder (square (expmod base (/ exp 2) m))
                      m))
          (else
           (remainder (* base (expmod base (- exp 1) m))
                      m))))
  (define (try-it a)
    (= (expmod a n n) a))
  (define (iter a is-prime)
    (cond [(= a n) is-prime]
          [(not is-prime) #f]
          [else (iter (+ a 1) (and is-prime (try-it a)))]))
  (iter 1 #t))

;; test
(use gauche.test)
(test-start "exercise 1.27")
(test* "561" #t (test-carmicheal-numbers 561))
(test* "1105" #t (test-carmicheal-numbers 1105))
(test* "1729" #t (test-carmicheal-numbers 1729))
(test* "2465" #t (test-carmicheal-numbers 2465))
(test* "2821" #t (test-carmicheal-numbers 2821))
(test* "6601" #t (test-carmicheal-numbers 6601))
(test-end)
