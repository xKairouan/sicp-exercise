;; -*- coding: utf-8 -*-

;; *Exercise 2.66:* Implement the `lookup' procedure for the case
;; where the set of records is structured as a binary tree, ordered
;; by the numerical values of the keys.

(define (make-record given-key given-value)
  (list given-key given-value))
(define (key record) (car record))
(define (value record) (cadr record))

(define (entry tree) (car tree))
(define (left-branch tree) (cadr tree))
(define (right-branch tree) (caddr tree))
(define (make-tree entry left right)
  (list entry left right))

(define (list->tree elements)
  (define (partial-tree elts n)
    (if (= n 0)
        (cons '() elts)
        (let ((left-size (quotient (- n 1) 2)))
          (let ((left-result (partial-tree elts left-size)))
            (let ((left-tree (car left-result))
                  (non-left-elts (cdr left-result))
                  (right-size (- n (+ left-size 1))))
              (let ((this-entry (car non-left-elts))
                    (right-result (partial-tree (cdr non-left-elts)
                                                right-size)))
                (let ((right-tree (car right-result))
                      (remaining-elts (cdr right-result)))
                  (cons (make-tree this-entry left-tree right-tree)
                        remaining-elts))))))))
  (car (partial-tree elements (length elements))))


(define (lookup given-key set-of-records)
  (cond [(null? set-of-records) #f]
        [(< given-key (key (entry set-of-records)))
         (lookup given-key (left-branch set-of-records))]
        [(= given-key (key (entry set-of-records)))
         (entry set-of-records)]
        [(> given-key (key (entry set-of-records)))
         (lookup given-key (right-branch set-of-records))]))

;; test
(use gauche.test)
(test-start "exercise 2.66")
(test* "lookup" #f (lookup 1 '()))
(let ((records (list->tree (list (make-record 1 'Alice)
                                 (make-record 2 'Bob)
                                 (make-record 3 'Christopher)
                                 (make-record 4 'Denis)))))
  (test* "lookup" #f (lookup 0 records))
  (test* "lookup" #f (lookup 5 records))
  (test* "lookup" (make-record 1 'Alice) (lookup 1 records))
  (test* "lookup" (make-record 3 'Christopher) (lookup 3 records))
  (test* "lookup" (make-record 4 'Denis) (lookup 4 records)))
(test-end)
