;; -*- coding: utf-8 -*-

;; *Exercise 3.19:* Redo *Note Exercise 3-18:: using an algorithm
;; that takes only a constant amount of space.  (This requires a very
;; clever idea.)

(define (loop? lis)
  (if (or (null? lis) (null? (cdr lis)))
      #f
      (let ((p1 lis)
            (p2 (cdr lis)))
        (define (loop-inner)
          (cond [(null? (cdr p1)) #f]
                [(or (null? (cdr p2)) (null? (cddr p2))) #f]
                [(eq? p1 p2) #t]
                [else
                 (set! p1 (cdr p1))
                 (set! p2 (cddr p2))
                 (loop-inner)]))
        (loop-inner))))


;; test data
(define x '(1 2 3))
(set-cdr! (last-pair x) x)

(define y '(1 2 3))
(set-cdr! (last-pair y) (cdr y))

(use gauche.test)
(test-start "exercise 3.18")
(test* "loop test -- false" #f (loop? '(1 2 3)))
(test* "loop test -- true" #t (loop? x))
(test* "loop test -- true" #t (loop? y))
(test-end)
