;; -*- coding: utf-8 -*-

;; *Figure 3.35:* Signal-flow diagram for the solution to a
;; second-order linear differential equation.

;;                     dy_0                y_0
;;                      |                   |
;;                      V                   V
;;         ddy     +----------+    dy  +----------+    y
;;      +--------->| integral +-----*--+ integral +--*--->
;;      |          +----------+     |  +----------+  |
;;      |                           |                |
;;      |            +----------+   |                |
;;      |     __/|<--+ scale: a |<--+                |
;;      |   _/   |   +----------+                    |
;;      +--<_add |                                   |
;;           \__ |   +----------+                    |
;;              \|<--+ scale: b |<-------------------+
;;                   +----------+

;; *Exercise 3.78:* Consider the problem of designing a
;; signal-processing system to study the homogeneous second-order
;; linear differential equation

;;      d^2 y        d y
;;      -----  -  a -----  -  by  =  0
;;      d t^2        d t

;; The output stream, modeling y, is generated by a network that
;; contains a loop. This is because the value of d^2y/dt^2 depends
;; upon the values of y and dy/dt and both of these are determined by
;; integrating d^2y/dt^2.  The diagram we would like to encode is
;; shown in *Note Figure 3-35::.  Write a procedure `solve-2nd' that
;; takes as arguments the constants a, b, and dt and the initial
;; values y_0 and dy_0 for y and dy/dt and generates the stream of
;; successive values of y.


(load "./stream.scm")

(define (add-stream s1 s2) (stream-map + s1 s2))
(define (scale-map s factor) (stream-map (lambda (x) (* x factor)) s))

(define (integral delayed-integrand initial-value dt)
  (cons-stream initial-value
               (let ((integrand (force delayed-integrand)))
                 (if (stream-null? integrand)
                     the-empty-stream
                     (integral (delay (stream-cdr integrand))
                               (+ (* dt (stream-car integrand))
                                  initial-value)
                               dt)))))

(define (solve-2nd a b dt y0 dy0)
  (define y (integral (delay dy) y0 dt))
  (define dy (integral (delay ddy) dy0 dt))
  (define ddy (add-stream (scale-stream y b)
                          (scale-stream dy a)))
  y)

(print (stream-ref (solve-2nd 1 -1 0.001 0 1) 1000))