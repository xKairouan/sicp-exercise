;; -*- coding: utf-8 -*-

;; *Exercise 3.75:* Unfortunately, Alyssa's zero-crossing detector in
;; *Note Exercise 3-74:: proves to be insufficient, because the noisy
;; signal from the sensor leads to spurious zero crossings.  Lem E.
;; Tweakit, a hardware specialist, suggests that Alyssa smooth the
;; signal to filter out the noise before extracting the zero
;; crossings.  Alyssa takes his advice and decides to extract the
;; zero crossings from the signal constructed by averaging each value
;; of the sense data with the previous value.  She explains the
;; problem to her assistant, Louis Reasoner, who attempts to
;; implement the idea, altering Alyssa's program as follows:

;;      (define (make-zero-crossings input-stream last-value)
;;        (let ((avpt (/ (+ (stream-car input-stream) last-value) 2)))
;;          (cons-stream (sign-change-detector avpt last-value)
;;                       (make-zero-crossings (stream-cdr input-stream)
;;                                            avpt))))

;; This does not correctly implement Alyssa's plan.  Find the bug
;; that Louis has installed and fix it without changing the structure
;; of the program.  (Hint: You will need to increase the number of
;; arguments to `make-zero-crossings'.)

(use math.const)

(load "./stream.scm")

(define (sign-change-detector old-value new-value)
  (cond [(and (negative? old-value) (not (negative? new-value)))
         1]
        [(and (not (negative? old-value)) (negative? new-value))
         -1]
        [else 0]))

(define (list->stream lis)
  (if (null? lis)
      the-empty-stream
      (cons-stream (car lis) (list->stream (cdr lis)))))

(define integers (cons-stream 1 (stream-map (lambda (x) (+ x 1)) integers)))

(define sense-data
  (stream-map (lambda (x) (sin (* x (/ pi 4))))
              integers))

;; last-value として平均値を渡して
;; それを次回の平均の算出に使うと、
;; Alyssa の意図した「前回の値と今回の値の平均」にはならないので
;; これがバグ。
;; 前回の生の値を渡してやるようにする。
(define (make-zero-crossings input-stream prev-value last-value)
  (let ((avpt (/ (+ (stream-car input-stream) prev-value) 2)))
    (cons-stream (sign-change-detector avpt last-value)
                 (make-zero-crossings (stream-cdr input-stream)
                                      (stream-car input-stream)
                                      avpt))))
