;; -*- coding: utf-8 -*-

;; *Exercise 2.33:* Fill in the missing expressions to complete the
;; following definitions of some basic list-manipulation operations
;; as accumulations:

;;      (define (map p sequence)
;;        (accumulate (lambda (x y) <??>) nil sequence))

;;      (define (append seq1 seq2)
;;        (accumulate cons <??> <??>))

;;      (define (length sequence)
;;        (accumulate <??> 0 sequence))

(define (accumulate op initial sequence)
  (if (null? sequence)
      initial
      (op (car sequence)
          (accumulate op initial (cdr sequence)))))

(define (my-map p sequence)
  (accumulate (lambda (x y) (cons (p x) y)) '() sequence))
(define (my-append seq1 seq2)
  (accumulate cons seq2 seq1))
(define (my-length sequence)
  (accumulate (lambda (x y) (+ y 1)) 0 sequence))

(use gauche.test)
(test-start "exercise 2.33")
(test* "my-map" (map (lambda (x) (* x x)) '(1 2 3 4 5))
       (my-map (lambda (x) (* x x)) '(1 2 3 4 5)))
(test* "my-append" (append '(1 2 3) '(4 5 6))
       (my-append '(1 2 3) '(4 5 6)))
(test* "my-length" (length '(1 2 3 4 5))
       (my-length '(1 2 3 4 5)))
(test-end)
