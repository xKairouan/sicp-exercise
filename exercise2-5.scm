;; -*- coding: utf-8 -*-

;; *Exercise 2.5:* Show that we can represent pairs of nonnegative
;; integers using only numbers and arithmetic operations if we
;; represent the pair a and b as the integer that is the product 2^a
;; 3^b.  Give the corresponding definitions of the procedures `cons',
;; `car', and `cdr'.

(define (mycons a b)
  (* (expt 2 a) (expt 3 b)))

(define (divide-count x y)
  (define (devides? a b)
    (= (remainder a b) 0))
  (define (iter x y result)
    (if (devides? x y)
        (iter (/ x y) y (+ result 1))
        result))
  (iter x y 0))

(define (mycar p)
  (divide-count p 2))

(define (mycdr p)
  (divide-count p 3))

;; test
(use gauche.test)
(test-start "exercise 2-5")
(test* "(mycar (mycons 5 8)) => 5" 5 (mycar (mycons 5 8)))
(test* "(mycdr (mycons 5 8)) => 8" 8 (mycdr (mycons 5 8)))
(test-end)
