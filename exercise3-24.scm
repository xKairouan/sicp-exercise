;; -*- coding: utf-8 -*-

;; *Exercise 3.24:* In the table implementations above, the keys are
;; tested for equality using `equal?' (called by `assoc').  This is
;; not always the appropriate test.  For instance, we might have a
;; table with numeric keys in which we don't need an exact match to
;; the number we're looking up, but only a number within some
;; tolerance of it.  Design a table constructor `make-table' that
;; takes as an argument a `same-key?' procedure that will be used to
;; test "equality" of keys.  `Make-table' should return a `dispatch'
;; procedure that can be used to access appropriate `lookup' and
;; `insert!' procedures for a local table.


(define (make-table same-key?)
  (define (assoc/same-key key alist)
    (cond [(null? alist) #f]
          [(same-key? key (caar alist)) (car alist)]
          [else (assoc/same-key key (cdr alist))]))
  (let ((local-table (list '*table*)))
    (define (lookup key1 key2)
      (let ((subtable (assoc/same-key key1 (cdr local-table))))
        (if subtable
            (let ((record (assoc/same-key key2 (cdr subtable))))
              (if record
                  (cdr record)
                  #f))
            #f)))
    (define (insert! key1 key2 value)
      (let ((subtable (assoc/same-key key1 (cdr local-table))))
        (if subtable
            (let ((record (assoc/same-key key2 (cdr subtable))))
              (if record
                  (set-cdr! record value)
                  (set-cdr! subtable (cons (cons key2 value)
                                           (cdr subtable)))))
            (set-cdr! local-table (cons (list key1 (cons key2 value))
                                        (cdr local-table))))))
    (define (dispatch m)
      (cond [(eq? m 'lookup) lookup]
            [(eq? m 'insert!) insert!]
            [else (error "no such method" m)]))
    dispatch))


(use gauche.test)
(test-start "exercise 3.24")
(define table (make-table (lambda (x y) (= (mod x 5) (mod y 5)))))
((table 'insert!) 1 0 'a)
((table 'insert!) 2 1 'b)
((table 'insert!) 3 2 'c)
((table 'insert!) 4 3 'd)
((table 'insert!) 5 4 'e)
(test* "(1 0) in table" 'a ((table 'lookup) 1 0))
(test* "(6 0) is same value to 1 in table" 'a ((table 'lookup) 6 0))
((table 'insert!) 6 5 'f)
(test* "(1 0) is now f" 'f ((table 'lookup) 1 0))
(test-end)
