;; -*- coding: utf-8 -*-

;; Exercise 2.37
;; .............

;; Suppose we represent vectors v = (v_i) as sequences of numbers, and
;; matrices m = (m_(ij)) as sequences of vectors (the rows of the matrix).
;; For example, the matrix

;;      +-         -+
;;      |  1 2 3 4  |
;;      |  4 5 6 6  |
;;      |  6 7 8 9  |
;;      +-         -+

;; is represented as the sequence `((1 2 3 4) (4 5 6 6) (6 7 8 9))'.  With
;; this representation, we can use sequence operations to concisely
;; express the basic matrix and vector operations.  These operations
;; (which are described in any book on matrix algebra) are the following:

;;                                             __
;;      (dot-product v w)      returns the sum >_i v_i w_i

;;      (matrix-*-vector m v)  returns the vector t,
;;                                         __
;;                             where t_i = >_j m_(ij) v_j

;;      (matrix-*-matrix m n)  returns the matrix p,
;;                                            __
;;                             where p_(ij) = >_k m_(ik) n_(kj)

;;      (transpose m)          returns the matrix n,
;;                             where n_(ij) = m_(ji)

;;    We can define the dot product as(4)

;;      (define (dot-product v w)
;;        (accumulate + 0 (map * v w)))

;;    Fill in the missing expressions in the following procedures for
;; computing the other matrix operations.  (The procedure `accumulate-n'
;; is defined in *Note Exercise 2-36::.)

;;      (define (matrix-*-vector m v)
;;        (map <??> m))

;;      (define (transpose mat)
;;        (accumulate-n <??> <??> mat))

;;      (define (matrix-*-matrix m n)
;;        (let ((cols (transpose n)))
;;          (map <??> m)))

(define (accumulate op initial sequence)
  (if (null? sequence)
      initial
      (op (car sequence)
          (accumulate op initial (cdr sequence)))))

(define (accumulate-n op init seqs)
  (if (null? (car seqs))
      '()
      (cons (accumulate op init (map car seqs))
            (accumulate-n op init (map cdr seqs)))))

;; answer
(define (dot-product v w)
  (accumulate + 0 (map * v w)))

(define (matrix-*-vector m v)
  (map (lambda (row) (dot-product row v)) m))

(define (transpose mat)
  (accumulate-n cons '() mat))

(define (matrix-*-matrix m n)
  (let ((cols (transpose n)))
    (map (lambda (row)
           (map (lambda (col)
                  (dot-product row col))
                cols))
         m)))

;; test
(use gauche.test)
(test-start "exercise 2.37")
(test* "matrix-*-vector" '(1 2) (matrix-*-vector '((1 2) (2 3)) '(1 0)))
(test* "matrix-*-vector" '(2 3) (matrix-*-vector '((1 2) (2 3)) '(0 1)))
(test* "transpose" '((0 1) (1 0) (1 1)) (transpose '((0 1 1) (1 0 1))))
(test* "matrix-*-matrix" '((2 1 3) (3 2 5))
       (matrix-*-matrix '((1 2) (2 3))
                        '((0 1 1) (1 0 1))))
(test-end)
