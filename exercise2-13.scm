;; -*- coding: utf-8 -*-

;; *Exercise 2.13:* Show that under the assumption of small
;; percentage tolerances there is a simple formula for the approximate
;; percentage tolerance of the product of two intervals in terms of
;; the tolerances of the factors.  You may simplify the problem by
;; assuming that all numbers are positive.

(define make-interval cons)
(define lower-bound car)
(define upper-bound cdr)

(define (make-center-percent center percent)
  (let ((width (* center (/. percent 100))))
    (make-interval (- center width)
                   (+ center width))))
(define (center interval)
  (/. (+ (lower-bound interval) (upper-bound interval)) 2))
(define (percent interval)
  (let ((width (/. (- (upper-bound interval) (lower-bound interval)) 2)))
    (/. (* 100 width) (center interval))))
