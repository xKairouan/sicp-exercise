;; -*- coding: utf-8 -*-

;; *Exercise 2.18:* Define a procedure `reverse' that takes a list as
;; argument and returns a list of the same elements in reverse order:

;;      (reverse (list 1 4 9 16 25))
;;      (25 16 9 4 1)

(define (my-reverse lis)
  (define (reverse-iter lis result)
    (if (null? lis)
        result
        (reverse-iter (cdr lis) (cons (car lis) result))))
  (reverse-iter lis '()))

(use gauche.test)
(test-start "exercise 2.18")
(test* "(my-reverse '()) => ()" '() (my-reverse '()))
(test* "(my-reverse (list 1)) => (1)" '(1) (my-reverse (list 1)))
(test* "(my-reverse (list 1 4 9 16 25)) => (25 16 9 4 1)"
       '(25 16 9 4 1)
       (my-reverse (list 1 4 9 16 25)))
(test-end)
