;; -*- coding: utf-8 -*-

;; *Exercise 3.76:* Eva Lu Ator has a criticism of Louis's approach
;; in *Note Exercise 3-75::.  The program he wrote is not modular,
;; because it intermixes the operation of smoothing with the
;; zero-crossing extraction.  For example, the extractor should not
;; have to be changed if Alyssa finds a better way to condition her
;; input signal.  Help Louis by writing a procedure `smooth' that
;; takes a stream as input and produces a stream in which each
;; element is the average of two successive input stream elements.
;; Then use `smooth' as a component to implement the zero-crossing
;; detector in a more modular style.

(use math.const)

(load "./stream.scm")

(define (sign-change-detector old-value new-value)
  (cond [(and (negative? old-value) (not (negative? new-value)))
         1]
        [(and (not (negative? old-value)) (negative? new-value))
         -1]
        [else 0]))

(define (list->stream lis)
  (if (null? lis)
      the-empty-stream
      (cons-stream (car lis) (list->stream (cdr lis)))))

(define integers (cons-stream 1 (stream-map (lambda (x) (+ x 1)) integers)))
(define sense-data
  (stream-map (lambda (x) (sin (* x (/ pi 4))))
              (cons-stream 0 integers)))

;; answer
(define (smooth stream)
  (define (average x y) (/ (+ x y) 2))
  (stream-map average stream (cons-stream 0 stream)))

(define (make-zero-crossings stream)
  (stream-map sign-change-detector
              (smooth stream)
              (cons-stream 0 (smooth stream))))
(define zero-crossing (make-zero-crossings sense-data))