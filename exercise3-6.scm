;; -*- coding: utf-8 -*-

;; *Exercise 3.6:* It is useful to be able to reset a random-number
;; generator to produce a sequence starting from a given value.
;; Design a new `rand' procedure that is called with an argument that
;; is either the symbol `generate' or the symbol `reset' and behaves
;; as follows: `(rand 'generate)' produces a new random number;
;; `((rand 'reset) <NEW-VALUE>)' resets the internal state variable
;; to the designated <NEW-VALUE>.  Thus, by resetting the state, one
;; can generate repeatable sequences.  These are very handy to have
;; when testing and debugging programs that use random numbers.

(define random-init 1)

(define (random-update x)
  (modulo (+ (* 69069 x) 1) #x100000000))

(define random
  (let ((seed random-init))
    (lambda (command)
      (cond [(eq? command 'generate)
             (set! seed (random-update seed))
             seed]
            [(eq? command 'reset)
             (lambda (s)
               (set! seed s))]
            [else (error "unknown command -- RAND" command)]))))

;; test
(use gauche.test)
(test-start "exercise 3.6")
(let ((s1 (random 'generate))
      (s2 (random 'generate))
      (s3 (random 'generate)))
  ((random 'reset) random-init)
  (test* "generate same result -- 1st" s1 (random 'generate))
  (test* "generate same result -- 2nd" s2 (random 'generate))
  (test* "generate same result -- 3rd" s3 (random 'generate)))
(test-end)
